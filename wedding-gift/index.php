<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Подарок на свадьбу");
?>
<script>
    $(document).ready(function(){
        var fancy = jQuery('.fancy');
        fancy.fancybox({
            beforeShow: function () {
                this.title = "<div class='fancy-title'>" +
                "<div class='text'>Желаете такую же?</div>" +
                "<div class='purple-button' style='margin-top: 0'><a class='pb-link' href='http://<?php echo $_SERVER['HTTP_HOST'] ?>/zakazat-kartinu-na-khoste'>заказать</a></div>" +
                "</div>";
            },
            helpers: {
                title: {
                    type: 'inside'
                }
            },
            openEffect: 'elastic',
            closeEffect: 'elastic'
        });
    });
</script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/index.js"></script>
<?php
$file = file_get_contents('sect_inc9000321.php');
$spos = strpos($file, 'src="') + 5;
$subs = substr($file, $spos);
$furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
?>
<div id="bx_incl_area_8_1_me_ol">
    <? $APPLICATION->IncludeComponent(
        "bitrix:main.include",
        "",
        Array(
            "COMPONENT_TEMPLATE" => ".default",
            "AREA_FILE_SHOW" => "sect",
            "AREA_FILE_SUFFIX" => "inc9000321",
            "EDIT_TEMPLATE" => "",
            "AREA_FILE_RECURSIVE" => "Y"
        )
    ); ?>
</div>
<style>
    #bx_incl_area_8_1_me_ol img{
        display: none;
    }
</style>
<div class="original-gift" style="background-image: url(<?php echo $furl; ?>)">
    <div class="container">
        <div class="left_text_area">
            <div class="text-for-form top_area">
                <div class="text-normal">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "COMPONENT_TEMPLATE" => ".default",
                            "AREA_FILE_SHOW" => "sect",
                            "AREA_FILE_SUFFIX" => "inc_4",
                            "EDIT_TEMPLATE" => "",
                            "AREA_FILE_RECURSIVE" => "Y"
                        )
                    );?>

                </div>
            </div>
            <div class="text-for-form bottom_area">

                <div class="text-small">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "COMPONENT_TEMPLATE" => ".default",
                            "AREA_FILE_SHOW" => "sect",
                            "AREA_FILE_SUFFIX" => "inc_5",
                            "EDIT_TEMPLATE" => "",
                            "AREA_FILE_RECURSIVE" => "Y"
                        )
                    );?>
                </div>
                <div class="pink-arrow">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "COMPONENT_TEMPLATE" => ".default",
                            "AREA_FILE_SHOW" => "sect",
                            "AREA_FILE_SUFFIX" => "inc_6",
                            "EDIT_TEMPLATE" => "",
                            "AREA_FILE_RECURSIVE" => "Y"
                        )
                    );?></div>
            </div>
        </div>
        <div class="form-wraper">
            <form method="post"  action="" id="ajaxform1" class="send-form" enctype="multipart/form-data">
                <div class="form-header">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "COMPONENT_TEMPLATE" => ".default",
                            "AREA_FILE_SHOW" => "sect",
                            "AREA_FILE_SUFFIX" => "inc_7",
                            "EDIT_TEMPLATE" => "",
                            "AREA_FILE_RECURSIVE" => "Y"
                        )
                    );?>
                </div>
                <div class="form-small-text">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "COMPONENT_TEMPLATE" => ".default",
                            "AREA_FILE_SHOW" => "sect",
                            "AREA_FILE_SUFFIX" => "inc_8",
                            "EDIT_TEMPLATE" => "",
                            "AREA_FILE_RECURSIVE" => "Y"
                        )
                    );?>
                </div>
                <input type="text" class="form-input" name="name" placeholder="Ваше имя">

                <div class="help-text">Для связи с Вами</div>
                <div class="clearfix"></div>
                <input type="text" class="form-input" name="phone" placeholder="Ваш телефон">

                <div class="help-text">Для связи с Вами</div>
                <div class="clearfix"></div>
                <input type="email" class="form-input" name="email" placeholder="Ваш e-mail">

                <div class="help-text">Чтобы отправить Вам готовый эскиз картины</div>
                <div class="clearfix"></div>
                <div class="header-for-upload">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "COMPONENT_TEMPLATE" => ".default",
                            "AREA_FILE_SHOW" => "sect",
                            "AREA_FILE_SUFFIX" => "inc9",
                            "EDIT_TEMPLATE" => "",
                            "AREA_FILE_RECURSIVE" => "Y"
                        )
                    );?>
                </div>
                <div class="upload-button">
                    <input id="file_s" class="real-button" multiple="multiple" name="file" type="file">
                    <input class="uploaded-name" type="text">
                    <button class="fake-button">Загрузить файл</button>
                </div>
                <button type="submit" class="send-button-cutom">ПОЛУЧИТЬ ЭСКИЗ</button>
                <div class="privacy">Политика конфиденциальности</div>
            </form>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="examples">
    <div class="container">
        <div class="color-header">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "AREA_FILE_SHOW" => "sect",
                    "AREA_FILE_SUFFIX" => "inc10",
                    "EDIT_TEMPLATE" => "",
                    "AREA_FILE_RECURSIVE" => "Y"
                )
            );?>
        </div>
        <div class="photo-links">
            <?$APPLICATION->IncludeComponent("bitrix:news.list", "presents", Array(
                "COMPONENT_TEMPLATE" => ".default",
                "IBLOCK_TYPE" => "simple",	// Тип информационного блока (используется только для проверки)
                "IBLOCK_ID" => "2",	// Код информационного блока
                "NEWS_COUNT" => "20",	// Количество новостей на странице
                "SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
                "SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
                "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
                "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
                "FILTER_NAME" => "",	// Фильтр
                "FIELD_CODE" => array(	// Поля
                    0 => "",
                    1 => "",
                ),
                "PROPERTY_CODE" => array(	// Свойства
                    0 => "URL",
                    1 => "",
                ),
                "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
                "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                "AJAX_MODE" => "N",	// Включить режим AJAX
                "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
                "CACHE_TYPE" => "A",	// Тип кеширования
                "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
                "CACHE_GROUPS" => "Y",	// Учитывать права доступа
                "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
                "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
                "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
                "SET_BROWSER_TITLE" => "Y",	// Устанавливать заголовок окна браузера
                "SET_META_KEYWORDS" => "Y",	// Устанавливать ключевые слова страницы
                "SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
                "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
                "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
                "PARENT_SECTION" => "",	// ID раздела
                "PARENT_SECTION_CODE" => "",	// Код раздела
                "INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
                "DISPLAY_DATE" => "Y",	// Выводить дату элемента
                "DISPLAY_NAME" => "Y",	// Выводить название элемента
                "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
                "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
                "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
                "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                "DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
                "PAGER_TITLE" => "Новости",	// Название категорий
                "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
                "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
                "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
                "PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
                "SET_STATUS_404" => "N",	// Устанавливать статус 404
                "SHOW_404" => "N",	// Показ специальной страницы
                "MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
            ),
                false
            );?>
            <div class="clearfix"></div>
        </div>
        <div class="purple-button">
            <a href="http://<?php echo $_SERVER['HTTP_HOST'] ?>/zakazat-kartinu-na-khoste" class="pb-link">заказать фото на холсте</a>
        </div>
        <div class="green-bottom">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "AREA_FILE_SHOW" => "sect",
                    "AREA_FILE_SUFFIX" => "inc11",
                    "EDIT_TEMPLATE" => "",
                    "AREA_FILE_RECURSIVE" => "Y"
                )
            );?>
        </div>
    </div>
</div>
<div class="why-picture">
    <div class="container">
        <div class="color-header">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "AREA_FILE_SHOW" => "sect",
                    "AREA_FILE_SUFFIX" => "inc12",
                    "EDIT_TEMPLATE" => "",
                    "AREA_FILE_RECURSIVE" => "Y"
                )
            );?>
        </div>
        <div class="advantages-list">
            <?$APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "best_presents",
                array(
                    "COMPONENT_TEMPLATE" => "best_presents",
                    "IBLOCK_TYPE" => "simple",
                    "IBLOCK_ID" => "3",
                    "NEWS_COUNT" => "20",
                    "SORT_BY1" => "TIMESTAMP_X",
                    "SORT_ORDER1" => "ASC",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER2" => "DESC",
                    "FILTER_NAME" => "",
                    "FIELD_CODE" => array(
                        0 => "",
                        1 => "",
                    ),
                    "PROPERTY_CODE" => array(
                        0 => "",
                        1 => "",
                    ),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "SET_TITLE" => "Y",
                    "SET_BROWSER_TITLE" => "Y",
                    "SET_META_KEYWORDS" => "Y",
                    "SET_META_DESCRIPTION" => "Y",
                    "SET_LAST_MODIFIED" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                    "ADD_SECTIONS_CHAIN" => "Y",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "PAGER_TEMPLATE" => ".default",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "SET_STATUS_404" => "N",
                    "SHOW_404" => "N",
                    "MESSAGE_404" => ""
                ),
                false
            );?>
            <div class="clearfix"></div>
        </div>
        <div class="why-picture-bottom">
            <div class="hint left-hint">
                <div class="right-arrow">
                    <div class="hint-text">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "AREA_FILE_SHOW" => "sect",
                                "AREA_FILE_SUFFIX" => "inc13",
                                "EDIT_TEMPLATE" => "",
                                "AREA_FILE_RECURSIVE" => "Y"
                            )
                        );?>
                    </div>
                </div>
            </div>
            <div class="mackup-button">
                <a href="javascript:void(0);" class="mackup-link call-form">Получить эскиз картины сегодня</a>
            </div>
            <div class="hint right-hint">
                <div class="left-arrow">
                    <div class="hint-text">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "AREA_FILE_SHOW" => "sect",
                                "AREA_FILE_SUFFIX" => "inc14",
                                "EDIT_TEMPLATE" => "",
                                "AREA_FILE_RECURSIVE" => "Y"
                            )
                        );?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="slider">
    <div class="container">
        <div class="color-header">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "AREA_FILE_SHOW" => "sect",
                    "AREA_FILE_SUFFIX" => "inc15",
                    "EDIT_TEMPLATE" => "",
                    "AREA_FILE_RECURSIVE" => "Y"
                )
            );?>
        </div>
    </div>
    <div class="container gallery_simple">
        <?$APPLICATION->IncludeComponent("bitrix:news.list", "slider_emotion_simple", Array(
            "COMPONENT_TEMPLATE" => ".default",
            "IBLOCK_TYPE" => "slider",	// Тип информационного блока (используется только для проверки)
            "IBLOCK_ID" => "19",	// Код информационного блока
            "NEWS_COUNT" => "20",	// Количество новостей на странице
            "SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
            "SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
            "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
            "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
            "FILTER_NAME" => "",	// Фильтр
            "FIELD_CODE" => array(	// Поля
                0 => "",
                1 => "",
            ),
            "PROPERTY_CODE" => array(	// Свойства
                0 => "",
                1 => "",
            ),
            "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
            "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
            "AJAX_MODE" => "N",	// Включить режим AJAX
            "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
            "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
            "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
            "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
            "CACHE_TYPE" => "A",	// Тип кеширования
            "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
            "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
            "CACHE_GROUPS" => "Y",	// Учитывать права доступа
            "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
            "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
            "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
            "SET_BROWSER_TITLE" => "Y",	// Устанавливать заголовок окна браузера
            "SET_META_KEYWORDS" => "Y",	// Устанавливать ключевые слова страницы
            "SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
            "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
            "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
            "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
            "PARENT_SECTION" => "",	// ID раздела
            "PARENT_SECTION_CODE" => "",	// Код раздела
            "INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
            "DISPLAY_DATE" => "Y",	// Выводить дату элемента
            "DISPLAY_NAME" => "Y",	// Выводить название элемента
            "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
            "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
            "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
            "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
            "DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
            "PAGER_TITLE" => "Новости",	// Название категорий
            "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
            "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
            "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
            "PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
            "SET_STATUS_404" => "N",	// Устанавливать статус 404
            "SHOW_404" => "N",	// Показ специальной страницы
            "MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
        ),
            false
        );?>

    </div>
    <div class="link-box">
        <a href="#" id="more_clients" class="clients-link">Cмотреть еще примеры</a>
    </div>
</div>
<div class="best-size">
    <div class="container">
        <div class="color-header">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "AREA_FILE_SHOW" => "sect",
                    "AREA_FILE_SUFFIX" => "inc41",
                    "EDIT_TEMPLATE" => "",
                    "AREA_FILE_RECURSIVE" => "Y"
                )
            );?>
        </div>
        <div class="choose-size">
            <img src="/bitrix/templates/pikcher/images/lady.png" class="nice-image" alt="">
            <div class="size-line">
                <div class="size-example example4x4">
                    <div class="size-price">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "AREA_FILE_SHOW" => "sect",
                                "AREA_FILE_SUFFIX" => "inc202",
                                "EDIT_TEMPLATE" => "",
                                "AREA_FILE_RECURSIVE" => "Y"
                            )
                        );?><i class="fa fa-rub"></i>
                    </div>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "COMPONENT_TEMPLATE" => ".default",
                            "AREA_FILE_SHOW" => "sect",
                            "AREA_FILE_SUFFIX" => "500111",
                            "EDIT_TEMPLATE" => "",
                            "AREA_FILE_RECURSIVE" => "Y"
                        )
                    );?>
                    <div class="size-name">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "AREA_FILE_SHOW" => "sect",
                                "AREA_FILE_SUFFIX" => "inc203",
                                "EDIT_TEMPLATE" => "",
                                "AREA_FILE_RECURSIVE" => "Y"
                            )
                        );?>
                    </div>
                    <div class="purple-button"><a class="pb-link" href="http://<?php echo $_SERVER['HTTP_HOST'] ?>/zakazat-kartinu-na-khoste?size=<?php include('sect_inc203.php'); ?>">заказать</a></div>
                </div>
                <div class="size-example example4x6">
                    <div class="size-price">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "AREA_FILE_SHOW" => "sect",
                                "AREA_FILE_SUFFIX" => "inc204",
                                "EDIT_TEMPLATE" => "",
                                "AREA_FILE_RECURSIVE" => "Y"
                            )
                        );?><i class="fa fa-rub"></i>
                    </div>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "COMPONENT_TEMPLATE" => ".default",
                            "AREA_FILE_SHOW" => "sect",
                            "AREA_FILE_SUFFIX" => "500211",
                            "EDIT_TEMPLATE" => "",
                            "AREA_FILE_RECURSIVE" => "Y"
                        )
                    );?>
                    <div class="size-name">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "AREA_FILE_SHOW" => "sect",
                                "AREA_FILE_SUFFIX" => "inc205",
                                "EDIT_TEMPLATE" => "",
                                "AREA_FILE_RECURSIVE" => "Y"
                            )
                        );?>
                    </div>
                    <div class="purple-button"><a class="pb-link" href="http://<?php echo $_SERVER['HTTP_HOST'] ?>/zakazat-kartinu-na-khoste?size=<?php include('sect_inc205.php'); ?>">заказать</a></div>
                </div>
                <div class="size-example example6x6">
                    <div class="size-price">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "AREA_FILE_SHOW" => "sect",
                                "AREA_FILE_SUFFIX" => "inc206",
                                "EDIT_TEMPLATE" => "",
                                "AREA_FILE_RECURSIVE" => "Y"
                            )
                        );?><i class="fa fa-rub"></i>
                    </div>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "COMPONENT_TEMPLATE" => ".default",
                            "AREA_FILE_SHOW" => "sect",
                            "AREA_FILE_SUFFIX" => "500311",
                            "EDIT_TEMPLATE" => "",
                            "AREA_FILE_RECURSIVE" => "Y"
                        )
                    );?>
                    <div class="size-name">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "AREA_FILE_SHOW" => "sect",
                                "AREA_FILE_SUFFIX" => "inc207",
                                "EDIT_TEMPLATE" => "",
                                "AREA_FILE_RECURSIVE" => "Y"
                            )
                        );?>
                    </div>
                    <div class="purple-button"><a class="pb-link" href="http://<?php echo $_SERVER['HTTP_HOST'] ?>/zakazat-kartinu-na-khoste?size=<?php include('sect_inc207.php'); ?>">заказать</a></div>
                </div>
                <div class="size-example example12x8">
                    <div class="size-price">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "AREA_FILE_SHOW" => "sect",
                                "AREA_FILE_SUFFIX" => "inc208",
                                "EDIT_TEMPLATE" => "",
                                "AREA_FILE_RECURSIVE" => "Y"
                            )
                        );?><i class="fa fa-rub"></i>
                    </div>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "COMPONENT_TEMPLATE" => ".default",
                            "AREA_FILE_SHOW" => "sect",
                            "AREA_FILE_SUFFIX" => "500411",
                            "EDIT_TEMPLATE" => "",
                            "AREA_FILE_RECURSIVE" => "Y"
                        )
                    );?>
                    <div class="size-name">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "AREA_FILE_SHOW" => "sect",
                                "AREA_FILE_SUFFIX" => "inc209",
                                "EDIT_TEMPLATE" => "",
                                "AREA_FILE_RECURSIVE" => "Y"
                            )
                        );?>
                    </div>
                    <div class="purple-button"><a class="pb-link" href="http://<?php echo $_SERVER['HTTP_HOST'] ?>/zakazat-kartinu-na-khoste?size=<?php include('sect_inc209.php'); ?>">заказать</a></div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="green-bottom">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "AREA_FILE_SHOW" => "sect",
                    "AREA_FILE_SUFFIX" => "inc42",
                    "EDIT_TEMPLATE" => "",
                    "AREA_FILE_RECURSIVE" => "Y"
                )
            );?>
        </div>
        <div class="green-button"><a href="http://<?php echo $_SERVER['HTTP_HOST'] ?>/zakazat-kartinu-na-khoste" class="gb-link">Выбрать другой размер</a></div>
    </div>
</div>
<div class="free-delivery">
    <div class="container">
        <div class="photo-block">
            <div class="photo-place">
                <img src="/bitrix/templates/pikcher/images/photo.png" class="photo" alt="">

                <div class="rounded-small">
                    <div class="line-text small-triangle">Загрузить изображение...</div>
                </div>
            </div>
            <div class="rounded-big">
                <div class="line-text">Заказать пробник прямо сейчас!</div>
            </div>
        </div>
        <div class="info-block">
            <div class="align-center">
                <div class="info-text">
                        <span class="text-bold">
                             <?$APPLICATION->IncludeComponent(
                                 "bitrix:main.include",
                                 "",
                                 Array(
                                     "COMPONENT_TEMPLATE" => ".default",
                                     "AREA_FILE_SHOW" => "sect",
                                     "AREA_FILE_SUFFIX" => "inc16",
                                     "EDIT_TEMPLATE" => "",
                                     "AREA_FILE_RECURSIVE" => "Y"
                                 )
                             );?></span>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "COMPONENT_TEMPLATE" => ".default",
                            "AREA_FILE_SHOW" => "sect",
                            "AREA_FILE_SUFFIX" => "inc17",
                            "EDIT_TEMPLATE" => "",
                            "AREA_FILE_RECURSIVE" => "Y"
                        )
                    );?>

                </div>
                <div class="buttons-block">
                    <div class="send-and-get">
                        <a href="javascript:void(0);" class="sag-link call-form">отправить фото и получить эскиз</a>
                    </div>
                    <div class="order">
                        <a href="javascript:void(0);" class="order-link call-form">заказать пробник</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="for-whom">
    <div class="container">
        <div class="color-header">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "AREA_FILE_SHOW" => "sect",
                    "AREA_FILE_SUFFIX" => "inc18",
                    "EDIT_TEMPLATE" => "",
                    "AREA_FILE_RECURSIVE" => "Y"
                )
            );?>
        </div>
        <div class="variant-loop">
            <?$APPLICATION->IncludeComponent("bitrix:news.list", "whom_present", Array(
                "COMPONENT_TEMPLATE" => ".default",
                "IBLOCK_TYPE" => "simple",	// Тип информационного блока (используется только для проверки)
                "IBLOCK_ID" => "5",	// Код информационного блока
                "NEWS_COUNT" => "20",	// Количество новостей на странице
                "SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
                "SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
                "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
                "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
                "FILTER_NAME" => "",	// Фильтр
                "FIELD_CODE" => array(	// Поля
                    0 => "",
                    1 => "",
                ),
                "PROPERTY_CODE" => array(	// Свойства
                    0 => "",
                    1 => "",
                ),
                "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
                "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                "AJAX_MODE" => "N",	// Включить режим AJAX
                "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
                "CACHE_TYPE" => "A",	// Тип кеширования
                "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
                "CACHE_GROUPS" => "Y",	// Учитывать права доступа
                "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
                "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
                "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
                "SET_BROWSER_TITLE" => "Y",	// Устанавливать заголовок окна браузера
                "SET_META_KEYWORDS" => "Y",	// Устанавливать ключевые слова страницы
                "SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
                "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
                "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
                "PARENT_SECTION" => "",	// ID раздела
                "PARENT_SECTION_CODE" => "",	// Код раздела
                "INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
                "DISPLAY_DATE" => "Y",	// Выводить дату элемента
                "DISPLAY_NAME" => "Y",	// Выводить название элемента
                "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
                "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
                "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
                "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                "DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
                "PAGER_TITLE" => "Новости",	// Название категорий
                "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
                "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
                "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
                "PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
                "SET_STATUS_404" => "N",	// Устанавливать статус 404
                "SHOW_404" => "N",	// Показ специальной страницы
                "MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
            ),
                false
            );?>
            <div class="clearfix"></div>
        </div>
        <div class="green-bottom">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "AREA_FILE_SHOW" => "sect",
                    "AREA_FILE_SUFFIX" => "inc19",
                    "EDIT_TEMPLATE" => "",
                    "AREA_FILE_RECURSIVE" => "Y"
                )
            );?>
        </div>
        <div class="purple-button">
            <a href="javascript:void(0);" class="pb-link call-form">отправить фото и получить эскиз</a>
        </div>
    </div>
    <div class="hands-block">
        <div class="hands">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "AREA_FILE_SHOW" => "sect",
                    "AREA_FILE_SUFFIX" => "inc10011",
                    "EDIT_TEMPLATE" => "",
                    "AREA_FILE_RECURSIVE" => "Y"
                )
            );?>
        </div>
        <div class="hands">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "AREA_FILE_SHOW" => "sect",
                    "AREA_FILE_SUFFIX" => "inc10111",
                    "EDIT_TEMPLATE" => "",
                    "AREA_FILE_RECURSIVE" => "Y"
                )
            );?>
        </div>
        <div class="hands">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "AREA_FILE_SHOW" => "sect",
                    "AREA_FILE_SUFFIX" => "inc10211",
                    "EDIT_TEMPLATE" => "",
                    "AREA_FILE_RECURSIVE" => "Y"
                )
            );?>
        </div>
        <div class="hands">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "AREA_FILE_SHOW" => "sect",
                    "AREA_FILE_SUFFIX" => "inc10311",
                    "EDIT_TEMPLATE" => "",
                    "AREA_FILE_RECURSIVE" => "Y"
                )
            );?>
        </div>
        <div class="hands">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "AREA_FILE_SHOW" => "sect",
                    "AREA_FILE_SUFFIX" => "inc10411",
                    "EDIT_TEMPLATE" => "",
                    "AREA_FILE_RECURSIVE" => "Y"
                )
            );?>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="order-pikcher">
    <div class="container">
        <div class="color-header">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "AREA_FILE_SHOW" => "sect",
                    "AREA_FILE_SUFFIX" => "inc20",
                    "EDIT_TEMPLATE" => "",
                    "AREA_FILE_RECURSIVE" => "Y"
                )
            );?>
        </div>
        <div class="why-pikcher-list">
            <img src="/bitrix/templates/pikcher/images/order-pikcher.png" class="image-bg" alt="">
            <div class="pikcher-list">
                <div class="list-item"><span class="text-bold">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "AREA_FILE_SHOW" => "sect",
                                    "AREA_FILE_SUFFIX" => "inc21",
                                    "EDIT_TEMPLATE" => "",
                                    "AREA_FILE_RECURSIVE" => "Y"
                                )
                            );?></span><br><span class="other-text">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "AREA_FILE_SHOW" => "sect",
                                    "AREA_FILE_SUFFIX" => "inc22",
                                    "EDIT_TEMPLATE" => "",
                                    "AREA_FILE_RECURSIVE" => "Y"
                                )
                            );?>
                        </span></div>
                <div class="list-item"><span class="text-bold">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "AREA_FILE_SHOW" => "sect",
                                    "AREA_FILE_SUFFIX" => "inc23",
                                    "EDIT_TEMPLATE" => "",
                                    "AREA_FILE_RECURSIVE" => "Y"
                                )
                            );?>
                        </span><br><span class="other-text">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "AREA_FILE_SHOW" => "sect",
                                    "AREA_FILE_SUFFIX" => "inc24",
                                    "EDIT_TEMPLATE" => "",
                                    "AREA_FILE_RECURSIVE" => "Y"
                                )
                            );?>
                        </span></div>
                <div class="list-item"><span class="text-bold">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "AREA_FILE_SHOW" => "sect",
                                    "AREA_FILE_SUFFIX" => "inc25",
                                    "EDIT_TEMPLATE" => "",
                                    "AREA_FILE_RECURSIVE" => "Y"
                                )
                            );?>
                        </span><br><span class="other-text">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "AREA_FILE_SHOW" => "sect",
                                    "AREA_FILE_SUFFIX" => "inc26",
                                    "EDIT_TEMPLATE" => "",
                                    "AREA_FILE_RECURSIVE" => "Y"
                                )
                            );?>
                        </span></div>
                <div class="list-item"><span class="text-bold">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "AREA_FILE_SHOW" => "sect",
                                    "AREA_FILE_SUFFIX" => "inc27",
                                    "EDIT_TEMPLATE" => "",
                                    "AREA_FILE_RECURSIVE" => "Y"
                                )
                            );?>
                        </span><br><span class="other-text">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "AREA_FILE_SHOW" => "sect",
                                    "AREA_FILE_SUFFIX" => "inc28",
                                    "EDIT_TEMPLATE" => "",
                                    "AREA_FILE_RECURSIVE" => "Y"
                                )
                            );?>
                        </span></div>
                <div class="list-item"><span class="text-bold">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "AREA_FILE_SHOW" => "sect",
                                    "AREA_FILE_SUFFIX" => "inc29",
                                    "EDIT_TEMPLATE" => "",
                                    "AREA_FILE_RECURSIVE" => "Y"
                                )
                            );?>
                        </span><br><span class="other-text">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "AREA_FILE_SHOW" => "sect",
                                    "AREA_FILE_SUFFIX" => "inc30",
                                    "EDIT_TEMPLATE" => "",
                                    "AREA_FILE_RECURSIVE" => "Y"
                                )
                            );?>
                        </span></div>
                <div class="list-item"><span class="text-bold">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "AREA_FILE_SHOW" => "sect",
                                    "AREA_FILE_SUFFIX" => "inc31",
                                    "EDIT_TEMPLATE" => "",
                                    "AREA_FILE_RECURSIVE" => "Y"
                                )
                            );?>
                        </span><br><span class="other-text">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "AREA_FILE_SHOW" => "sect",
                                    "AREA_FILE_SUFFIX" => "inc32",
                                    "EDIT_TEMPLATE" => "",
                                    "AREA_FILE_RECURSIVE" => "Y"
                                )
                            );?>
                        </span></div>
                <div class="list-item"><span class="text-bold">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "AREA_FILE_SHOW" => "sect",
                                    "AREA_FILE_SUFFIX" => "inc33",
                                    "EDIT_TEMPLATE" => "",
                                    "AREA_FILE_RECURSIVE" => "Y"
                                )
                            );?>
                        </span><br><span class="other-text">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "AREA_FILE_SHOW" => "sect",
                                    "AREA_FILE_SUFFIX" => "inc34",
                                    "EDIT_TEMPLATE" => "",
                                    "AREA_FILE_RECURSIVE" => "Y"
                                )
                            );?>
                        </span></div>
                <div class="list-item"><span class="text-bold">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "AREA_FILE_SHOW" => "sect",
                                    "AREA_FILE_SUFFIX" => "inc35",
                                    "EDIT_TEMPLATE" => "",
                                    "AREA_FILE_RECURSIVE" => "Y"
                                )
                            );?>
                        </span><br><span class="other-text">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "AREA_FILE_SHOW" => "sect",
                                    "AREA_FILE_SUFFIX" => "inc36",
                                    "EDIT_TEMPLATE" => "",
                                    "AREA_FILE_RECURSIVE" => "Y"
                                )
                            );?>
                        </span></div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="purple-button">
            <a href="javascript:void(0);" class="pb-link call-form">получить маке картины бесплатно</a>
        </div>
    </div>
</div>
<div class="production">
    <div class="container">
        <div class="color-header">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "AREA_FILE_SHOW" => "sect",
                    "AREA_FILE_SUFFIX" => "inc37",
                    "EDIT_TEMPLATE" => "",
                    "AREA_FILE_RECURSIVE" => "Y"
                )
            );?>
        </div>
        <div class="production-cycle">
            <?$APPLICATION->IncludeComponent("bitrix:news.list", "production", Array(
                "COMPONENT_TEMPLATE" => ".default",
                "IBLOCK_TYPE" => "simple",	// Тип информационного блока (используется только для проверки)
                "IBLOCK_ID" => "6",	// Код информационного блока
                "NEWS_COUNT" => "20",	// Количество новостей на странице
                "SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
                "SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
                "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
                "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
                "FILTER_NAME" => "",	// Фильтр
                "FIELD_CODE" => array(	// Поля
                    0 => "",
                    1 => "",
                ),
                "PROPERTY_CODE" => array(	// Свойства
                    0 => "",
                    1 => "",
                ),
                "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
                "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                "AJAX_MODE" => "N",	// Включить режим AJAX
                "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
                "CACHE_TYPE" => "A",	// Тип кеширования
                "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
                "CACHE_GROUPS" => "Y",	// Учитывать права доступа
                "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
                "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
                "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
                "SET_BROWSER_TITLE" => "Y",	// Устанавливать заголовок окна браузера
                "SET_META_KEYWORDS" => "Y",	// Устанавливать ключевые слова страницы
                "SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
                "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
                "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
                "PARENT_SECTION" => "",	// ID раздела
                "PARENT_SECTION_CODE" => "",	// Код раздела
                "INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
                "DISPLAY_DATE" => "Y",	// Выводить дату элемента
                "DISPLAY_NAME" => "Y",	// Выводить название элемента
                "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
                "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
                "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
                "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                "DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
                "PAGER_TITLE" => "Новости",	// Название категорий
                "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
                "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
                "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
                "PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
                "SET_STATUS_404" => "N",	// Устанавливать статус 404
                "SHOW_404" => "N",	// Показ специальной страницы
                "MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
            ),
                false
            );?>
        </div>
        <div class="green-bottom">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "AREA_FILE_SHOW" => "sect",
                    "AREA_FILE_SUFFIX" => "inc38",
                    "EDIT_TEMPLATE" => "",
                    "AREA_FILE_RECURSIVE" => "Y"
                )
            );?>
        </div>
        <div class="purple-button">
            <a href="javascript:void(0);" class="pb-link call-form">отправить фотографию для эскиза</a>
        </div>
    </div>
</div>
<div class="best-quality">
    <div class="container">
        <div class="color-header">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "AREA_FILE_SHOW" => "sect",
                    "AREA_FILE_SUFFIX" => "inc39",
                    "EDIT_TEMPLATE" => "",
                    "AREA_FILE_RECURSIVE" => "Y"
                )
            );?>
        </div>
        <div class="small-title">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "AREA_FILE_SHOW" => "sect",
                    "AREA_FILE_SUFFIX" => "inc40",
                    "EDIT_TEMPLATE" => "",
                    "AREA_FILE_RECURSIVE" => "Y"
                )
            );?>
        </div>
        <div class="compare">
            <div class="our-work">
                <div class="works-title">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "COMPONENT_TEMPLATE" => ".default",
                            "AREA_FILE_SHOW" => "sect",
                            "AREA_FILE_SUFFIX" => "inc200",
                            "EDIT_TEMPLATE" => "",
                            "AREA_FILE_RECURSIVE" => "Y"
                        )
                    );?>
                </div>
                <?$APPLICATION->IncludeComponent("bitrix:news.list", "our_work", Array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "IBLOCK_TYPE" => "simple",	// Тип информационного блока (используется только для проверки)
                    "IBLOCK_ID" => "7",	// Код информационного блока
                    "NEWS_COUNT" => "20",	// Количество новостей на странице
                    "SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
                    "SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
                    "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
                    "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
                    "FILTER_NAME" => "",	// Фильтр
                    "FIELD_CODE" => array(	// Поля
                        0 => "",
                        1 => "",
                    ),
                    "PROPERTY_CODE" => array(	// Свойства
                        0 => "",
                        1 => "",
                    ),
                    "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
                    "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                    "AJAX_MODE" => "N",	// Включить режим AJAX
                    "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                    "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                    "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                    "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
                    "CACHE_TYPE" => "A",	// Тип кеширования
                    "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                    "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
                    "CACHE_GROUPS" => "Y",	// Учитывать права доступа
                    "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
                    "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
                    "SET_BROWSER_TITLE" => "Y",	// Устанавливать заголовок окна браузера
                    "SET_META_KEYWORDS" => "Y",	// Устанавливать ключевые слова страницы
                    "SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
                    "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
                    "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
                    "PARENT_SECTION" => "",	// ID раздела
                    "PARENT_SECTION_CODE" => "",	// Код раздела
                    "INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
                    "DISPLAY_DATE" => "Y",	// Выводить дату элемента
                    "DISPLAY_NAME" => "Y",	// Выводить название элемента
                    "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
                    "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
                    "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
                    "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                    "DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
                    "PAGER_TITLE" => "Новости",	// Название категорий
                    "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
                    "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
                    "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
                    "PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
                    "SET_STATUS_404" => "N",	// Устанавливать статус 404
                    "SHOW_404" => "N",	// Показ специальной страницы
                    "MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
                ),
                    false
                );?>
            </div>
            <div class="their-work">
                <div class="works-title">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "COMPONENT_TEMPLATE" => ".default",
                            "AREA_FILE_SHOW" => "sect",
                            "AREA_FILE_SUFFIX" => "inc201",
                            "EDIT_TEMPLATE" => "",
                            "AREA_FILE_RECURSIVE" => "Y"
                        )
                    );?>
                </div>
                <?$APPLICATION->IncludeComponent("bitrix:news.list", "theirs_works", Array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "IBLOCK_TYPE" => "simple",	// Тип информационного блока (используется только для проверки)
                    "IBLOCK_ID" => "8",	// Код информационного блока
                    "NEWS_COUNT" => "20",	// Количество новостей на странице
                    "SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
                    "SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
                    "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
                    "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
                    "FILTER_NAME" => "",	// Фильтр
                    "FIELD_CODE" => array(	// Поля
                        0 => "",
                        1 => "",
                    ),
                    "PROPERTY_CODE" => array(	// Свойства
                        0 => "",
                        1 => "",
                    ),
                    "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
                    "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                    "AJAX_MODE" => "N",	// Включить режим AJAX
                    "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                    "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                    "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                    "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
                    "CACHE_TYPE" => "A",	// Тип кеширования
                    "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                    "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
                    "CACHE_GROUPS" => "Y",	// Учитывать права доступа
                    "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
                    "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
                    "SET_BROWSER_TITLE" => "Y",	// Устанавливать заголовок окна браузера
                    "SET_META_KEYWORDS" => "Y",	// Устанавливать ключевые слова страницы
                    "SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
                    "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
                    "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
                    "PARENT_SECTION" => "",	// ID раздела
                    "PARENT_SECTION_CODE" => "",	// Код раздела
                    "INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
                    "DISPLAY_DATE" => "Y",	// Выводить дату элемента
                    "DISPLAY_NAME" => "Y",	// Выводить название элемента
                    "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
                    "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
                    "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
                    "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                    "DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
                    "PAGER_TITLE" => "Новости",	// Название категорий
                    "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
                    "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
                    "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
                    "PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
                    "SET_STATUS_404" => "N",	// Устанавливать статус 404
                    "SHOW_404" => "N",	// Показ специальной страницы
                    "MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
                ),
                    false
                );?>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>



<div class="specialists">
    <div class="container">
        <div class="color-header">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "AREA_FILE_SHOW" => "sect",
                    "AREA_FILE_SUFFIX" => "inc44",
                    "EDIT_TEMPLATE" => "",
                    "AREA_FILE_RECURSIVE" => "Y"
                )
            );?>
        </div>
        <div class="specialists-scontrol">
            <div class="slider-prev"></div>
            <div class="slider-next"></div>
        </div>
        <div class="specialist-slider">
            <?$APPLICATION->IncludeComponent("bitrix:news.list", "specialist", Array(
                "COMPONENT_TEMPLATE" => ".default",
                "IBLOCK_TYPE" => "slider",	// Тип информационного блока (используется только для проверки)
                "IBLOCK_ID" => "10",	// Код информационного блока
                "NEWS_COUNT" => "20",	// Количество новостей на странице
                "SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
                "SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
                "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
                "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
                "FILTER_NAME" => "",	// Фильтр
                "FIELD_CODE" => array(	// Поля
                    0 => "",
                    1 => "",
                ),
                "PROPERTY_CODE" => array(	// Свойства
                    0 => "SEX",
                    1 => "STATE",
                ),
                "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
                "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                "AJAX_MODE" => "N",	// Включить режим AJAX
                "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
                "CACHE_TYPE" => "A",	// Тип кеширования
                "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
                "CACHE_GROUPS" => "Y",	// Учитывать права доступа
                "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
                "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
                "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
                "SET_BROWSER_TITLE" => "Y",	// Устанавливать заголовок окна браузера
                "SET_META_KEYWORDS" => "Y",	// Устанавливать ключевые слова страницы
                "SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
                "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
                "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
                "PARENT_SECTION" => "",	// ID раздела
                "PARENT_SECTION_CODE" => "",	// Код раздела
                "INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
                "DISPLAY_DATE" => "Y",	// Выводить дату элемента
                "DISPLAY_NAME" => "Y",	// Выводить название элемента
                "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
                "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
                "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
                "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                "DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
                "PAGER_TITLE" => "Новости",	// Название категорий
                "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
                "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
                "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
                "PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
                "SET_STATUS_404" => "N",	// Устанавливать статус 404
                "SHOW_404" => "N",	// Показ специальной страницы
                "MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
            ),
                false
            );?>

        </div>
    </div>
</div>
<div class="bottom-block">
    <div class="color-header">
        <?$APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            Array(
                "COMPONENT_TEMPLATE" => ".default",
                "AREA_FILE_SHOW" => "sect",
                "AREA_FILE_SUFFIX" => "inc45",
                "EDIT_TEMPLATE" => "",
                "AREA_FILE_RECURSIVE" => "Y"
            )
        );?>
    </div>
    <div class="container">
        <div class="text-for-form">
            <img src="/bitrix/templates/pikcher/images/big-grid.png" class="abs-image" alt="">
            <div class="grey-arrow">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "COMPONENT_TEMPLATE" => ".default",
                        "AREA_FILE_SHOW" => "sect",
                        "AREA_FILE_SUFFIX" => "inc50",
                        "EDIT_TEMPLATE" => "",
                        "AREA_FILE_RECURSIVE" => "Y"
                    )
                );?>
            </div>
            <div class="texts">
                <div class="text-big1">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "COMPONENT_TEMPLATE" => ".default",
                            "AREA_FILE_SHOW" => "sect",
                            "AREA_FILE_SUFFIX" => "inc46",
                            "EDIT_TEMPLATE" => "",
                            "AREA_FILE_RECURSIVE" => "Y"
                        )
                    );?>
                </div>
                <div class="text-smallest">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "COMPONENT_TEMPLATE" => ".default",
                            "AREA_FILE_SHOW" => "sect",
                            "AREA_FILE_SUFFIX" => "inc47",
                            "EDIT_TEMPLATE" => "",
                            "AREA_FILE_RECURSIVE" => "Y"
                        )
                    );?>
                </div>
                <div class="text-small">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "COMPONENT_TEMPLATE" => ".default",
                            "AREA_FILE_SHOW" => "sect",
                            "AREA_FILE_SUFFIX" => "inc48",
                            "EDIT_TEMPLATE" => "",
                            "AREA_FILE_RECURSIVE" => "Y"
                        )
                    );?>
                </div>
                <div class="text-big2">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "COMPONENT_TEMPLATE" => ".default",
                            "AREA_FILE_SHOW" => "sect",
                            "AREA_FILE_SUFFIX" => "inc49",
                            "EDIT_TEMPLATE" => "",
                            "AREA_FILE_RECURSIVE" => "Y"
                        )
                    );?>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-wraper">
            <form method="post"  action="" id="ajaxform2" class="send-form" enctype="multipart/form-data">
                <div class="form-header">заполните форму</div>
                <div class="form-small-text">и мы вышлем Вам эскиз вашей картины БЕСПЛАТНО!</div>
                <input type="text" class="form-input" name="name" placeholder="Ваше имя">

                <div class="help-text">Для связи с Вами</div>
                <div class="clearfix"></div>
                <input type="text" class="form-input" name="phone" placeholder="Ваш телефон">

                <div class="help-text">Для связи с Вами</div>
                <div class="clearfix"></div>
                <input type="email" class="form-input" name="email" placeholder="Ваш e-mail">

                <div class="help-text">Чтобы отправить Вам готовый эскиз картины</div>
                <div class="clearfix"></div>
                <div class="header-for-upload">Мы не берем предоплаты за изготовление картины!</div>
                <div class="upload-button">
                    <input id="file_s" class="real-button" name="file" type="file">
                    <input class="uploaded-name" type="text">
                    <button class="fake-button">Загрузить файл</button>
                </div>
                <button type="submit" class="send-button">ПОЛУЧИТЬ ЭСКИЗ</button>
                <div class="privacy">Политика конфиденциальности</div>
            </form>
        </div>
        <div class="clearfix"></div>
    </div>
</div><?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>

