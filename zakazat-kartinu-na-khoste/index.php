<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Заказать картину на хосте");
?>
<?php if (isset($_GET['size'])) {
    $res = $_GET['size'];
} else {
    $res = '40x40';
}
?>
<script>
    $(document).ready(function () {
        $('.cs-select').val('<?php  echo $res ?> cm');
    });
</script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/order.js"></script>
<div class="order-block">
    <div class="container">
        <div class="color-header">
            <? $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "AREA_FILE_SHOW" => "sect121414",
                    "AREA_FILE_SUFFIX" => "inc1",
                    "EDIT_TEMPLATE" => "",
                    "AREA_FILE_RECURSIVE" => "Y"
                )
            ); ?>
            </div>
    </div>
    <form method="post" action="" id="ajaxform_order" enctype="multipart/form-data" class="order-form">
        <div class="container">
            <div class="chose-size form-block">
                <div class="fblock-header">
                    <img class="fb-image" src="<?= SITE_TEMPLATE_PATH ?>/images/icons/size-icon.png" alt="">

                    <div class="fb-text">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "AREA_FILE_SHOW" => "sect",
                                "AREA_FILE_SUFFIX" => "inc1",
                                "EDIT_TEMPLATE" => "",
                                "AREA_FILE_RECURSIVE" => "Y"
                            )
                        ); ?>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="chose-size-select">
                    <select class="cs-select" name="size">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:news.list",
                            "rozmer",
                            array(
                                "COMPONENT_TEMPLATE" => "rozmer",
                                "IBLOCK_TYPE" => "simple",
                                "IBLOCK_ID" => "18",
                                "NEWS_COUNT" => "20",
                                "SORT_BY1" => "TIMESTAMP_X",
                                "SORT_ORDER1" => "ASC",
                                "SORT_BY2" => "SORT",
                                "SORT_ORDER2" => "ASC",
                                "FILTER_NAME" => "",
                                "FIELD_CODE" => array(
                                    0 => "",
                                    1 => "",
                                ),
                                "PROPERTY_CODE" => array(
                                    0 => "PRICE",
                                    1 => "HOLST",
                                    2 => "FACTUR",
                                    3 => "BAGET",
                                    4 => "OBROB",
                                    5 => "SHARS",
                                ),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "AJAX_MODE" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N",
                                "AJAX_OPTION_ADDITIONAL" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "CACHE_FILTER" => "N",
                                "CACHE_GROUPS" => "Y",
                                "PREVIEW_TRUNCATE_LEN" => "",
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                "SET_TITLE" => "Y",
                                "SET_BROWSER_TITLE" => "Y",
                                "SET_META_KEYWORDS" => "Y",
                                "SET_META_DESCRIPTION" => "Y",
                                "SET_LAST_MODIFIED" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                                "ADD_SECTIONS_CHAIN" => "Y",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "INCLUDE_SUBSECTIONS" => "Y",
                                "DISPLAY_DATE" => "Y",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "Y",
                                "PAGER_TEMPLATE" => ".default",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "Y",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "PAGER_BASE_LINK_ENABLE" => "N",
                                "SET_STATUS_404" => "N",
                                "SHOW_404" => "N",
                                "MESSAGE_404" => ""
                            ),
                            false
                        ); ?>
                    </select>
                    <input id="start_price" hidden type="text" name="start_price"/>

                    <div class="size-price">= 1300 <i class="fa fa-rub"></i></div>
                </div>
                <div class="check">
                    <a class="fancy2" id="fance_thumbs" href="<?= SITE_TEMPLATE_PATH ?>/images/control-image.png">
                        <img class="control-image" src="<?= SITE_TEMPLATE_PATH ?>/images/control-image.png" alt=""/>
                    </a>

                    <div class="hint">
                        <div class="hint-text">
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "AREA_FILE_SHOW" => "sect",
                                    "AREA_FILE_SUFFIX" => "inc2",
                                    "EDIT_TEMPLATE" => "",
                                    "AREA_FILE_RECURSIVE" => "Y"
                                )
                            ); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="add-photo form-block">
            <div class="container">
                <div class="fblock-header">
                    <img class="fb-image" src="<?= SITE_TEMPLATE_PATH ?>/images/icons/photo-icon.png" alt="">

                    <div class="fb-text">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "AREA_FILE_SHOW" => "sect",
                                "AREA_FILE_SUFFIX" => "inc3",
                                "EDIT_TEMPLATE" => "",
                                "AREA_FILE_RECURSIVE" => "Y"
                            )
                        ); ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="upload-button">
                    <input id="file_s" class="real-button" onchange="app.readImageURL(this)" name="file" type="file">
                    <input class="uploaded-name" type="text" placeholder="Имя файла">
                    <button class="fake-button">Загрузить файл</button>
                    <div class="clearfix"></div>
                </div>
                <div class="hint">
                    <div class="hint-text">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "AREA_FILE_SHOW" => "sect",
                                "AREA_FILE_SUFFIX" => "inc4",
                                "EDIT_TEMPLATE" => "",
                                "AREA_FILE_RECURSIVE" => "Y"
                            )
                        ); ?>

                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="decoration form-block">
                <div class="fblock-header">
                    <img class="fb-image" src="<?= SITE_TEMPLATE_PATH ?>/images/icons/size-icon.png" alt="">

                    <div class="fb-text">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "AREA_FILE_SHOW" => "sect",
                                "AREA_FILE_SUFFIX" => "inc5",
                                "EDIT_TEMPLATE" => "",
                                "AREA_FILE_RECURSIVE" => "Y"
                            )
                        ); ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="accordion">
                    <h3>
                        <div class="price-about">
                            <div class="purple-checkbox">
                                <input type="checkbox" id="canvas" name="canvas" value="0"/>
                                <label for="canvas"></label>
                            </div>
                            <div class="pa-name trigger" data-index="0">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc3000",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </div>
                            <div class="pa-price"> +
                                    <span id="canvas-price-label">

                                    <? $APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        "",
                                        Array(
                                            "COMPONENT_TEMPLATE" => ".default",
                                            "AREA_FILE_SHOW" => "sect",
                                            "AREA_FILE_SUFFIX" => "inc3001",
                                            "EDIT_TEMPLATE" => "",
                                            "AREA_FILE_RECURSIVE" => "Y"
                                        )
                                    ); ?>
                                </span>
                                <i class="fa fa-rub"></i></div>
                            <div class="pa-about trigger" data-index="0">Подробнeе</div>
                            <div class="clearfix"></div>
                        </div>
                    </h3>
                    <div class="canvas">
                        <div class="item item1">
                            <?php
                            $file = file_get_contents('sect_inc9000.php');
                            $spos = strpos($file, 'src="') + 5;
                            $subs = substr($file, $spos);
                            $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                            ?>
                            <a class="fancy2" href="<?= $furl ?>">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc9000",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </a>

                            <div class="hint">
                                <div class="hint-text">
                                    <? $APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        "",
                                        Array(
                                            "COMPONENT_TEMPLATE" => ".default",
                                            "AREA_FILE_SHOW" => "sect",
                                            "AREA_FILE_SUFFIX" => "inc3002",
                                            "EDIT_TEMPLATE" => "",
                                            "AREA_FILE_RECURSIVE" => "Y"
                                        )
                                    ); ?>
                                </div>
                            </div>
                        </div>
                        <div class="item item2">
                            <?php
                            $file = file_get_contents('sect_inc9001.php');
                            $spos = strpos($file, 'src="') + 5;
                            $subs = substr($file, $spos);
                            $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                            ?>
                            <a class="fancy2" href="<?= $furl ?>">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc9001",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </a>

                            <div class="hint">
                                <div class="hint-text">
                                    <? $APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        "",
                                        Array(
                                            "COMPONENT_TEMPLATE" => ".default",
                                            "AREA_FILE_SHOW" => "sect",
                                            "AREA_FILE_SUFFIX" => "inc3003",
                                            "EDIT_TEMPLATE" => "",
                                            "AREA_FILE_RECURSIVE" => "Y"
                                        )
                                    ); ?>

                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <h3>
                        <div class="price-about">
                            <div class="purple-checkbox">
                                <input type="checkbox" id="cover" name="cover" value="600"/>
                                <label for="cover"></label>
                            </div>
                            <div class="pa-name trigger" data-index="1">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc3004",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </div>
                            <div class="pa-price"> +
                                    <span id="cover-price-label">

                                    <? $APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        "",
                                        Array(
                                            "COMPONENT_TEMPLATE" => ".default",
                                            "AREA_FILE_SHOW" => "sect",
                                            "AREA_FILE_SUFFIX" => "inc3005",
                                            "EDIT_TEMPLATE" => "",
                                            "AREA_FILE_RECURSIVE" => "Y"
                                        )
                                    ); ?>
                                    </span>
                                <i class="fa fa-rub"></i></div>
                            <div class="pa-about trigger" data-index="1">Подробнeе</div>
                            <div class="clearfix"></div>
                        </div>
                    </h3>
                    <div class="cover">
                        <div class="item item1">
                            <?php
                            $file = file_get_contents('sect_inc9025.php');
                            $spos = strpos($file, 'src="') + 5;
                            $subs = substr($file, $spos);
                            $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                            ?>
                            <a class="fancy2" href="<?= $furl ?>">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc9025",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </a>
                        </div>
                        <div class="item item2">
                            <?php
                            $file = file_get_contents('sect_inc9002.php');
                            $spos = strpos($file, 'src="') + 5;
                            $subs = substr($file, $spos);
                            $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                            ?>
                            <a class="fancy2" href="<?= $furl ?>">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc9002",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </a>
                            <div class="hint">
                                <div class="hint-text">
                                    <? $APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        "",
                                        Array(
                                            "COMPONENT_TEMPLATE" => ".default",
                                            "AREA_FILE_SHOW" => "sect",
                                            "AREA_FILE_SUFFIX" => "inc3006",
                                            "EDIT_TEMPLATE" => "",
                                            "AREA_FILE_RECURSIVE" => "Y"
                                        )
                                    ); ?>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="frame form-block">
            <div class="container">
                <div class="fblock-header">
                    <img class="fb-image" src="<?= SITE_TEMPLATE_PATH ?>/images/icons/frame-icon.png" alt="">

                    <div class="fb-text">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "AREA_FILE_SHOW" => "sect",
                                "AREA_FILE_SUFFIX" => "inc6",
                                "EDIT_TEMPLATE" => "",
                                "AREA_FILE_RECURSIVE" => "Y"
                            )
                        ); ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="accordion check-control">
                    <h3>
                        <div class="price-about">
                            <div class="purple-checkbox cb-to-control">
                                <input type="checkbox" id="frame" name="frame" value="0"/>
                                <label for="frame"></label>
                            </div>
                            <div class="pa-name trigger" data-index="0">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc3007",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </div>
                            <div class="pa-price">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc3008",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?></div>
                            <div class="pa-about trigger" data-index="0">Подробнeе</div>
                            <div class="clearfix"></div>
                        </div>
                    </h3>
                    <div class="full-frame">
                        <div class="item item1">
                            <?php
                            $file = file_get_contents('sect_inc9003.php');
                            $spos = strpos($file, 'src="') + 5;
                            $subs = substr($file, $spos);
                            $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                            ?>
                            <a class="fancy2" href="<?= $furl ?>">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc9003",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </a>
                        </div>
                        <div class="item">
                            <?php
                            $file = file_get_contents('sect_inc9004.php');
                            $spos = strpos($file, 'src="') + 5;
                            $subs = substr($file, $spos);
                            $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                            ?>
                            <a class="fancy2" href="<?= $furl ?>">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc9004",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </a>
                            <div class="hint">
                                <div class="hint-text">
                                    <? $APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        "",
                                        Array(
                                            "COMPONENT_TEMPLATE" => ".default",
                                            "AREA_FILE_SHOW" => "sect",
                                            "AREA_FILE_SUFFIX" => "inc3009",
                                            "EDIT_TEMPLATE" => "",
                                            "AREA_FILE_RECURSIVE" => "Y"
                                        )
                                    ); ?>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <h3>
                        <div class="price-about">
                            <div class="purple-checkbox cb-to-control">
                                <input type="checkbox" id="baget" name="baget" value="600"/>
                                <label for="baget"></label>
                            </div>
                            <div class="pa-name trigger" data-index="1">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc3010",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </div>
                            <div class="pa-price"> +
                                    <span id="baget-price-label">
                                    <? $APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        "",
                                        Array(
                                            "COMPONENT_TEMPLATE" => ".default",
                                            "AREA_FILE_SHOW" => "sect",
                                            "AREA_FILE_SUFFIX" => "inc3011",
                                            "EDIT_TEMPLATE" => "",
                                            "AREA_FILE_RECURSIVE" => "Y"
                                        )
                                    ); ?>
                                    </span>
                                <i class="fa fa-rub"></i></div>
                            <div class="pa-about trigger" data-index="1">Подробнeе</div>
                            <div class="clearfix"></div>
                        </div>
                    </h3>
                    <div class="corner-frame check-control">
                        <div class="item">
                            <div class="purple-checkbox cb-to-control parent-on">
                                <input type="checkbox" id="baget1" name="baget1" value="600"/>
                                <label for="baget1"></label>
                            </div>
                            <?php
                            $file = file_get_contents('sect_inc9005.php');
                            $spos = strpos($file, 'src="') + 5;
                            $subs = substr($file, $spos);
                            $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                            ?>
                            <a class="fancy2" href="<?= $furl ?>">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc9005",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </a>
                        </div>
                        <div class="item">
                            <div class="purple-checkbox cb-to-control parent-on">
                                <input type="checkbox" id="baget2" name="baget2" value="600"/>
                                <label for="baget2"></label>
                            </div>
                            <?php
                            $file = file_get_contents('sect_inc9006.php');
                            $spos = strpos($file, 'src="') + 5;
                            $subs = substr($file, $spos);
                            $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                            ?>
                            <a class="fancy2" href="<?= $furl ?>">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc9006",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </a>
                        </div>
                        <div class="item">
                            <div class="purple-checkbox cb-to-control parent-on">
                                <input type="checkbox" id="baget3" name="baget3" value="600"/>
                                <label for="baget3"></label>
                            </div>
                            <?php
                            $file = file_get_contents('sect_inc9007.php');
                            $spos = strpos($file, 'src="') + 5;
                            $subs = substr($file, $spos);
                            $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                            ?>
                            <a class="fancy2" href="<?= $furl ?>">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc9007",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </a>
                        </div>
                        <div class="item">
                            <div class="purple-checkbox cb-to-control parent-on">
                                <input type="checkbox" id="baget4" name="baget4" value="600"/>
                                <label for="baget4"></label>
                            </div>
                            <?php
                            $file = file_get_contents('sect_inc9008.php');
                            $spos = strpos($file, 'src="') + 5;
                            $subs = substr($file, $spos);
                            $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                            ?>
                            <a class="fancy2" href="<?= $furl ?>">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc9008",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </a>
                        </div>
                        <div class="item">
                            <div class="purple-checkbox cb-to-control parent-on">
                                <input type="checkbox" id="baget5" name="baget5" value="600"/>
                                <label for="baget5"></label>
                            </div>
                            <?php
                            $file = file_get_contents('sect_inc90071.php');
                            $spos = strpos($file, 'src="') + 5;
                            $subs = substr($file, $spos);
                            $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                            ?>
                            <a class="fancy2" href="<?= $furl ?>">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc90071",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </a>
                        </div>
                        <div class="item">
                            <div class="purple-checkbox cb-to-control parent-on">
                                <input type="checkbox" id="baget6" name="baget6" value="600"/>
                                <label for="baget6"></label>
                            </div>
                            <?php
                            $file = file_get_contents('sect_inc90072.php');
                            $spos = strpos($file, 'src="') + 5;
                            $subs = substr($file, $spos);
                            $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                            ?>
                            <a class="fancy2" href="<?= $furl ?>">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc90072",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </a>
                        </div>
                        <div class="item">
                            <div class="purple-checkbox cb-to-control parent-on">
                                <input type="checkbox" id="baget7" name="baget7" value="600"/>
                                <label for="baget7"></label>
                            </div>
                            <?php
                            $file = file_get_contents('sect_inc90073.php');
                            $spos = strpos($file, 'src="') + 5;
                            $subs = substr($file, $spos);
                            $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                            ?>
                            <a class="fancy2" href="<?= $furl ?>">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc90073",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </a>
                        </div>
                        <div class="item">
                            <div class="purple-checkbox cb-to-control parent-on">
                                <input type="checkbox" id="baget8" name="baget8" value="600"/>
                                <label for="baget8"></label>
                            </div>
                            <?php
                            $file = file_get_contents('sect_inc90074.php');
                            $spos = strpos($file, 'src="') + 5;
                            $subs = substr($file, $spos);
                            $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                            ?>
                            <a class="fancy2" href="<?= $furl ?>">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc90074",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </a>
                        </div>
                        <div class="item">
                            <div class="purple-checkbox cb-to-control parent-on">
                                <input type="checkbox" id="baget9" name="baget9" value="600"/>
                                <label for="baget9"></label>
                            </div>
                            <?php
                            $file = file_get_contents('sect_inc90075.php');
                            $spos = strpos($file, 'src="') + 5;
                            $subs = substr($file, $spos);
                            $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                            ?>
                            <a class="fancy2" href="<?= $furl ?>">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc90075",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </a>
                        </div>
                        <div class="item">
                            <div class="purple-checkbox cb-to-control parent-on">
                                <input type="checkbox" id="baget10" name="baget10" value="600"/>
                                <label for="baget10"></label>
                            </div>
                            <?php
                            $file = file_get_contents('sect_inc90076.php');
                            $spos = strpos($file, 'src="') + 5;
                            $subs = substr($file, $spos);
                            $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                            ?>
                            <a class="fancy2" href="<?= $furl ?>">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc90076",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </a>
                        </div>
                        <div class="item">
                            <div class="purple-checkbox cb-to-control parent-on">
                                <input type="checkbox" id="baget11" name="baget11" value="600"/>
                                <label for="baget11"></label>
                            </div>
                            <?php
                            $file = file_get_contents('sect_inc90077.php');
                            $spos = strpos($file, 'src="') + 5;
                            $subs = substr($file, $spos);
                            $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                            ?>
                            <a class="fancy2" href="<?= $furl ?>">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc90077",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </a>
                        </div>
                        <div class="item">
                            <div class="purple-checkbox cb-to-control parent-on">
                                <input type="checkbox" id="baget12" name="baget12" value="600"/>
                                <label for="baget12"></label>
                            </div>
                            <?php
                            $file = file_get_contents('sect_inc90078.php');
                            $spos = strpos($file, 'src="') + 5;
                            $subs = substr($file, $spos);
                            $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                            ?>
                            <a class="fancy2" href="<?= $furl ?>">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc90078",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </a>
                        </div>
                        <div class="clearfix"></div>
                        <div class="hint hint-above">
                            <div class="hint-text">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc3012",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="processing form-block">
            <div class="container">
                <div class="fblock-header">
                    <img class="fb-image" src="<?= SITE_TEMPLATE_PATH ?>/images/icons/brush-icon.png" alt="">

                    <div class="fb-text">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "AREA_FILE_SHOW" => "sect",
                                "AREA_FILE_SUFFIX" => "inc7",
                                "EDIT_TEMPLATE" => "",
                                "AREA_FILE_RECURSIVE" => "Y"
                            )
                        ); ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="accordion check-control">
                    <h3>
                        <div class="price-about">
                            <div class="purple-checkbox cb-to-control">
                                <input type="checkbox" id="correction" name="correction" value="0"/>
                                <label for="correction"></label>
                            </div>
                            <div class="pa-name trigger" data-index="0">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc3013",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </div>
                            <div class="pa-price">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc3014",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </div>
                            <div class="pa-about trigger" data-index="0">Подробнeе</div>
                            <div class="clearfix"></div>
                        </div>
                    </h3>
                    <div class="correction">
                        <div class="item">
                            <?php
                            $file = file_get_contents('sect_inc9010.php');
                            $spos = strpos($file, 'src="') + 5;
                            $subs = substr($file, $spos);
                            $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                            ?>
                            <a class="fancy2" href="<?= $furl ?>">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc9010",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </a>
                        </div>
                        <div class="item">
                            <?php
                            $file = file_get_contents('sect_inc9011.php');
                            $spos = strpos($file, 'src="') + 5;
                            $subs = substr($file, $spos);
                            $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                            ?>
                            <a class="fancy2" href="<?= $furl ?>">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc9011",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </a>

                            <div class="hint">
                                <div class="hint-text">
                                    <? $APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        "",
                                        Array(
                                            "COMPONENT_TEMPLATE" => ".default",
                                            "AREA_FILE_SHOW" => "sect",
                                            "AREA_FILE_SUFFIX" => "inc3015",
                                            "EDIT_TEMPLATE" => "",
                                            "AREA_FILE_RECURSIVE" => "Y"
                                        )
                                    ); ?>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <h3>
                        <div class="price-about">
                            <div class="purple-checkbox cb-to-control">
                                <input type="checkbox" id="art-processing" name="art-processing" value="600"/>
                                <label for="art-processing"></label>
                            </div>
                            <div class="pa-name trigger" data-index="1">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc3016",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </div>
                            <div class="pa-price">+
            <span id="art-processing-price-label">
            <? $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "AREA_FILE_SHOW" => "sect",
                    "AREA_FILE_SUFFIX" => "inc3017",
                    "EDIT_TEMPLATE" => "",
                    "AREA_FILE_RECURSIVE" => "Y"
                )
            ); ?>
            </span>
                                <i class="fa fa-rub"></i></div>
                            <div class="pa-about trigger" data-index="1">Подробнeе</div>
                            <div class="clearfix"></div>
                        </div>
                    </h3>
                    <div class="art-processing">
                        <div class="item">
                            <?php
                            $file = file_get_contents('sect_inc9012.php');
                            $spos = strpos($file, 'src="') + 5;
                            $subs = substr($file, $spos);
                            $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                            ?>
                            <a class="fancy2" href="<?= $furl ?>">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc9012",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </a>
                        </div>
                        <div class="item">
                            <?php
                            $file = file_get_contents('sect_inc9013.php');
                            $spos = strpos($file, 'src="') + 5;
                            $subs = substr($file, $spos);
                            $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                            ?>
                            <a class="fancy2" href="<?= $furl ?>">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc9013",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </a>
                        </div>
                        <div class="item">
                            <?php
                            $file = file_get_contents('sect_inc9014.php');
                            $spos = strpos($file, 'src="') + 5;
                            $subs = substr($file, $spos);
                            $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                            ?>
                            <a class="fancy2" href="<?= $furl ?>">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc9014",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </a>
                        </div>
                        <div class="item">
                            <?php
                            $file = file_get_contents('sect_inc9015.php');
                            $spos = strpos($file, 'src="') + 5;
                            $subs = substr($file, $spos);
                            $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                            ?>
                            <a class="fancy2" href="<?= $furl ?>">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc9015",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </a>
                        </div>
                        <div class="item">
                            <?php
                            $file = file_get_contents('sect_inc90165.php');
                            $spos = strpos($file, 'src="') + 5;
                            $subs = substr($file, $spos);
                            $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                            ?>
                            <a class="fancy2" href="<?= $furl ?>">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc90165",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </a>
                        </div>
                        <div class="item">
                            <?php
                            $file = file_get_contents('sect_inc901212.php');
                            $spos = strpos($file, 'src="') + 5;
                            $subs = substr($file, $spos);
                            $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                            ?>
                            <a class="fancy2" href="<?= $furl ?>">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc901212",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </a>
                        </div>
                        <div class="clearfix"></div>
                        <div class="hint hint-above">
                            <div class="hint-text">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc3018",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </div>
                        </div>
                    </div>
                    <h3>
                        <div class="price-about">
                            <div class="purple-checkbox cb-to-control">
                                <input type="checkbox" id="art-processing2" name="art-processing2"
                                       value="<?php include "sect_inc3020.php" ?>"/>
                                <label for="art-processing2"></label>
                            </div>
                            <div class="pa-name trigger" data-index="2">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc3019",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </div>
                            <div class="pa-price">+
            <span id="art-processing2-price-label">
            <? $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "AREA_FILE_SHOW" => "sect",
                    "AREA_FILE_SUFFIX" => "inc3020",
                    "EDIT_TEMPLATE" => "",
                    "AREA_FILE_RECURSIVE" => "Y"
                )
            ); ?>
            </span>
                                <i class="fa fa-rub"></i></div>
                            <div class="pa-about trigger" data-index="2">Подробнeе</div>
                            <div class="clearfix"></div>
                        </div>
                    </h3>
                    <div class="caricature">
                        <div class="item">
                            <?php
                            $file = file_get_contents('sect_inc9016.php');
                            $spos = strpos($file, 'src="') + 5;
                            $subs = substr($file, $spos);
                            $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                            ?>
                            <a class="fancy2" href="<?= $furl ?>">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc9016",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </a>
                        </div>
                        <div class="item">
                            <?php
                            $file = file_get_contents('sect_inc9017.php');
                            $spos = strpos($file, 'src="') + 5;
                            $subs = substr($file, $spos);
                            $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                            ?>
                            <a class="fancy2" href="<?= $furl ?>">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc9017",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </a>
                        </div>
                        <div class="item">
                            <?php
                            $file = file_get_contents('sect_inc901711.php');
                            $spos = strpos($file, 'src="') + 5;
                            $subs = substr($file, $spos);
                            $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                            ?>
                            <a class="fancy2" href="<?= $furl ?>">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc901711",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </a>
                        </div>
                        <div class="item">
                            <?php
                            $file = file_get_contents('sect_inc901712.php');
                            $spos = strpos($file, 'src="') + 5;
                            $subs = substr($file, $spos);
                            $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                            ?>
                            <a class="fancy2" href="<?= $furl ?>">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc901712",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </a>
                        </div>
                        <div class="item">
                            <?php
                            $file = file_get_contents('sect_inc901713.php');
                            $spos = strpos($file, 'src="') + 5;
                            $subs = substr($file, $spos);
                            $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                            ?>
                            <a class="fancy2" href="<?= $furl ?>">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc901713",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </a>
                        </div>
                        <div class="item">
                            <?php
                            $file = file_get_contents('sect_inc901714.php');
                            $spos = strpos($file, 'src="') + 5;
                            $subs = substr($file, $spos);
                            $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                            ?>
                            <a class="fancy2" href="<?= $furl ?>">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc901714",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </a>
                        </div>
                        <div class="clearfix"></div>
                        <div class="hint hint-above">
                            <div class="hint-text">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc3021",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="request">
            <div class="container form-block">
                <div class="fblock-header">
                    <img class="fb-image" src="<?= SITE_TEMPLATE_PATH ?>/images/icons/pen-icon.png" alt="">

                    <div class="fb-text">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "AREA_FILE_SHOW" => "sect",
                                "AREA_FILE_SUFFIX" => "inc8",
                                "EDIT_TEMPLATE" => "",
                                "AREA_FILE_RECURSIVE" => "Y"
                            )
                        ); ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="list">
                    <div class="your-name inputs">
                        <input class="small-input" type="text" name="yourname" placeholder="Ваше имя"/>

                        <div class="hint">Как к Вам обращаться</div>
                    </div>
                    <div class="your-phone inputs">
                        <input class="small-input" type="text" name="yourphone" placeholder="Ваш телефон"/>

                        <div class="hint">Для связи с Вами</div>
                    </div>
                    <div class="your-email inputs">
                        <input class="small-input" type="text" name="youremail" placeholder="Ваше e-mail"/>

                        <div class="hint">Чтобы отправить Вам готовый эскиз картины</div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="list">
                    <div class="bottom">
                <textarea class="response" name="response" id="message" cols="30" rows="10"
                          placeholder="Здесь Вы можете добавить Ваши пожелания"></textarea>

                        <div class="sum">
                            <div class="sum-text">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc2011",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    )
                                ); ?>

                            </div>
                            <div class="sum-amount"><span class="value">0</span> <i class="fa fa-rub"></i></div>
                        </div>
                        <input hidden id="all_price" type="text" name="all_price"/>
                        <button type="submit" class="purple-button">
                            <a class="pb-link">Отправить заявку</a>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<footer class="op-footer">
    <div class="grey-horizontal">
        <div class="container">
            <? $APPLICATION->IncludeComponent("bitrix:menu", "menu_bottom", Array(
                "COMPONENT_TEMPLATE" => ".default",
                "ROOT_MENU_TYPE" => "top",    // Тип меню для первого уровня
                "MENU_CACHE_TYPE" => "N",    // Тип кеширования
                "MENU_CACHE_TIME" => "3600",    // Время кеширования (сек.)
                "MENU_CACHE_USE_GROUPS" => "Y",    // Учитывать права доступа
                "MENU_CACHE_GET_VARS" => "",    // Значимые переменные запроса
                "MAX_LEVEL" => "1",    // Уровень вложенности меню
                "CHILD_MENU_TYPE" => "left",    // Тип меню для остальных уровней
                "USE_EXT" => "N",    // Подключать файлы с именами вида .тип_меню.menu_ext.php
                "DELAY" => "N",    // Откладывать выполнение шаблона меню
                "ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
            ),
                false
            ); ?>
            <div class="phone-block">
                <div class="phone phone-to-change">8 (495) 761-76-61</div>
                <div class="work-time time-to-change">мы работаем: 9:00 - 20:00</div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="copyright">Copyright PikCher &copy; 2015.Все права защищены</div>
</footer>
</body>
</html>
