<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?>
<script>
    ymaps.ready(init);
    var myMap,
        myPlacemark;

    function init() {
        myMap = new ymaps.Map("map", {
            center: [55.856356, 37.641225],
            zoom: 16
        });

        myPlacemark = new ymaps.Placemark([55.856356, 37.641225], {
            hintContent: 'проезд Русанова 25к1'
        });
        myPlacemark.options.set('preset', 'islands#redIcon');

        myMap.geoObjects.add(myPlacemark);
    }

</script>
<script src="/bitrix/templates/pikcher/js/picture-type_cont.js"></script>
<style>
    footer {
        margin-top: 0px !important;
    }
</style>
<div class="kontakt">
    <div class="picture-type-block">
        <div class="container">
            <div class="bread_crumb">
                <ul>
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Контакты</a></li>
                </ul>
            </div>
            <div class="page">
                <div class="color-header">
                    Наши контакты: <span id="cont-city" class="text-bold">Москва</span>
                </div>
            </div>
            <div class="addr_box">
                <div class="cont_line">
                    <img src="/bitrix/templates/pikcher/images/contakt/phone.jpg" alt="" class="a-image">

                    <div class="cont_text">
                        <a href="tel=84957617661" id="ph1">8 (495) 761-76-61</a> <a href="tel=88005500117" id="ph2">8
                            (800) 55-00-117</a>
                    </div>
                </div>
                <div class="cont_line">
                    <img src="/bitrix/templates/pikcher/images/contakt/post.jpg" alt="" class="a-image">

                    <div class="cont_text">
                        <a class="mails" href="mailto:radost@pikcher.me">radost@pikcher.me</a> <a class="mails"
                                                                                                  href="mailto:pikcher.me@gmail.com">pikcher.me@gmail.com</a>
                    </div>
                </div>
                <div class="cont_line">
                    <img src="/bitrix/templates/pikcher/images/contakt/pin.jpg" alt="" class="a-image">

                    <div class="cont_text">
                        <p id="adr_main">
                            м. Свиблово, проезд Русанова д. 25 корп. 1
                        </p>
                    </div>
                </div>
                <div class="cont_line">
                    <img src="/bitrix/templates/pikcher/images/contakt/send.jpg" alt="" class="a-image">

                    <div class="cont_text">
                        <h3>Отправить сообщение</h3>
                    </div>
                </div>
            </div>
            <div class="map_g">
                <div id="map" style="height: 300px">
                </div>
            </div>
            <div class="clearfix">
            </div>
            <div class="cont_form">
                <form  method="post"  action="" id="ajaxform_review">
                    <div class="left_part">
                        <input type="text" name="name" placeholder="Ваше имя">
                        <p>
                            Как к Вам обращаться
                        </p>
                    </div>
                    <div class="right_part">
                        <input type="text" name="email" placeholder="Ваш e-mail">
                        <p>
                            Чтобы мы могли Вам ответить
                        </p>
                    </div>
                    <div class="clearfix">
                    </div>
                    <textarea name="response-text" id="" cols="30" rows="10" placeholder="Напишите Ваше сообщение..."></textarea>
                    <button type="submit" class="send-button">ОТПРАВИТЬ</button>
                </form>
                <script>
                    $(document).ready(function() {
                        jQuery('form').on('focusout', 'input', function () {
                            jQuery(this).css('border','none');
                        });
                        var body = jQuery('body');
                        $("#ajaxform_review").submit(function(e){
                            e.preventDefault();
                            var form = $(this);
                            var error = false;
                            form.find('input, textarea').each( function(){
                                if ($(this).val() == '') {
                                    $(this).css('border','1px solid red');
                                    error = true; // oшибкa
                                }
                            });

                            if (!error) {
                                var fd = new FormData();
                                fd.append('name', $(this).find('input[name=name]').val());
                                fd.append('email', $(this).find('input[name=email]').val());
                                fd.append('comment', $(this).find('textarea[name=response-text]').val());
                                $.ajax({
                                    type: 'POST', // oтпрaвляeм в POST фoрмaтe, мoжнo GET
                                    url: '<?=SITE_TEMPLATE_PATH?>/email-contact.php', // путь дo oбрaбoтчикa, у нaс oн лeжит в тoй жe пaпкe
                                    processData: false,
                                    contentType: false,
                                    dataType: 'json', // oтвeт ждeм в json фoрмaтe
                                    data: fd, // дaнныe для oтпрaвки
                                    beforeSend: function(data) { // сoбытиe дo oтпрaвки
                                        form.find('input[type="submit"]').attr('disabled', 'disabled'); // нaпримeр, oтключим кнoпку, чтoбы нe жaли пo 100 рaз
                                    },
                                    success: function(data){ // сoбытиe пoслe удaчнoгo oбрaщeния к сeрвeру и пoлучeния oтвeтa
                                        form.find('.close-form').click();
                                        var sucForm = jQuery('.popup-success');
                                        sucForm.toggle();
                                        body.css({"overflow": "hidden"});
                                    },
                                    error: function (xhr, ajaxOptions, thrownError) { // в случae нeудaчнoгo зaвeршeния зaпрoсa к сeрвeру
                                        alert(xhr.status); // пoкaжeм oтвeт сeрвeрa
                                        alert(thrownError); // и тeкст oшибки
                                    },
                                    complete: function(data) { // сoбытиe пoслe любoгo исхoдa
                                        form.find('input[type="submit"]').prop('disabled', false); // в любoм случae включим кнoпку oбрaтнo
                                    }

                                });
                            }
                            return false; // вырубaeм стaндaртную oтпрaвку фoрмы
                        });
                    });
                </script>
            </div>
            <div class="page">
                <div class="color-header">
                    Мы в регионах
                </div>
            </div>
            <script>
                $(document).ready(function () {
                    $('.box').click(function (e) {
                        e.preventDefault();
                        debugger;
                        var tel1 = $(this).find('.tel1').html();
                        var tel2 = $(this).find('.tel2').html();
                        var adr = $(this).find('.adr_astr').html();
                        var adr_hor = parseFloat($(this).find('.adr_hor').html());
                        var adr_ver = parseFloat($(this).find('.adr_ver').html());
                        var pin = $(this).find('.pin_text').html();

                        $('#cont-city').html($(this).find('p').html());
                        $('#ph1').html(tel1);
                        $('#ph2').html(tel2);
                        $('#adr_main').html(adr);
                        $('#adr_main').html(adr);
                        myMap.geoObjects.remove(myPlacemark);
                        myPlacemark = new ymaps.Placemark([adr_ver, adr_hor], {
                            hintContent: pin
                        });
                        myPlacemark.options.set('preset', 'islands#redIcon');
                        myMap.geoObjects.add(myPlacemark);
                        myMap.setCenter([adr_ver, adr_hor]);
                        $( "body" ).scrollTop( 100 );
                    })
                });
            </script>
            <? $APPLICATION->IncludeComponent("bitrix:news.list", "contacts", Array(
                "COMPONENT_TEMPLATE" => ".default",
                "IBLOCK_TYPE" => "-",    // Тип информационного блока (используется только для проверки)
                "IBLOCK_ID" => "25",    // Код информационного блока
                "NEWS_COUNT" => "60",    // Количество новостей на странице
                "SORT_BY1" => "ACTIVE_FROM",    // Поле для первой сортировки новостей
                "SORT_ORDER1" => "DESC",    // Направление для первой сортировки новостей
                "SORT_BY2" => "SORT",    // Поле для второй сортировки новостей
                "SORT_ORDER2" => "ASC",    // Направление для второй сортировки новостей
                "FILTER_NAME" => "",    // Фильтр
                "FIELD_CODE" => array(    // Поля
                    0 => "",
                    1 => "",
                ),
                "PROPERTY_CODE" => array(    // Свойства
                    0 => "TEL1",
                    1 => "ADRESS",
                    2 => "TEL1",
                    3 => "TEL2",
                    4 => "LAT",
                    5 => "LAN",
                ),
                "CHECK_DATES" => "Y",    // Показывать только активные на данный момент элементы
                "DETAIL_URL" => "",    // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                "AJAX_MODE" => "N",    // Включить режим AJAX
                "AJAX_OPTION_JUMP" => "N",    // Включить прокрутку к началу компонента
                "AJAX_OPTION_STYLE" => "Y",    // Включить подгрузку стилей
                "AJAX_OPTION_HISTORY" => "N",    // Включить эмуляцию навигации браузера
                "AJAX_OPTION_ADDITIONAL" => "",    // Дополнительный идентификатор
                "CACHE_TYPE" => "A",    // Тип кеширования
                "CACHE_TIME" => "36000000",    // Время кеширования (сек.)
                "CACHE_FILTER" => "N",    // Кешировать при установленном фильтре
                "CACHE_GROUPS" => "Y",    // Учитывать права доступа
                "PREVIEW_TRUNCATE_LEN" => "",    // Максимальная длина анонса для вывода (только для типа текст)
                "ACTIVE_DATE_FORMAT" => "d.m.Y",    // Формат показа даты
                "SET_TITLE" => "Y",    // Устанавливать заголовок страницы
                "SET_BROWSER_TITLE" => "Y",    // Устанавливать заголовок окна браузера
                "SET_META_KEYWORDS" => "Y",    // Устанавливать ключевые слова страницы
                "SET_META_DESCRIPTION" => "Y",    // Устанавливать описание страницы
                "SET_LAST_MODIFIED" => "N",    // Устанавливать в заголовках ответа время модификации страницы
                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",    // Включать инфоблок в цепочку навигации
                "ADD_SECTIONS_CHAIN" => "Y",    // Включать раздел в цепочку навигации
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",    // Скрывать ссылку, если нет детального описания
                "PARENT_SECTION" => "",    // ID раздела
                "PARENT_SECTION_CODE" => "",    // Код раздела
                "INCLUDE_SUBSECTIONS" => "Y",    // Показывать элементы подразделов раздела
                "DISPLAY_DATE" => "Y",    // Выводить дату элемента
                "DISPLAY_NAME" => "Y",    // Выводить название элемента
                "DISPLAY_PICTURE" => "Y",    // Выводить изображение для анонса
                "DISPLAY_PREVIEW_TEXT" => "Y",    // Выводить текст анонса
                "PAGER_TEMPLATE" => ".default",    // Шаблон постраничной навигации
                "DISPLAY_TOP_PAGER" => "N",    // Выводить над списком
                "DISPLAY_BOTTOM_PAGER" => "Y",    // Выводить под списком
                "PAGER_TITLE" => "Новости",    // Название категорий
                "PAGER_SHOW_ALWAYS" => "N",    // Выводить всегда
                "PAGER_DESC_NUMBERING" => "N",    // Использовать обратную навигацию
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",    // Время кеширования страниц для обратной навигации
                "PAGER_SHOW_ALL" => "N",    // Показывать ссылку "Все"
                "PAGER_BASE_LINK_ENABLE" => "N",    // Включить обработку ссылок
                "SET_STATUS_404" => "N",    // Устанавливать статус 404
                "SHOW_404" => "N",    // Показ специальной страницы
                "MESSAGE_404" => "",    // Сообщение для показа (по умолчанию из компонента)
            ),
                false
            ); ?><br>
        </div>
    </div>
    <div class="cont_slider">
        <div class="slider">
            <div class="container">
                <div class="color-header">
                    Наше производство
                </div>
                <div class="slider-control">
                    <div class="slider-prev">
                    </div>
                    <div class="slider-next">
                    </div>
                </div>
            </div>
            <div class="slider-opacity">
                <div class="left-side">
                </div>
                <div class="right-side">
                </div>
            </div>
            <div class="two-rows-carousel">
                <div class="item">
                    <img alt="1" src="/bitrix/templates/pikcher/images/contakt/owl1.jpg">
                </div>
                <div class="item">
                    <img alt="1" src="/bitrix/templates/pikcher/images/contakt/owl2.jpg">
                </div>
                <div class="item">
                    <img alt="1" src="/bitrix/templates/pikcher/images/contakt/owl3.jpg">
                </div>
                <div class="item">
                    <img alt="1" src="/bitrix/templates/pikcher/images/contakt/owl1.jpg">
                </div>
                <div class="item">
                    <img alt="1" src="/bitrix/templates/pikcher/images/contakt/owl2.jpg">
                </div>
                <div class="item">
                    <img alt="1" src="/bitrix/templates/pikcher/images/contakt/owl3.jpg">
                </div>
                <div class="item">
                    <img alt="1" src="/bitrix/templates/pikcher/images/contakt/owl2.jpg">
                </div>
                <div class="item">
                    <img alt="1" src="/bitrix/templates/pikcher/images/contakt/owl3.jpg">
                </div>
                <div class="item">
                    <img alt="1" src="/bitrix/templates/pikcher/images/contakt/owl1.jpg">
                </div>
                <div class="item">
                    <img alt="1" src="/bitrix/templates/pikcher/images/contakt/owl2.jpg">
                </div>
                <div class="item">
                    <img alt="1" src="/bitrix/templates/pikcher/images/contakt/owl3.jpg">
                </div>
                <div class="item">
                    <img alt="1" src="/bitrix/templates/pikcher/images/contakt/owl1.jpg">
                </div>
                <div class="item">
                    <img alt="1" src="/bitrix/templates/pikcher/images/contakt/owl1.jpg">
                </div>
                <div class="item">
                    <img alt="1" src="/bitrix/templates/pikcher/images/contakt/owl2.jpg">
                </div>
                <div class="item">
                    <img alt="1" src="/bitrix/templates/pikcher/images/contakt/owl3.jpg">
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="op-footer">
    <div class="grey-horizontal">
        <div class="container">

            <ul class="nav-phone">

                <li class="nav-item"><a class="nav-link" href="/catalog">Каталог картин</a></li>

                <li class="nav-item"><a href="/review" class="nav-link">Отзывы</a></li>

                <li class="nav-item"><a class="nav-link" href="/delivery">Оплата и доставка</a></li>

            </ul>
            <div class="phone-block">
                <div class="phone phone-to-change">8 (495) 761-76-61</div>
                <div class="work-time time-to-change">мы работаем: 9:00 - 20:00</div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="copyright">Copyright PikCher © 2015.Все права защищены</div>
</footer>