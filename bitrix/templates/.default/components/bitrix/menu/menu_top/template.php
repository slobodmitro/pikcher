<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
    <ul class="menu">

        <?
        foreach($arResult as $arItem):
            if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
                continue;
            ?>
            <?if($arItem["SELECTED"]):?>
            <li class="menu-item nice-hosver"><a href="<?=$arItem["LINK"]?>" class="link mi-active-me"><?=$arItem["TEXT"]?></a></li>
        <?else:?>
            <li class="menu-item nice-hover"><a  class="link" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
        <?endif?>

        <?endforeach?>
    </ul>
<?endif?>