<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div class="response">
                    <div class="left-side">
                        <div class="image-wraper">
                            <a class="fancy" href="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>">
                                <img class="image" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>">
                            </a>
                        </div>
                    </div>
                    <div class="right-side">
                        <div class="author">
                            <div class="name"><?echo $arItem["NAME"]?></div>
                            <div class="city">(<?php echo $arItem['PROPERTIES']['CITY']['VALUE']; ?>)</div>
                            <div class="date"><?php echo $arItem['PROPERTIES']['DATA']['VALUE']; ?></div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="response-text">
                            <?echo $arItem["PREVIEW_TEXT"];?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

