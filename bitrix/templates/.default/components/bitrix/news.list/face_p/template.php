<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	 <div class="transform-block">
                            <a class="fancy" href="<?php echo $arItem['DISPLAY_PROPERTIES']['BEFORE']['FILE_VALUE']['SRC']; ?>" rel="tab-group1">
                                <div class="photo-before">
                                    <img class="image" src="<?php echo $arItem['DISPLAY_PROPERTIES']['BEFORE']['FILE_VALUE']['SRC']; ?>" alt="">
                                </div>
                                <img class="pink-arrow" src="<?=SITE_TEMPLATE_PATH?>/images/big-pink-arrow.png" alt="">
                                <div class="photo-after">
                                    <img src="<?php echo $arItem['DISPLAY_PROPERTIES']['AFTER']['FILE_VALUE']['SRC']; ?>" alt="">
                                </div>
                            </a>
    </div>
<?endforeach;?>
