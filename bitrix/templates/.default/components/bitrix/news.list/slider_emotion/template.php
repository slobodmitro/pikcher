<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>

<?$arr_img=array_chunk($arResult["ITEMS"],2) ;
foreach($arr_img as $arItem):?>
	<?
	$this->AddEditAction($arItem[0]['ID'], $arItem[0]['EDIT_LINK'], CIBlock::GetArrayByID($arItem[0]["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem[0]['ID'], $arItem[0]['DELETE_LINK'], CIBlock::GetArrayByID($arItem[0]["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div>
            <a class="fancy" href="<?php echo $arItem[0]["PREVIEW_PICTURE"]["SRC"]; ?>" rel="group">
                <div class="image" style="background: url('<?php echo $arItem[0]["PREVIEW_PICTURE"]["SRC"]; ?>') center center"></div>
            </a>
            <a class="fancy" href="<?php echo $arItem[1]["PREVIEW_PICTURE"]["SRC"]; ?>" rel="group">
                <div class="image" style="background: url('<?php echo $arItem[1]["PREVIEW_PICTURE"]["SRC"]; ?>') center center"></div>
            </a>
    </div>
<?endforeach;?>

