<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<? $i=0;
foreach($arResult["ITEMS"] as $arItem):?>
	<?
    $i++;
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	 <div class="cycle-stage">
                    <div class="image-wraper">
                        <div class="num-place">0<?php echo $i; ?>.</div>
                        <div class="image" style="background-image: url('<?php echo $arItem["PREVIEW_PICTURE"]["SRC"]; ?>')"></div>
                        </div>
                    <div class="stage-text">
                        <?echo $arItem["PREVIEW_TEXT"];?>
                    </div>
     </div>
<?endforeach;?>
<div class="clearfix"></div>

