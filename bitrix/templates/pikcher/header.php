<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?><!DOCTYPE html>
        <html>
        <head lang="en">
            <?$APPLICATION->ShowHead();?>
            <title><?$APPLICATION->ShowTitle();?></title>
            <meta http-equiv="x-ua-compatible" content="ie=edge">
            <meta name="description" content="">
            <meta name="MobileOptimized" content="320"/>
            <meta name="HandheldFriendly" content="true"/>
            <meta name="viewport" content="width=1100, initial-scale=1.0, maximum-scale=1.0">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
            <script src="<?=SITE_TEMPLATE_PATH?>/vendors/modernizr-2.8.3.min.js"></script>
            <link href="<?=SITE_TEMPLATE_PATH?>/css/normalize.css" type="text/css" rel="stylesheet">
            <script src="<?=SITE_TEMPLATE_PATH?>/js/jcf.js"></script>
            <script src="<?=SITE_TEMPLATE_PATH?>/js/jcf.select.js"></script>
<!--            <script src="--><?//=SITE_TEMPLATE_PATH?><!--/js/jcf..js"></script>-->
            <link href="<?=SITE_TEMPLATE_PATH?>/css/main.2.css" type="text/css" rel="stylesheet">
            <link href="<?=SITE_TEMPLATE_PATH?>/css/main.css" type="text/css" rel="stylesheet">
            <script src="<?=SITE_TEMPLATE_PATH?>/js/main.js"></script>
            <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/jcf.css"/>
            <script src="<?=SITE_TEMPLATE_PATH?>/vendors/jquery-ui-1.11.4/jquery-ui.js"></script>
            <link href="<?=SITE_TEMPLATE_PATH?>/vendors/jquery-ui-1.11.4/jquery-ui.css" type="text/css" rel="stylesheet">
            <link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/vendors/owl.carousel.2/assets/owl.carousel.css"/>
            <script src="<?=SITE_TEMPLATE_PATH?>/vendors/owl.carousel.2/owl.carousel.js"></script>
            <link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/vendors/fancybox/jquery.fancybox.css"/>
            <script src="<?=SITE_TEMPLATE_PATH?>/vendors/fancybox/jquery.fancybox.js"></script>
            <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
            <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>

            <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65774031-1', 'auto');
  ga('send', 'pageview');

</script>
            <script>
                $(document).ready(function(){
                $('.alphabet ul li a').click(function(e){
                    e.preventDefault();
                    $('.alphabet ul li a').removeClass('red_text');
                    $(this).addClass('red_text');
                });
                })
            </script>

          </head>

        <style>
            .bx-context-toolbar-empty-area {
                position: relative;
                z-index: 30;
            }
            .hands div{
                position: relative;
                z-index: 30;
            }
            .olp1 .pluso{
                position: fixed !important;
                top: 100px;
                z-index: 99999 !important;
            }
        </style>
        <script>
            app.base_url='<?php echo SITE_TEMPLATE_PATH ?>';
        </script>
        <body>
        <div class="text_bod">
            сохранить, чтобы не забыть
        </div>
              <div class="adress_change">
            <div class="close_form"></div>
            <p>Мы доставляем наш товар <br/>
                в <span>3506</span> населЁнных пунктов росии</p>
            <div class="inp_button">
                <input type="text" placeholder="Начните вводить название города"/>
                <a href="#">СОХРАНИТЬ</a>
                <div class="clearfix"></div>
            </div>
            <div class="little_green_text">Выбор города</div>
            <div class="alphabet">
                <ul>
                    <li><a href="#">А</a></li>
                    <li><a href="#">Б</a></li>
                    <li><a href="#">В</a></li>
                    <li><a href="#">Г</a></li>
                    <li><a href="#">Д</a></li>
                    <li><a href="#">Е</a></li>
                    <li><a href="#">Ж</a></li>
                    <li><a href="#">З</a></li>
                    <li><a href="#">И</a></li>
                    <li><a href="#">Й</a></li>
                    <li><a href="#">К</a></li>
                    <li><a href="#">Л</a></li>
                    <li><a href="#">М</a></li>
                    <li><a href="#">Н</a></li>
                    <li><a href="#">О</a></li>
                    <li><a href="#">П</a></li>
                    <li><a href="#">Р</a></li>
                    <li><a href="#">С</a></li>
                    <li><a href="#">Т</a></li>
                    <li><a href="#">У</a></li>
                    <li><a href="#">Ф</a></li>
                    <li><a href="#">Х</a></li>
                    <li><a href="#">Ч</a></li>
                    <li><a href="#">Ш</a></li>
                    <li><a href="#">Щ</a></li>
                    <li><a href="#">Э</a></li>
                    <li><a href="#">Ю</a></li>
                    <li><a href="#">Я</a></li>
                </ul>
                <a href="#" class="reset_a">
                    Сбросить фильтр по букве
                </a>
            </div>
            <div class="towns_in">
                <ul>
                    <li><a href="#">Москва и МО</a></li>
                    <li><a href="#">Санкт-Петербуг и ЛО</a></li>
                    <li><a href="#">Алтайский край</a></li>
                    <li><a href="#">Архангелськая обл.</a></li>
                    <li><a href="#">Астраханская обл.</a></li>
                    <li><a href="#">Брянская обл.</a></li>
                    <li><a href="#">Мурманская обл.</a></li>
                    <li><a href="#">Нижегородская обл.</a></li>
                    <li><a href="#">Омская обл.</a></li>
                    <li><a href="#">Оренбургская обл.</a></li>
                    <li><a href="#">Орловская обл.</a></li>
                    <li><a href="#">Республика Хакасия</a></li>
                    <li><a href="#">Ростовская обл.</a></li>
                    <li><a href="#">Разянская обл.</a></li>
                    <li><a href="#">Саратовская обл.</a></li>
                    <li><a href="#">Самарская обл.</a></li>
                    <li><a href="#">Свердловская обл.</a></li>
                    <li><a href="#">Свердловская обл.</a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
        </div>



        <script type="text/javascript">(function() {
                if (window.pluso)if (typeof window.pluso.start == "function") return;
                if (window.ifpluso==undefined) { window.ifpluso = 1;
                    var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                    s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
                    s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
                    var h=d[g]('body')[0];
                    h.appendChild(s);
                }})();</script>
       <div class="olp1">
        <div class="pluso" data-background="#ebebeb" data-options="medium,square,line,vertical,counter,theme=08" data-services="vkontakte,odnoklassniki,facebook,twitter,google,moimir,email,print" data-url="http://pikcher.me/" data-title="ПЕЧАТЬ ФОТО НА ХОЛСТЕ" data-description="ПЕЧАТЬ ФОТО"></div>
       </div>
        <div id="panel">
            <?$APPLICATION->ShowPanel();?>
        </div>
        <a href="#" id="overflow"></a>
        <div class="popup-form form-to-close">
            <div class="form-wraper">
                <form method="post"  action="" id="ajaxform" class="send-form" enctype="multipart/form-data">
                    <div class="form-header">заполните форму</div>
                    <div class="form-small-text">и мы вышлем Вам эскиз вашей картины БЕСПЛАТНО!</div>
                    <input type="text" class="form-input" name="name" placeholder="Ваше имя"/>

                    <div class="help-text">Для связи с Вами</div>
                    <div class="clearfix"></div>
                    <input type="text" class="form-input" name="phone" placeholder="Ваш телефон"/>

                    <div class="help-text">Для связи с Вами</div>
                    <div class="clearfix"></div>
                    <input type="email" class="form-input" name="email" placeholder="Ваш e-mail"/>

                    <div class="help-text">Чтобы отправить Вам готовый эскиз картины</div>
                    <div class="clearfix"></div>
                    <div class="header-for-upload">Мы не берем предоплаты за изготовление картины!</div>
                    <div class="upload-button">
                        <input id="file_s" class="real-button" name="file" type="file">
                        <input class="uploaded-name" type="text">
                        <button class="fake-button">Загрузить файл</button>
                    </div>
                    <button type="submit" class="send-button">ПОЛУЧИТЬ эскиз</button>
                    <div class="privacy">Политика конфиденциальности</div>
                    <img class="close-form" src="<?=SITE_TEMPLATE_PATH?>/images/close-popup.png" alt="">
                </form>

            </div>
        </div>
        <div class="popup-success">
            <div class="success-wraper">
                <div class="success">
                    <div class="white-text">Спасибо за Вашу заявку!</div>
                    <div class="green-text">Мы вышлем Вам эскиз вашей картины бесплатно в ближайшее время!</div>
                    <img class="close-button" src="<?=SITE_TEMPLATE_PATH?>/images/close-popup.png" alt="">
                </div>
            </div>
        </div>
        <div class="popup-privacy-policy">
            <div class="ppp-wraper">
                <div class="ppp-text">
                    <img class="closer" src="<?=SITE_TEMPLATE_PATH?>/images/close-popup.png" alt="">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "COMPONENT_TEMPLATE" => ".default",
                            "AREA_FILE_SHOW" => "sect",
                            "AREA_FILE_SUFFIX" => "inc_1111111",
                            "EDIT_TEMPLATE" => "",
                            "AREA_FILE_RECURSIVE" => "Y"
                        )
                    );?>
                </div>
            </div>
        </div>

        <header>
            <div class="color-block">
                <div class="green-color"></div>
                <div class="pink-color"></div>
                <div class="cyan-color"></div>
                <div class="red-color"></div>
            </div>
            <div class="info-block">
                <div class="container" id="top_anchor">
                    <div class="logo-block">
                        <a href="http://<?php echo $_SERVER['HTTP_HOST'] ?>"><img class="logo" src="<?=SITE_TEMPLATE_PATH?>/images/logo.png" alt=""/></a>
                    </div>
                    <div class="contact-block">
                        <div class="phone phone-to-change">8 (495) 761-76-61</div>
                        <div class="work-time">мы работаем: <span class="time-to-change">9:00 - 20:00</span></div>
                    </div>
                    <div class="social_group">
                        мы в соц.сетях:
                        <a target="_blank" class="vk" href="http://vk.com/public73985681"></a>
                        <a target="_blank" class="ins" href="https://instagram.com/pikcher.me/"></a>
                        <a target="_blank" class="odn" href="odn"></a>
                    </div>
                    <div class="city-block">
                        <label for="city-select" class="city-label">Ваш город:</label>
                      <!--  --><?/*$APPLICATION->IncludeComponent("bitrix:news.list", "cityes", Array(
                            "COMPONENT_TEMPLATE" => ".default",
                            "IBLOCK_TYPE" => "simple",	// Тип информационного блока (используется только для проверки)
                            "IBLOCK_ID" => "1",	// Код информационного блока
                            "NEWS_COUNT" => "20",	// Количество новостей на странице
                            "SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
                            "SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
                            "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
                            "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
                            "FILTER_NAME" => "",	// Фильтр
                            "FIELD_CODE" => array(	// Поля
                                0 => "",
                                1 => "",
                            ),
                            "PROPERTY_CODE" => array(	// Свойства
                                0 => "DATA",
                                1 => "PHONE",
                            ),
                            "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
                            "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                            "AJAX_MODE" => "N",	// Включить режим AJAX
                            "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                            "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                            "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                            "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
                            "CACHE_TYPE" => "A",	// Тип кеширования
                            "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                            "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
                            "CACHE_GROUPS" => "Y",	// Учитывать права доступа
                            "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
                            "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
                            "SET_BROWSER_TITLE" => "Y",	// Устанавливать заголовок окна браузера
                            "SET_META_KEYWORDS" => "Y",	// Устанавливать ключевые слова страницы
                            "SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
                            "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
                            "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
                            "PARENT_SECTION" => "",	// ID раздела
                            "PARENT_SECTION_CODE" => "",	// Код раздела
                            "INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
                            "DISPLAY_DATE" => "Y",	// Выводить дату элемента
                            "DISPLAY_NAME" => "Y",	// Выводить название элемента
                            "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
                            "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
                            "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
                            "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                            "DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
                            "PAGER_TITLE" => "Новости",	// Название категорий
                            "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
                            "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
                            "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
                            "PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
                            "SET_STATUS_404" => "N",	// Устанавливать статус 404
                            "SHOW_404" => "N",	// Показ специальной страницы
                            "MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
                        ),
                            false
                        );*/?>
                        <span>
                        <a id="citys" href="">
                            <?php
                        if(CModule::IncludeModule("altasib.geoip"))
                        {
                            $arData = ALX_GeoIP::GetAddr();
                            print_r($arData['city']);
                        }
                        ?>
                        </a>
                        </span>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="advantages">
                <div class="container">
                    <div class="advantage"><img src="<?=SITE_TEMPLATE_PATH?>/images/no-prepayment.png"  alt="" class="a-image">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "AREA_FILE_SHOW" => "sect",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "",
                                "AREA_FILE_RECURSIVE" => "Y"
                            )
                        );?>
                        </div>
                    <div class="advantage"><img src="<?=SITE_TEMPLATE_PATH?>/images/one-day.png"  alt="" class="a-image">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "AREA_FILE_SHOW" => "sect",
                                "AREA_FILE_SUFFIX" => "inc_1",
                                "EDIT_TEMPLATE" => "",
                                "AREA_FILE_RECURSIVE" => "Y"
                            )
                        );?>
                        </div>
                    <div class="advantage"><img src="<?=SITE_TEMPLATE_PATH?>/images/money-back.png" alt="" class="a-image">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "AREA_FILE_SHOW" => "sect",
                                "AREA_FILE_SUFFIX" => "inc_2",
                                "EDIT_TEMPLATE" => "",
                                "AREA_FILE_RECURSIVE" => "Y"
                            )
                        );?>

                    </div>
                </div>
            </div>
            <div class="fixed-menu">
                <div class="menu-block">
                    <div class="container">
                        <ul class="menu special">
                            <li class="menu-item nice-hover"><a href="http://<?php echo $_SERVER['HTTP_HOST'] ?>/kontakty/" class="link">
                                    Контакты</a>
                            </li>
                            <li id="green_me" class="menu-item nice-hover"><a href="http://<?php echo $_SERVER['HTTP_HOST'] ?>/zakazat-kartinu-na-khoste" class="link mi-active">
                                    <?$APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        "",
                                        Array(
                                            "COMPONENT_TEMPLATE" => ".default",
                                            "AREA_FILE_SHOW" => "sect",
                                            "AREA_FILE_SUFFIX" => "inc_11111",
                                            "EDIT_TEMPLATE" => "",
                                            "AREA_FILE_RECURSIVE" => "Y"
                                        )
                                    );?></a>
                            </li>
                        </ul>

                        <?$APPLICATION->IncludeComponent("bitrix:menu", "menu_top", Array(
                            "COMPONENT_TEMPLATE" => ".default",
                            "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
                            "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                            "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                            "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                            "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                            "MAX_LEVEL" => "1",	// Уровень вложенности меню
                            "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                            "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                            "DELAY" => "N",	// Откладывать выполнение шаблона меню
                            "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                        ),
                            false
                        );?>
                        <ul class="menu left_area">
                            <li class="menu-item nice-hover"><a href="http://<?php echo $_SERVER['HTTP_HOST'] ?>" class="link">
                                 Главная</a>
                            </li>
                        </ul>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="color-block">
                <div class="green-color"></div>
                <div class="pink-color"></div>
                <div class="cyan-color"></div>
                <div class="red-color"></div>
            </div>
        </header>