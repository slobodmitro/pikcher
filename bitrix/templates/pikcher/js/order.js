jQuery(document).ready(function () {
    app.readImageURL = function (input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('.control-image').attr('src', e.target.result);
                $('#fance_thumbs').attr('href', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    };

    //SizeSelect
    var select;
    select = $('.repin .chose-size-select .cs-select');
    if (select.length == 0)  {
        select = jQuery('.cs-select');
    }

    select.selectmenu();
    var price = jQuery('.size-price');
    var text = '<i class="fa fa-rub"></i>';
    var currentSize = select.find('option:selected');
    select.on('selectmenuchange', function (event, ui) {
        currentSize = jQuery(ui.item.element[0]);
        var val = parseFloat(currentSize.attr('data-price'));
        price.text('= ' + val + ' ');
        price.append(text);
        var cImg = jQuery('.control-image');
        var value = currentSize.attr('value').replace(' cm', '');
        var width = parseInt(value.substr(0, value.indexOf('x')));
        var height = parseInt(value.substr(value.indexOf('x') + 1, value.length));
        cImg.css({
            'width': (width * 1.5).toString() + 'px',
            'height': (height * 1.5).toString() + 'px',
            'margin-left': -(((width * 1.5) - 40) / 2).toString() + 'px'
        });

        onChangeSize(currentSize);
    });
    var val = parseFloat(currentSize.attr('data-price'));
    price.text('= ' + val + ' ');
    price.append(text);
    var cImg = jQuery('.control-image');
    var value = currentSize.attr('value').replace(' cm', '');
    var width = parseInt(value.substr(0, value.indexOf('x')));
    var height = parseInt(value.substr(value.indexOf('x') + 1, value.length));
    cImg.css({
        'width': (width * 1.5).toString() + 'px',
        'height': (height * 1.5).toString() + 'px',
        'margin-left': -(((width * 1.5) - 40) / 2).toString() + 'px'
    });
    price.text('= ' + currentSize.attr('data-price') + ' ');
    price.append(text);

    //Accordion
    var acc = jQuery('.accordion');
    acc.accordion({
        collapsible: true,
        active: false,
        disabled: true,
        heightStyle: "content"
    });
    var lastIndex;
    jQuery('.trigger').click(function () {
        var parAcc = jQuery(this).parents('.accordion');
        var index = parseInt($(this).data('index'));
        if (lastIndex === index) {
            parAcc.accordion('option', 'active', false);
            lastIndex = false;
        } else {
            parAcc.accordion('option', 'active', index);
            lastIndex = index;
        }
    });

    //LightBox
    var fancy2 = jQuery('.fancy2');
    fancy2.fancybox({
        helpers: {
            title: {
                type: 'inside'
            }
        },
        openEffect: 'elastic',
        closeEffect: 'elastic'
    });

    var sActive = false;
    var sum = jQuery('.sum');

    if (jQuery('.add-photo').length > 0) {
        var topEdge = 150;
    }

    //SUM
    jQuery(window).on('scroll', function (e) {
        var botEdge = jQuery('.request').position().top - $(window).height() + 280;
        if (!sActive) {
            if ((window.pageYOffset > topEdge) && (window.pageYOffset < botEdge)) {
                sum.hide();
                sum.addClass('sum-bottom');
                sum.fadeIn(600, function () {
                    sum.show();
                });
                sActive = true;
            } else if ((window.pageYOffset < topEdge) || (window.pageYOffset > botEdge)) {
                sActive = false;
                sum.removeClass('sum-bottom');
            }
        }
        else {
            if ((window.pageYOffset < topEdge) || (window.pageYOffset > botEdge)) {
                sActive = false;
            }
        }

    });

    //SumCalc
    var sumPlace = jQuery('.sum-amount>.value');
    var totalPrice = {
        'size': 0,
        'canvas': 0,
        'cover': 0,
        'frame': 0,
        'baget': 0,
        'correction': 0,
        'art-processing': 0,
        'art-processing2': 0
    };

    function setValue(key, value) {
        if (jQuery('.add-photo').length >0) {
        }else{
            if(key=='size'){
                value = $('#ui-id-1 option:selected').data('price');
            }
        }
        totalPrice[key] = (value) ? parseFloat(value) : 0;
        updatePrice();
    }

    function updatePrice() {
        var total = 0;
        jQuery.each(totalPrice, function (key, value) {
            total += value;
        });
        sumPlace.text(total);
        $('#all_price').val(total);
    }

    setValue('size', currentSize.attr('data-price'));

    jQuery.each(totalPrice, function (key, item) {
        var input = jQuery('#' + key);
        if (input.length) {
            input.change(function () {
                if (jQuery(this).is(':checked')) {
                    setValue(key, input.val());
                } else {
                    setValue(key, 0);
                }
            });
        }
    });


    //Checkbox Control in Block

    var checkBoxes = jQuery('.check-control input[type=checkbox]');
    checkBoxes.change(function () {
        var par = jQuery(this).parents('.check-control');
        var children = par.find('.cb-to-control input[type=checkbox]');
        var isCurChecked = jQuery(this).is(':checked');
        if (isCurChecked) {
            jQuery.each(children, function (key, val) {
                var cur = jQuery(val);
                cur.prop('checked', false);
                setValue(cur.attr('name'), 0);
            });
            var checked = jQuery(this);
            checked.prop('checked', true);
            setValue(checked.attr('name'), checked.val());
        }
    });

    function onChangeSize(val) {
        var prices = priceData[jQuery(val).val()];
        var sizeSelect = $('.repin #ui-id-1 option:selected');
        if (sizeSelect.length) {
            prices['price'] = sizeSelect.data('price');
        }
        var getVal = function (key) {
                if (key === 'price') {
                    return prices[key];
                }
                var val = prices[key];
                if (val.slice(-1) === '%') {
                    return prices['price'] * (parseFloat(val.replace('%', '')) / 100);
                } else {
                    return parseFloat(val);
                }
            };
        setValue('size', getVal('price'));
        jQuery('#start_price').val(getVal('price'));

        jQuery('#canvas').val(getVal('holst'));
        jQuery('#canvas-price-label').text(getVal('holst'));

        jQuery('#cover').val(getVal('factur'));
        jQuery('#cover-price-label').text(getVal('factur'));

        jQuery('#baget').val(getVal('baget'));
        jQuery('#baget-price-label').text(getVal('baget'));

        jQuery('#art-processing').val(getVal('obrob'));
        jQuery('#art-processing-price-label').text(getVal('obrob'));

        jQuery('#art-processing2').val(getVal('shars'));
        jQuery('#art-processing2-price-label').text(getVal('shars'));

    }

    onChangeSize(currentSize);

    //SetWhereFree
    function setWF() {
        var inputs = jQuery('input[type=checkbox]');
        jQuery.each(inputs, function (key, val) {
            var cur = jQuery(val);
            var amount = cur.val();
            if (amount == "0") {
                cur.prop('checked', true);
            }
        });
    }

    setWF();

    //ChoosenFrame

    var parentControl = jQuery('.parent-on');
    parentControl.click(function () {
        var cur = jQuery(this);
        var parentCheck = cur.parents('.corner-frame').prev().find('input');
        var checked = cur.find('input').prop('checked');
        if (checked) {
            parentCheck.prop('checked', true);
        } else {
            parentCheck.prop('checked', false);
            setWF();
        }
    });
});
$(document).ready(function () {
    jQuery('form').on('focusout', 'input', function () {
        debugger;
        jQuery(this).css('border', '1px solid rgb(169, 169, 169)');
    });
    jQuery('form').on('focusout', 'textarea', function () {
        debugger;
        jQuery(this).css('border', '1px solid rgb(169, 169, 169)');
    });

    var body = jQuery('body');
    $("#ajaxform_order").submit(function () {
        var form = $(this);
        var error = false;
        form.find('input, textarea').each(function () {
            if ($(this).val() == '') {
                $(this).css('border', '1px solid red');
                error = true; // oшибкa
            }
        });

        if (!error) { // eсли oшибки нeт
            var fd = new FormData();
            var data = form.serializeArray();
            jQuery.each(data, function () {
                fd.append(this.name, this.value);
            });
            fd.append('img', form.find('#file_s')[0].files[0]);
            debugger;
            $.ajax({
                type: 'POST', // oтпрaвляeм в POST фoрмaтe, мoжнo GET
                url: app.base_url + '/email-order.php', // путь дo oбрaбoтчикa, у нaс oн лeжит в тoй жe пaпкe
                processData: false,
                contentType: false,
                dataType: 'json', // oтвeт ждeм в json фoрмaтe
                data: fd, // дaнныe для oтпрaвки
                beforeSend: function (data) { // сoбытиe дo oтпрaвки
                    form.find('input[type="submit"]').attr('disabled', 'disabled'); // нaпримeр, oтключим кнoпку, чтoбы нe жaли пo 100 рaз
                },
                success: function (data) { // сoбытиe пoслe удaчнoгo oбрaщeния к сeрвeру и пoлучeния oтвeтa
                    form.find('.close-form').click();
                    var sucForm = jQuery('.popup-success');
                    sucForm.toggle();
                    body.css({"overflow": "hidden"});
                },
                error: function (xhr, ajaxOptions, thrownError) { // в случae нeудaчнoгo зaвeршeния зaпрoсa к сeрвeру
                    alert(xhr.status); // пoкaжeм oтвeт сeрвeрa
                    alert(thrownError); // и тeкст oшибки
                },
                complete: function (data) { // сoбытиe пoслe любoгo исхoдa
                    form.find('input[type="submit"]').prop('disabled', false); // в любoм случae включим кнoпку oбрaтнo
                }

            });
        }
        return false; // вырубaeм стaндaртную oтпрaвку фoрмы
    });
});