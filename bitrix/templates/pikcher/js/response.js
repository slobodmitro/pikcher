jQuery(document).ready(function () {
    //LightBox


    //PopupResponse
    var pResponse = jQuery('.popup-response');
    var body = jQuery('body');
    var closer = $(this).find('.close-form');
    closer.click(function (e) {
        pResponse.toggle();
        body.css({"overflow": "auto"});
        e.preventDefault(e);
        return false;
    });
    var opener = jQuery('.call-pr');
    opener.click(function (e) {
        e.preventDefault(e);
        var top = jQuery(window).scrollTop();
        pResponse.css({"top": top + "px"});
        pResponse.toggle();
        body.css({"overflow": "hidden"});
        return false;
    });

});