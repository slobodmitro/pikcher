jQuery(document).ready(function () {
    //Tabs
    $(".picture-types-tabs").tabs();

    //Slider Owl 2 rows
    var slider = jQuery(".two-rows-carousel");
    slider.owlCarousel({
        loop: true,
        autoWidth:true,
        center: true,
        responsive: {
            1900: {items: 2},
            1600: {items: 2},
            1200: {items: 2},
            1000: {items: 2},
            700: {items: 2},
            400: {items: 2}
        }
    });
    jQuery('.slider-control .slider-prev').click(function () {
        slider.trigger('prev.owl.carousel')
    });
    jQuery('.slider-control .slider-next').click(function () {
        slider.trigger('next.owl.carousel')
    });

    //LightBox

});