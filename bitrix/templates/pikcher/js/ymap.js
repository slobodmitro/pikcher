ymaps.ready(init);
var myMap,
    myPlacemark;

function init(){
    myMap = new ymaps.Map("map", {
        center: [55.856356, 37.641225],
        zoom: 16
    });

    myPlacemark = new ymaps.Placemark([55.856356, 37.641225], {
        hintContent: 'проезд Русанова 25к1'
    });
    myPlacemark.options.set('preset', 'islands#redIcon');

    myMap.geoObjects.add(myPlacemark);
}
