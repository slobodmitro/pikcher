jQuery(document).ready(function () {
    //Slider Owl 2 rows
    var slider = jQuery(".two-rows-carousel");
    slider.owlCarousel({
        loop: true,
        center: true,
        autoWidth: true,
        responsive: {
            1900: {items: 2},
            1600: {items: 2},
            1200: {items: 2},
            1000: {items: 2},
            700: {items: 2},
            400: {items: 2}
        }
    });
    jQuery('.slider-control .slider-prev').click(function () {
        slider.trigger('prev.owl.carousel')
    });
    jQuery('.slider-control .slider-next').click(function () {
        slider.trigger('next.owl.carousel')
    });

    //Slider Owl
    var slider2 = jQuery(".specialist-slider");
    slider2.owlCarousel({
        items: 5,
        loop: true
    });
    jQuery('.specialists-scontrol .slider-prev').click(function () {
        slider2.trigger('prev.owl.carousel')
    });
    jQuery('.specialists-scontrol .slider-next').click(function () {
        slider2.trigger('next.owl.carousel')
    });

    //LightBox

});