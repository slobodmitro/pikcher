jQuery(document).ready(function () {
    //Tabs
    $(".picture-types-tabs").tabs();

    //Slider Owl 2 rows
    var slider = jQuery(".two-rows-carousel");
    slider.owlCarousel({
        loop: true,
        autoWidth:true,
        center: true,
    });
    jQuery('.slider-control .slider-prev').click(function () {
        slider.trigger('prev.owl.carousel')
    });
    jQuery('.slider-control .slider-next').click(function () {
        slider.trigger('next.owl.carousel')
    });

    //LightBox

});