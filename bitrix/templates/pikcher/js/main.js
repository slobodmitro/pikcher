var app = {};
jQuery(document).ready(function () {

    var items=$('.container.gallery_simple .item_arr');
    var width=0;
    var items_length=items.length;
    width=parseInt(items_length/4);
    if(items.length % 4 !=0){
        width++;
    }
    if(width>=3){
        width= 3*225;
    }else if(width<3){
        width= width*225;
    }
    $('.container.gallery_simple').animate({height: width+"px"}, {duration: 50});
    $('#more_clients').click(function(e){
        e.preventDefault();
        width=0;
        width=parseInt(items_length/4);
        if(items.length % 4 !=0){
            width++;
        }
        width= width*228;
        $('.container.gallery_simple').animate({height: width+"px"}, {
            duration: 50
        });
        $(this).css('display','none');
    });

    //-----For all pages-----//
    var city = localStorage.getItem("city");
    //DropDown
    var dropDown = jQuery('#city-select');
    if (city) {
        dropDown.val(city);
    }
    dropDown.selectmenu()
        .selectmenu("menuWidget")
        .addClass("select-overflow");
    //DropDownChange


    dropDown.on("selectmenuchange", function (event, ui) {
        localStorage.setItem("city", jQuery(ui.item.element[0]).val());
        var currentCity = jQuery(ui.item.element[0]);
        var phone = jQuery('.phone-to-change');
        var time = jQuery('.time-to-change');
        phone.each(function (index, value) {
            value.innerText = currentCity.attr('data-phone');
        });
        time.each(function (index, value) {
            value.innerText = currentCity.attr('data-time');
        });
    });

    //Menu
    var mActive = false;
    var menu = jQuery('.fixed-menu');
    jQuery(window).on('scroll', function (e) {
        if (!mActive) {
            if (window.pageYOffset > 195) {
                menu.hide();
                menu.addClass('menu-top');
                menu.fadeIn(600, function () {
                    menu.show();
                });
                mActive = true;
            } else {
                mActive = false;
                menu.removeClass('menu-top');
            }
        }
        if (window.pageYOffset <= 195) {
            mActive = false;
        }
    });

    //FileUpload
    var fakeUB = jQuery('.fake-button');
    fakeUB.click(function (e) {
        e.preventDefault();
        var realUB = jQuery(this).parent().find('.real-button');
        realUB.click();
    });
    var realUB = jQuery('.real-button');
    realUB.on('change', function () {

        var cur = jQuery(this);
        var uploadedName = cur.parent().find('.uploaded-name');
        uploadedName.val(cur.val());
        uploadedName.trigger('focusout');
    });

    //PopupForm
    var popup = jQuery('.popup-form');
    var body = jQuery('body');
    var closer = popup.find('.close-form');
    closer.click(function () {
        popup.toggle();
        body.css({"overflow": "auto"});
    });
    var opener = jQuery('.call-form');
    opener.click(function () {
        var top = jQuery(window).scrollTop();
        popup.css({"top": top + "px"});
        popup.toggle();
        body.css({"overflow": "hidden"});
    });
    //PopupPP
    var pp = jQuery('.popup-privacy-policy');
    var closer2 = jQuery('.closer');
    closer2.click(function () {
        pp.toggle();
        body.css({"overflow": "auto"});
    });
    var privacy = jQuery('.privacy');
    privacy.click(function () {
        var top = jQuery(window).scrollTop();
        pp.css({"top": top + "px"});
        pp.toggle();
        body.css({"overflow": "hidden"});
    });

    //Success
    var sucForm = jQuery('.popup-success');
    var closer3 = sucForm.find('.close-button');
    closer3.click(function () {
        sucForm.toggle();
        body.css({"overflow": "auto"});
    });
    var sucCall = jQuery('.send-button1');
    sucCall.click(function (e) {
        e.preventDefault();
        sucCall.parents('.form-to-close').toggle();
        var top = jQuery(window).scrollTop();
        sucForm.css({"top": top + "px"});
        sucForm.toggle();
        body.css({"overflow": "hidden"});
    });

    //SliderOpacity
    var slidersSides = jQuery('.slider-opacity>div');
    var setSliderSidesWidth = function () {
        var w = (jQuery(window).width() - 1000) / 2;
        slidersSides.css('width', w);
    };
    $(window).resize(setSliderSidesWidth);
    setSliderSidesWidth();
});
$(document).ready(function () {
    jQuery('form').on('focusout', 'input', function () {

        jQuery(this).css('border', 'none');
    });
    var body = jQuery('body');
    $("#ajaxform, #ajaxform1, #ajaxform2").submit(function () {
        var form = $(this);
        var error = false;
        form.find('input, textarea').each(function () {
            if ($(this).val() == '') {
                $(this).css('border', '1px solid red');
                error = true; // oшибкa
            }
        });

        if (!error) { // eсли oшибки нeт
            var fd = new FormData();
            fd.append('name', $(this).find('input[name=name]').val());
            fd.append('email', $(this).find('input[name=email]').val());
            fd.append('phone', $(this).find('input[name=phone]').val());
            fd.append('img', $(this).find('#file_s')[0].files[0]);
            $.ajax({
                type: 'POST', // oтпрaвляeм в POST фoрмaтe, мoжнo GET
                url: app.base_url + '/email.php', // путь дo oбрaбoтчикa, у нaс oн лeжит в тoй жe пaпкe
                processData: false,
                contentType: false,
                dataType: 'json', // oтвeт ждeм в json фoрмaтe
                data: fd, // дaнныe для oтпрaвки
                beforeSend: function (data) { // сoбытиe дo oтпрaвки
                    form.find('input[type="submit"]').attr('disabled', 'disabled'); // нaпримeр, oтключим кнoпку, чтoбы нe жaли пo 100 рaз
                },
                success: function (data) { // сoбытиe пoслe удaчнoгo oбрaщeния к сeрвeру и пoлучeния oтвeтa
                    form.find('.close-form').click();
                    var sucForm = jQuery('.popup-success');
                    sucForm.toggle();
                    body.css({"overflow": "hidden"});
                },
                error: function (xhr, ajaxOptions, thrownError) { // в случae нeудaчнoгo зaвeршeния зaпрoсa к сeрвeру
                    alert(xhr.status); // пoкaжeм oтвeт сeрвeрa
                    alert(thrownError); // и тeкст oшибки
                },
                complete: function (data) { // сoбытиe пoслe любoгo исхoдa
                    form.find('input[type="submit"]').prop('disabled', false); // в любoм случae включим кнoпку oбрaтнo
                }

            });
        }
        return false; // вырубaeм стaндaртную oтпрaвку фoрмы
    });

    //PopUp
    $('.form_des').click(function (e) {
        e.preventDefault();
        $('.tobuy').fadeIn();
        $('#overflow').fadeIn();
    });
    $('.close-form').click(function (e) {
        e.preventDefault();
        $('.tobuy').fadeOut();
        $('#overflow').fadeOut();
    });
    $('#overflow').click(function (e) {
        e.preventDefault();
        $('.tobuy').fadeOut();
        $('#overflow').fadeOut();
    });
    //PopUp
    $('#citys').click(function (e) {
        e.preventDefault();
        $('.adress_change').fadeIn();
        $('#overflow').fadeIn();
    });
    $('.close_form').click(function (e) {
        e.preventDefault();
        $('.adress_change').fadeOut();
        $('#overflow').fadeOut();
    });
    $('#overflow').click(function (e) {
        e.preventDefault();
        $('.adress_change').fadeOut();
        $('#overflow').fadeOut();
    });
    //PopUp_thank`s

    $('.close-thanks').click(function (e) {
        e.preventDefault();
        $('.thanks_for').fadeOut();
        $('#overflow').fadeOut();
    });
    $('#overflow').click(function (e) {
        e.preventDefault();
        $('.thanks_for').fadeOut();
        $('#overflow').fadeOut();
    });
    //PopUp_thank`s
});


$(document).ready(function() {
    var num1 = 5;
    var num2 = 10;
    $('.rene .item').each(function(index) {
        $('.rene .item').slice(num1,num2).css( "background", "white" );
        num1=num1+10;
        num2=num2+10;
    });

    $('a#subm').click(function(e){
        e.preventDefault();
        $('.search_in').submit();
    });
});

