<?php
require_once 'toMail.php';
if ($_POST) {
    $data_array=$_POST;
    if ($_FILES["img"]["size"] > 1024 * 15 * 1024) {
        $json['error'] = ("Размер файла превышает 15 мегабайта");
        echo json_encode($json);
        die();
    }
    if (is_uploaded_file($_FILES["img"]["tmp_name"])) {
        $filename = date('Y-m-d_h:m:s') . '_' . $_FILES["img"]["name"];
        $path = realpath(dirname(dirname(dirname(__DIR__))) . "/upload/images/"). DIRECTORY_SEPARATOR  .$filename;
        move_uploaded_file($_FILES["img"]["tmp_name"], $path);
    } else {
        $json['error'] = ("Ошибка загрузки файла");
        echo json_encode($json);
        die();
    }
    $imgUrl = $_SERVER['HTTP_HOST']. '/upload/images/'.$filename;
    $html_email ='Приветствуем Вас, '.$data_array["yourname"].'!<br/>';
    $html_email .='Наша команда PIKcher получила от Вас заявку на картину.<br/>';
    $html_email .='В течение двух часов наш специалист свяжется с вами для уточнения деталей заказа.<br/><br/>';
    $html_email .= '<h3>Данные клиента:</h3>';
    $html_email .= 'Имя: ' . $data_array["yourname"] . '<br/>';
    $html_email .= 'Телефон: ' . $data_array["yourphone"] . '<br/>';
    $html_email .= 'E-mail: ' . $data_array["youremail"] . '<br/>';
    $html_email .= 'Пожелания: ' . $data_array["response"] . '<br/>';
    $html_email .= '<br/><h3>Характеристики:</h3>';
    $html_email .= 'Картина:<br/> <img style="width:400px" src="' . $imgUrl . '"><br/><a href="'.$imgUrl.'">Скачать</a><br/>';
    if (isset($data_array["size"])) {
        $html_email .= 'Размер картины: ' . $data_array["size"] . '('.$data_array['start_price'].'р)<br/>';
    }
    if (isset($data_array["canvas"])) {
        $html_email .= 'Оформление картины: Холст Дали (+'.$data_array["canvas"].'р)<br/>';
    }
    if (isset($data_array["cover"])) {
        $html_email .= 'Оформление картины: Покрыть фактурным гелем (+'.$data_array["cover"].'р)<br/>';
    }
    if (isset($data_array["frame"])) {
        $html_email .= 'Выбор рамки: Оформить картину на подрамник (+'.$data_array["frame"].'р)<br/>';
    }
    if (isset($data_array["baget"])) {
        $html_email .= 'Выбор рамки: Оформить картину в багет (+'.$data_array["baget"].'р)<br/>';
    }
    if (isset($data_array["baget1"])) {
        $html_email .= 'Оформить картину в багет: Багет №1 (+'.$data_array["baget1"].'р)<br/>';
    }
    if (isset($data_array["baget2"])) {
        $html_email .= 'Оформить картину в багет: Багет №2 (+'.$data_array["baget2"].'р)<br/>';
    }
    if (isset($data_array["baget3"])) {
        $html_email .= 'Оформить картину в багет: Багет №3 (+'.$data_array["baget3"].'р)<br/>';
    }
    if (isset($data_array["baget4"])) {
        $html_email .= 'Оформить картину в багет: Багет №4 (+'.$data_array["baget4"].'р)<br/>';
    }
    if (isset($data_array["baget5"])) {
        $html_email .= 'Оформить картину в багет: Багет №4 (+'.$data_array["baget5"].'р)<br/>';
    }
    if (isset($data_array["baget6"])) {
        $html_email .= 'Оформить картину в багет: Багет №4 (+'.$data_array["baget6"].'р)<br/>';
    }
    if (isset($data_array["baget7"])) {
        $html_email .= 'Оформить картину в багет: Багет №4 (+'.$data_array["baget7"].'р)<br/>';
    }
    if (isset($data_array["baget8"])) {
        $html_email .= 'Оформить картину в багет: Багет №4 (+'.$data_array["baget8"].'р)<br/>';
    }
    if (isset($data_array["baget9"])) {
        $html_email .= 'Оформить картину в багет: Багет №4 (+'.$data_array["baget9"].'р)<br/>';
    }
    if (isset($data_array["baget10"])) {
        $html_email .= 'Оформить картину в багет: Багет №4 (+'.$data_array["baget10"].'р)<br/>';
    }
    if (isset($data_array["baget11"])) {
        $html_email .= 'Оформить картину в багет: Багет №4 (+'.$data_array["baget11"].'р)<br/>';
    }
    if (isset($data_array["baget12"])) {
        $html_email .= 'Оформить картину в багет: Багет №4 (+'.$data_array["baget12"].'р)<br/>';
    }
    if (isset($data_array["correction"])) {
        $html_email .= 'Выбор обработки фотографии: Без обработки, легкая коррекция (+'.$data_array["correction"].'р)<br/>';
    }
    if (isset($data_array["art-processing"])) {
        $html_email .= 'Выбор обработки фотографии: Художественная обработка (+'.$data_array["art-processing"].'р)<br/>';
    }
    if (isset($data_array["art-processing2"])) {
        $html_email .= 'Выбор обработки фотографии: Шарж (+'.$data_array["art-processing2"].'р)<br/>';
    }
    $html_email .='<h3>Общая стоимость вашего заказ составляет: '.$data_array['all_price'].'р.</h3>';
    $html_email .='По поводу стоимости и сроков доставки с вами свяжется наш менеджер.<br/>';
    $html_email .='Благодарим Вас, что выбрали нашу компанию.<br/>';
    $html_email .='--<br/>';
    $html_email .='С надеждой подарить радость нашим клиентам,<br/>';
    $html_email .='Команда PIKcher<br/>';
    $html_email .='Наш тел. +7 (495) 761-76-61, 8 (800) 550-01-17 (телефон бесплатный)<br/>';
    $html_email .='Наш e-mail: radost@pikcher.me<br/>';
    $html_email .='Наш сайт: www.pikcher.me<br/>';

    $json = array(); // пoдгoтoвим мaссив oтвeтa

    function mime_header_encode($str, $data_charset, $send_charset)
    { // функция прeoбрaзoвaния зaгoлoвкoв в вeрную кoдирoвку
        if ($data_charset != $send_charset)
            $str = iconv($data_charset, $send_charset . '//IGNORE', $str);
        return ('=?' . $send_charset . '?B?' . base64_encode($str) . '?=');
    }

    /* супeр клaсс для oтпрaвки письмa в нужнoй кoдирoвкe */


    $emailgo = new TEmail;
    $emailgo->from_email = $data_array["youremail"];
    $emailgo->from_name = $data_array["yourname"];
    $emailgo->to_email = 'pikcher.me@gmail.com';
    $emailgo->subject = 'Оформить заказ макета картины';
    $emailgo->body = $html_email;
    $emailgo->send();

    $emailgo = new TEmail;
    $emailgo->from_email = $data_array["youremail"];
    $emailgo->from_name = $data_array["yourname"];
    $emailgo->to_email = $data_array["youremail"];
    $emailgo->subject = 'Оформить заказ макета картины';
    $emailgo->body = $html_email;
    $emailgo->send();

    $json['error'] = 0;

    echo json_encode($json);
} else {
    echo 'GET LOST!';
}
?>