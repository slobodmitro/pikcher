<?php
require_once 'toMail.php';

if ($_POST) { // eсли пeрeдaн мaссив POST
    $name = htmlspecialchars($_POST["name"]); // пишeм дaнныe в пeрeмeнныe и экрaнируeм спeцсимвoлы
    $email = htmlspecialchars($_POST["email"]);
    $phone = htmlspecialchars($_POST["phone"]);
    if ($_FILES["img"]["size"] > 1024 * 15 * 1024) {
        $json['error'] = ("Размер файла превышает 15 мегабайта");
        echo json_encode($json);
        die();
    }
    if (is_uploaded_file($_FILES["img"]["tmp_name"])) {
        $filename = date('Y-m-d_h:m:s') . '_' . $_FILES["img"]["name"];
        $path = realpath(dirname(dirname(dirname(__DIR__))) . "/upload/images/"). DIRECTORY_SEPARATOR  .$filename;
        move_uploaded_file($_FILES["img"]["tmp_name"], $path);
    } else {
        $json['error'] = ("Ошибка загрузки файла");
        echo json_encode($json);
        die();
    }
    $json = array(); // пoдгoтoвим мaссив oтвeтa


    function mime_header_encode($str, $data_charset, $send_charset)
    { // функция прeoбрaзoвaния зaгoлoвкoв в вeрную кoдирoвку
        if ($data_charset != $send_charset)
            $str = iconv($data_charset, $send_charset . '//IGNORE', $str);
        return ('=?' . $send_charset . '?B?' . base64_encode($str) . '?=');
    }
    $imgUrl = $_SERVER['HTTP_HOST']. '/upload/images/'.$filename;
    $emailgo = new TEmail;
    $emailgo->from_email = $email;
    $emailgo->from_name = $name;
    $emailgo->to_email = 'pikcher.me@gmail.com';
    $emailgo->subject = 'ЗАПОЛНИТЕ ФОРМУ и мы вышлем Вам макет вашей картины БЕСПЛАТНО!';
    $emailgo->subject = 'ЗАПОЛНИТЕ ФОРМУ и мы вышлем Вам макет вашей картины БЕСПЛАТНО!';
    $emailgo->body = 'Приветствуем Вас, '.$name.'!<br/>'.'Наша команда PIKcher получила от Вас заявку на картину.<br/>'.'В течение двух часов наш специалист свяжется с вами для уточнения деталей заказа.<br/><br/>'.'<h3>Ваши данные:</h3>'.'Имя: ' . $name . '<br/>' . 'Телефон: ' . $phone . '<br/>E-mail ' . $email .'<br/><h3>Описание Вашего заказа:</h3>' . 'Фотография:<br/> <img style="width:400px" src="' . $imgUrl . '"><br/><a href="'.$imgUrl.'">Скачать</a><br/><br/>Благодарим Вас, что выбрали нашу компанию.По поводу стоимости и сроков доставки с вами свяжется наш менеджер.<br/>--<br/>С надеждой подарить радость нашим клиентам,<br/> Команда PIKcher<br/>Наш тел. +7 (495) 761-76-61, 8 (800) 550-01-17 (телефон бесплатный)<br/> Наш e-mail: radost@pikcher.me<br/>Наш сайт: www.pikcher.me<br/>';
    $emailgo->send();
    $emailgo = new TEmail;
    $emailgo->from_email = 'pikcher.me@gmail.com';
    $emailgo->from_name = $name;
    $emailgo->to_email = $email;
    $emailgo->subject = 'ЗАПОЛНИТЕ ФОРМУ и мы вышлем Вам макет вашей картины БЕСПЛАТНО!';
    $emailgo->body = 'Приветствуем Вас, '.$name.'!<br/>'.'Наша команда PIKcher получила от Вас заявку на картину.<br/>'.'В течение двух часов наш специалист свяжется с вами для уточнения деталей заказа.<br/><br/>'.'<h3>Ваши данные:</h3>'.'Имя: ' . $name . '<br/>' . 'Телефон: ' . $phone . '<br/>E-mail ' . $email .'<br/><h3>Описание Вашего заказа:</h3>' . 'Фотография:<br/> <img style="width:400px" src="' . $imgUrl . '"><br/><a href="'.$imgUrl.'">Скачать</a><br/><br/>Благодарим Вас, что выбрали нашу компанию.По поводу стоимости и сроков доставки с вами свяжется наш менеджер.<br/>--<br/>С надеждой подарить радость нашим клиентам,<br/> Команда PIKcher<br/>Наш тел. +7 (495) 761-76-61, 8 (800) 550-01-17 (телефон бесплатный)<br/> Наш e-mail: radost@pikcher.me<br/>Наш сайт: www.pikcher.me<br/>';
    $emailgo->send();

     $json['error'] = 0;

    echo json_encode($json);
} else {
    echo 'GET LOST!';
}
?>