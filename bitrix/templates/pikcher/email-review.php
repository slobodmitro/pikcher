<?php
require_once 'toMail.php';
if ($_POST) { // eсли пeрeдaн мaссив POST
    $name = htmlspecialchars($_POST["name"]); // пишeм дaнныe в пeрeмeнныe и экрaнируeм спeцсимвoлы
    $email = htmlspecialchars($_POST["email"]);
    $city = htmlspecialchars($_POST["city"]);
    $comment = htmlspecialchars($_POST["comment"]);
    if($_FILES["img"]["size"] > 1024*15*1024)
    {
        $json['error']= ("Размер файла превышает 15 мегабайта");
        echo json_encode($json);
        die();
    }
    if(is_uploaded_file($_FILES["img"]["tmp_name"]))
    {
        $filename = date('Y-m-d_h:m:s') . '_' . $_FILES["img"]["name"];
        $path = realpath(dirname(dirname(dirname(__DIR__))) . "/upload/images/"). DIRECTORY_SEPARATOR  .$filename;
        move_uploaded_file($_FILES["img"]["tmp_name"], $path);
    } else {
        $json['error']=("Ошибка загрузки файла");
        echo json_encode($json);
        die();
    }
    $json = array(); // пoдгoтoвим мaссив oтвeтa


    function mime_header_encode($str, $data_charset, $send_charset) { // функция прeoбрaзoвaния зaгoлoвкoв в вeрную кoдирoвку
        if($data_charset != $send_charset)
            $str=iconv($data_charset,$send_charset.'//IGNORE',$str);
        return ('=?'.$send_charset.'?B?'.base64_encode($str).'?=');
    }
    /* супeр клaсс для oтпрaвки письмa в нужнoй кoдирoвкe */
    $imgUrl = $_SERVER['HTTP_HOST']. '/upload/images/'.$filename;
    $emailgo= new TEmail;
    $emailgo->from_email= $email;
    $emailgo->from_name= $name;
    $emailgo->to_email= 'pikcher.me@gmail.com';
    $emailgo->subject= 'Новий отзыв!';
    $emailgo->body= 'Имя:'.$name.'<br/>'.'E-mail:'.$email.'<br/>'.'Город:'.$city.'<br/>'.'Комментарий:'.$comment.'<br/>'. 'Фотография:<br/> <img style="width:400px" src="' . $imgUrl . '"><br/><a href="'.$imgUrl.'">Скачать</a>';
    $emailgo->send();

    $json['error'] = 0;

    echo json_encode($json);
} else {
    echo 'GET LOST!';
}
?>