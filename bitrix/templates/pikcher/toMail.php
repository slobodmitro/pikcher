<?php
require_once 'swiftmailer/lib/swift_required.php';
class TEmail
{
    public $from_email;
    public $from_name;
    public $to_email;
    public $to_name;
    public $subject;
    public $data_charset = 'UTF-8';
    public $send_charset = 'windows-1251';
    public $body = '';
    public $type = 'text/html';

    function send()
    {
        $transport = Swift_SmtpTransport::newInstance('ssl://smtp.yandex.ru', 465)
            ->setUsername('pikcher.me@yandex.ru')
            ->setPassword('kartini');

        $mailer = Swift_Mailer::newInstance($transport);
        $message = Swift_Message::newInstance($this->subject)
            ->setFrom(array('pikcher.me@yandex.ru' => $this->from_name))
            ->setTo(array($this->to_email))
            ->addPart($this->body, 'text/html');
        $result = $mailer->send($message);
    }

}
?>