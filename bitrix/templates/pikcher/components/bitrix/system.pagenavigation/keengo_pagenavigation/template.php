<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(!$arResult["NavShowAlways"]) // Если не включено "показывать всегда"
{
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
return;
}
$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
?>
<div class="pagination">
    <?if ($arResult["NavPageNomer"] > 1) { // Если страница не первая ?>
        <a class="paginationPrevNext" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>">Начало</a>
        <?if ($arResult["NavPageNomer"] > 2):?>
            <a class="paginationPrevNext" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>">Предыдущая</a>
        <?else:?>
            <a class="paginationPrevNext" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>">Предыдущая</a>
        <?endif?>
    <?} else { // Если страница первая?>
        <span class="paginationPrevNext">Начало</span>
        <span class="paginationPrevNext">Предыдущая</span>
    <?}?>
    <?$page = $arResult["nStartPage"]?>
    <?while($page <= $arResult["nEndPage"]): // Цикл для пробежки по страницам ?>
        <?if ($page == $arResult["NavPageNomer"]): // Если страница текущая ?>
            <span class="paginationCurrent"><?=$page?></span>
        <?else: // Если страница не текущая ?>
            <a class="paginationPage" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$page?>"><?=$page?></a>
        <?endif?>
        <?$page++?>
    <?endwhile?>
    <?if($arResult["NavPageNomer"] < $arResult["NavPageCount"]) { // Если страница не последняя ?>
        <a class="paginationPrevNext" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>">Следующая</a>
        <a class="paginationPrevNext" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageCount"]?>">Конец</a>
    <?} else { // Если страница последняя ?>
        <span class="paginationPrevNext">Следующая</span>
        <span class="paginationPrevNext">Конец</span>
    <?}?>
</div>