<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
            <? if($arResult['SECTION']['DEPTH_LEVEL']==1){ ?>
            <div class="page">
                <h2 class="color-header">ПЕЧАТЬ КАРТИН НА ХОЛСТЕ</h2>
            </div>
            <div class="works">
                <?
                    $arFilter = array('IBLOCK_ID' => $arParams['IBLOCK_ID'],'DEPTH_LEVEL' => 1);
                    $arSelect = array("PICTURE","CODE","SECTION_PAGE_URL","NAME");
                    $rsSect = CIBlockSection::GetList(array(), $arFilter, false, $arSelect, false);
                    while ($arSect = $rsSect->GetNext())
                    { ?>
                        <div class="item">
                            <a href="/catalog/<?=$arSect['CODE']?>/" class="for_img">
                                <img src="<?=CFile::GetPath($arSect['PICTURE'])?>" alt="" class="a-image"/>
                                <div class="for_img_name">
                                    <?=$arSect['NAME']?>
                                </div>
                            </a>
                            <a class="olol" href="/catalog/<?=$arSect['CODE']?>/">Подробнее</a>
                        </div>
                    <? } ?>
                <div class="clearfix"></div>
            </div>
            <div class="left_bar">
                <form method="GET" action="/search/" class="search_in"><input name="name" type="text"> <a id="subm" href="#"></a></form>
                <div class="clearfix"></div>
                <div class="tag_cloud">
                    <span>Теги:</span>
                    <?
                    $arSelect = Array("ID", "NAME", "CODE");
                    $arFilter = Array("IBLOCK_ID"=>24, "ACTIVE"=>"Y");
                    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                    while($ob = $res->GetNextElement())
                    {
                        $arFields = $ob->GetFields();?>
                        <a href="/catalog/?tag=<?=$arFields['CODE']?>"><?=$arFields['NAME']?></a>
                    <? } ?>
                </div>
                <p>Каталог авторов:</p>

                <div class="leftmenu">
                    <ul>
                        <? foreach($arResult['SECTIONS'] as $SEC){ ?>
                        <li><a href="<?=$SEC['SECTION_PAGE_URL']?>"><?=$SEC['NAME']?></a></li>
                        <? } ?>
                    </ul>
                </div>
            </div>
            <div class="content">
                <div class="page">
                    <h2 class="color-header"><?=$arResult['SECTION']['NAME']?></h2>
                </div>
                <div class="rene">
                    <div class="line5pic">
                        <?
                        $arFilter = array('IBLOCK_ID' => $arParams['IBLOCK_ID'],'DEPTH_LEVEL' => 2, 'SECTION_ID' => $arParams['SECTION_ID']);
                        $arNavParams = array(
                            "nPageSize" => '25',
                            "iNumPage" => $_GET['PAGEN_2']
                        );

                        $arSelect = array("CODE", "NAME", "PICTURE");
                        $rsElement =CIBlockSection::GetList(array("NAME" => "ASC"), $arFilter, false, $arSelect, $arNavParams);
                        $NAV_STRING = $rsElement->GetPageNavStringEx($navComponentObject, '', 'round', 'Y');

                        while($arElem = $rsElement->GetNext())
                        { ?>
                            <div class="item">
                                <p><?=$arElem["NAME"]?></p>

                                <div class="for_img">
                                    <img src="<?=CFile::GetPath($arElem["PICTURE"])?>" alt=""
                                         class="a-image"/>
                                </div>
                                <a href="/catalog/<?=$arParams['SECTION_CODE']?>/<?=$arElem["CODE"]?>/">Подробнее</a>
                            </div>
                       <? }
                        ?>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <? echo $NAV_STRING;?>
                <div class="page">
                    <?$arUF = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("IBLOCK_23_SECTION",$arResult['SECTION']['ID'],"UF_NAME2");?>
                    <h2 class="color-header"><?=$arUF['UF_NAME2']['VALUE']?></h2>
                </div>
                <div class="text_end">
                    <?$arUF = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("IBLOCK_23_SECTION",$arResult['SECTION']['ID'],"UF_TEXT2");?>
                    <p><?=$arUF['UF_TEXT2']['VALUE']?></p>
                </div>
            <? }else{ ?>
                    <div class="left_bar">
                        <form method="GET" action="/search/" class="search_in"><input name="name" type="text"> <a id="subm" href="#"></a></form>
                        <div class="clearfix"></div>
                        <div class="tag_cloud">
                            <span>Теги:</span>
                            <?
                            $arSelect = Array("ID", "NAME", "CODE");
                            $arFilter = Array("IBLOCK_ID"=>24, "ACTIVE"=>"Y");
                            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                            while($ob = $res->GetNextElement())
                            {
                                $arFields = $ob->GetFields();?>
                                <a href="/catalog/?tag=<?=$arFields['CODE']?>"><?=$arFields['NAME']?></a>
                            <? } ?>
                        </div>
                        <p>Каталог авторов:</p>

                        <div class="leftmenu">
                            <ul>
                                <?
                                $arFilter = array('IBLOCK_ID' => $arParams['IBLOCK_ID'],'DEPTH_LEVEL' => 2, 'SECTION_ID' => $arResult['SECTION']['IBLOCK_SECTION_ID']);
                                $arSelect = array("CODE", "NAME");
                                $rsElement =CIBlockSection::GetList(array("NAME" => "ASC"), $arFilter, false, $arSelect, false);
                                while($arElem = $rsElement->GetNext())
                                { $res = CIBlockSection::GetByID($arResult['SECTION']['IBLOCK_SECTION_ID']);
                                if($ar_res = $res->GetNext()) ?>
                                    <li><a href="/catalog/<?=$ar_res['CODE']?>/<?=$arElem["CODE"]?>/"><?=$arElem["NAME"]?></a></li>
                                <? }
                                ?>
                            </ul>
                        </div>
                    </div>
                    <div class="painter_hud">
                        <div class="content">
                            <div class="page">
                                <?$arUF = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("IBLOCK_23_SECTION",$arResult['SECTION']['ID'],"UF_ENMANE");?>
                                <h1 class="color-header"><?=$arResult['SECTION']['NAME']?> <?=$arUF['UF_ENMANE']['VALUE']?></h1>
                            </div>

                            <div class="for_painter">
                                <div class="painterface">
                                    <img src="<?=CFile::GetPath($arResult['SECTION']['PICTURE'])?>" alt="" class="a-image">
                                </div>
                                <div class="biography">
                                    <?$arUF = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("IBLOCK_23_SECTION",$arResult['SECTION']['ID'],"UF_DATE");?>
                                    <h2><?=$arUF['UF_DATE']['VALUE']?></h2>
                                    <p><?=$arResult['SECTION']['DESCRIPTION']?></p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <? global $arrFilter;
                            $arrFilter= Array("SUBSECTION" => $arResult['SECTION']['ID']); ?>
                            <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"section", 
	array(
		"COMPONENT_TEMPLATE" => "section",
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "23",
		"SECTION_ID" => "",
		"SECTION_CODE" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER2" => "desc",
		"FILTER_NAME" => "arrFilter",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "N",
		"PAGE_ELEMENT_COUNT" => "25",
		"LINE_ELEMENT_COUNT" => "5",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"OFFERS_LIMIT" => "5",
		"TEMPLATE_THEME" => "blue",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"SECTION_URL" => "/#SECTION_CODE#/",
		"DETAIL_URL" => "/catalog/#SECTION_CODE#/#CODE#/",
		"SECTION_ID_VARIABLE" => "SECTION_CODE_PATH",
		"SEF_MODE" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"SET_TITLE" => "Y",
		"SET_BROWSER_TITLE" => "Y",
		"BROWSER_TITLE" => "-",
		"SET_META_KEYWORDS" => "Y",
		"META_KEYWORDS" => "-",
		"SET_META_DESCRIPTION" => "Y",
		"META_DESCRIPTION" => "-",
		"SET_LAST_MODIFIED" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"CACHE_FILTER" => "N",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRICE_CODE" => array(
		),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"BASKET_URL" => "/personal/basket.php",
		"USE_PRODUCT_QUANTITY" => "N",
		"PRODUCT_QUANTITY_VARIABLE" => "",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRODUCT_PROPERTIES" => array(
		),
		"PAGER_TEMPLATE" => "round",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Товары",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"ADD_PICT_PROP" => "-",
		"LABEL_PROP" => "-",
		"SEF_RULE" => "/catalog/#SECTION_CODE_PATH#/",
		"SECTION_CODE_PATH" => $_REQUEST["SECTION_CODE_PATH"]
	),
	false
);?>
                            <div class="page">
                                <?$arUF = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("IBLOCK_23_SECTION",$arResult['SECTION']['ID'],"UF_NAME2");?>
                                <h2 class="color-header"><?=$arUF['UF_NAME2']['VALUE']?></h2>
                            </div>
                            <div class="text_end">
                                <?$arUF = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("IBLOCK_23_SECTION",$arResult['SECTION']['ID'],"UF_TEXT2");?>
                                <p><?=$arUF['UF_TEXT2']['VALUE']?></p>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
            <? } ?>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>