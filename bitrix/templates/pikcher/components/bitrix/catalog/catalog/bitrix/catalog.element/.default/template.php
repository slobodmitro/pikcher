<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$res = CIBlockElement::GetByID($arResult['ID']);
if($ar_res = $res->GetNext()) {
    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_*");
    $arFilter = Array("IBLOCK_ID" => $ar_res['IBLOCK_ID'], "ACTIVE" => "Y", "ID" => $ar_res['IBLOCK_SECTION_ID']);
    $res = CIBlockSection::GetList(Array(), $arFilter, false, $arSelect, false);
    while ($ob = $res->GetNext()) {
        $SECT=$ob;
    }
}
?>
        <div class="picture-type-block">
            <div class="container">
                <div class="page">
                    <?$arUF = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("IBLOCK_23_SECTION",$SECT['ID'],"UF_ENMANE");?>
                    <h1><?=$SECT['NAME']?> - <?=$arUF['UF_ENMANE']['VALUE']?></h1>
                    <h2 class="color-header">КАРТИНА <span><?=$arResult['NAME']?></span></h2>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="left_part">
                <div class="big_picture">
                    <img src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" alt="" class="a-image"/>
                </div>
                <div class="likethesame">
                    <p>Похожие картины:</p>

                    <div class="for_mamgin">
                        <? foreach($arResult['DISPLAY_PROPERTIES']['SIMILAR_PRODUCTS']['VALUE'] as $EID) {
                            $res = CIBlockElement::GetByID($EID);
                            $res->SetUrlTemplates("/catalog/#SECTION_CODE_PATH#/#ELEMENT_CODE#/");
                            if($ar_res = $res->GetNext()){
                            ?>
                            <a href="<?=$ar_res['DETAIL_PAGE_URL']?>" class="three_img">
                                <img src="<?=CFile::GetPath($ar_res['PREVIEW_PICTURE'])?>" alt="" class="a-image"/>
                            </a>
                        <? }} ?>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="tag_cloud">
                    <span>Теги:</span>
                    <? foreach($arResult['DISPLAY_PROPERTIES']['PRODUCT_TAGS']['VALUE'] as $EID) {
                    $res = CIBlockElement::GetByID($EID);
                    if($ar_res = $res->GetNext()){
                    ?>
                    <a href="/catalog/?tag=<?=$ar_res['CODE']?>"><?=$ar_res['NAME']?></a>
                    <? }} ?>
                </div>
                <div class="social">
                    <script type="text/javascript">(function() {
                            if (window.pluso)if (typeof window.pluso.start == "function") return;
                            if (window.ifpluso==undefined) { window.ifpluso = 1;
                                var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                                s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
                                s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
                                var h=d[g]('body')[0];
                                h.appendChild(s);
                            }})();</script>
                    <div class="pluso" data-background="transparent" data-options="medium,round,line,horizontal,counter,theme=04" data-services="vkontakte,odnoklassniki,facebook" data-url="http://pikcher_me.dev/kartina-page/" data-title="АБРАХАМ ВАН КАЛРАЕТ - ABRAHAM VAN CALRAET" data-description="КАРТИНА A HORSE WITH A SADDLE BESIDE IT"></div></div>
            </div>

            <div class="right_part">
                <div class="order-block">
                    <form method="post" action="" id="ajaxform_order" enctype="multipart/form-data" class="order-form">
                        <div class="chose-size form-block">
                            <div class="fblock-header">
                                <img class="fb-image" src="<?= SITE_TEMPLATE_PATH ?>/images/icons/size-icon.png" alt="">

                                <div class="fb-text">
                                    <? $APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        "",
                                        Array(
                                            "COMPONENT_TEMPLATE" => ".default",
                                            "AREA_FILE_SHOW" => "sect",
                                            "AREA_FILE_SUFFIX" => "inc1",
                                            "EDIT_TEMPLATE" => "",
                                            "AREA_FILE_RECURSIVE" => "Y"
                                        )
                                    ); ?>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="chose-size-select">
                                <select class="cs-select" name="size">
                                    <? foreach($arResult['DISPLAY_PROPERTIES']['SIZE']['VALUE'] as $key => $size){ ?>
                                        <option value="<?php echo $size ?>" data-price="<?php echo $arResult['DISPLAY_PROPERTIES']['SIZE']['DESCRIPTION'][$key]; ?>"><?php echo $size ?> (<?php echo $arResult['DISPLAY_PROPERTIES']['SIZE']['DESCRIPTION'][$key]; ?>р.)</option>
                                    <? } ?>
<!--                                    --><?// $APPLICATION->IncludeComponent(
//                                        "bitrix:news.list",
//                                        "rozmer",
//                                        array(
//                                            "COMPONENT_TEMPLATE" => "rozmer",
//                                            "IBLOCK_TYPE" => "simple",
//                                            "IBLOCK_ID" => "18",
//                                            "NEWS_COUNT" => "20",
//                                            "SORT_BY1" => "TIMESTAMP_X",
//                                            "SORT_ORDER1" => "ASC",
//                                            "SORT_BY2" => "SORT",
//                                            "SORT_ORDER2" => "ASC",
//                                            "FILTER_NAME" => "",
//                                            "FIELD_CODE" => array(
//                                                0 => "",
//                                                1 => "",
//                                            ),
//                                            "PROPERTY_CODE" => array(
//                                                0 => "PRICE",
//                                                1 => "HOLST",
//                                                2 => "FACTUR",
//                                                3 => "BAGET",
//                                                4 => "OBROB",
//                                                5 => "SHARS",
//                                            ),
//                                            "CHECK_DATES" => "Y",
//                                            "DETAIL_URL" => "",
//                                            "AJAX_MODE" => "N",
//                                            "AJAX_OPTION_JUMP" => "N",
//                                            "AJAX_OPTION_STYLE" => "Y",
//                                            "AJAX_OPTION_HISTORY" => "N",
//                                            "AJAX_OPTION_ADDITIONAL" => "",
//                                            "CACHE_TYPE" => "A",
//                                            "CACHE_TIME" => "36000000",
//                                            "CACHE_FILTER" => "N",
//                                            "CACHE_GROUPS" => "Y",
//                                            "PREVIEW_TRUNCATE_LEN" => "",
//                                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
//                                            "SET_TITLE" => "Y",
//                                            "SET_BROWSER_TITLE" => "Y",
//                                            "SET_META_KEYWORDS" => "Y",
//                                            "SET_META_DESCRIPTION" => "Y",
//                                            "SET_LAST_MODIFIED" => "N",
//                                            "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
//                                            "ADD_SECTIONS_CHAIN" => "Y",
//                                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
//                                            "PARENT_SECTION" => "",
//                                            "PARENT_SECTION_CODE" => "",
//                                            "INCLUDE_SUBSECTIONS" => "Y",
//                                            "DISPLAY_DATE" => "Y",
//                                            "DISPLAY_NAME" => "Y",
//                                            "DISPLAY_PICTURE" => "Y",
//                                            "DISPLAY_PREVIEW_TEXT" => "Y",
//                                            "PAGER_TEMPLATE" => ".default",
//                                            "DISPLAY_TOP_PAGER" => "N",
//                                            "DISPLAY_BOTTOM_PAGER" => "Y",
//                                            "PAGER_TITLE" => "Новости",
//                                            "PAGER_SHOW_ALWAYS" => "N",
//                                            "PAGER_DESC_NUMBERING" => "N",
//                                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
//                                            "PAGER_SHOW_ALL" => "N",
//                                            "PAGER_BASE_LINK_ENABLE" => "N",
//                                            "SET_STATUS_404" => "N",
//                                            "SHOW_404" => "N",
//                                            "MESSAGE_404" => ""
//                                        ),
//                                        false
//                                    ); ?>
                                </select>
                                <input id="start_price" hidden type="text" name="start_price"/>

                                <div class="size-price">= 1300 <i class="fa fa-rub"></i></div>
                            </div>

                        </div>
                        <div class="decoration form-block">
                            <div class="fblock-header">
                                <img class="fb-image" src="<?= SITE_TEMPLATE_PATH ?>/images/icons/size-icon.png" alt="">

                                <div class="fb-text">
                                    <? $APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        "",
                                        Array(
                                            "COMPONENT_TEMPLATE" => ".default",
                                            "AREA_FILE_SHOW" => "sect",
                                            "AREA_FILE_SUFFIX" => "inc5",
                                            "EDIT_TEMPLATE" => "",
                                            "AREA_FILE_RECURSIVE" => "Y"
                                        )
                                    ); ?>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="accordion">
                                <h3>
                                    <div class="price-about">
                                        <div class="purple-checkbox">
                                            <input type="checkbox" id="canvas" name="canvas" value="0"/>
                                            <label for="canvas"></label>
                                        </div>
                                        <div class="pa-name trigger" data-index="0">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc3000",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </div>
                                        <div class="pa-price"> +
                                    <span id="canvas-price-label">

                                    <? $APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        "",
                                        Array(
                                            "COMPONENT_TEMPLATE" => ".default",
                                            "AREA_FILE_SHOW" => "sect",
                                            "AREA_FILE_SUFFIX" => "inc3001",
                                            "EDIT_TEMPLATE" => "",
                                            "AREA_FILE_RECURSIVE" => "Y"
                                        )
                                    ); ?>
                                </span>
                                            <i class="fa fa-rub"></i></div>
                                        <div class="pa-about trigger" data-index="0">Подробнeе</div>
                                        <div class="clearfix"></div>
                                    </div>
                                </h3>
                                <div class="canvas">
                                    <div class="item item1">
                                        <?php
                                        $file = file_get_contents('sect_inc9000.php');
                                        $spos = strpos($file, 'src="') + 5;
                                        $subs = substr($file, $spos);
                                        $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                                        ?>
                                        <a class="fancy2" href="<?= $furl ?>">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc9000",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </a>

                                        <div class="hint">
                                            <div class="hint-text">
                                                <? $APPLICATION->IncludeComponent(
                                                    "bitrix:main.include",
                                                    "",
                                                    Array(
                                                        "COMPONENT_TEMPLATE" => ".default",
                                                        "AREA_FILE_SHOW" => "sect",
                                                        "AREA_FILE_SUFFIX" => "inc3002",
                                                        "EDIT_TEMPLATE" => "",
                                                        "AREA_FILE_RECURSIVE" => "Y"
                                                    )
                                                ); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item item2">
                                        <?php
                                        $file = file_get_contents('sect_inc9001.php');
                                        $spos = strpos($file, 'src="') + 5;
                                        $subs = substr($file, $spos);
                                        $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                                        ?>
                                        <a class="fancy2" href="<?= $furl ?>">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc9001",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </a>

                                        <div class="hint  ">
                                            <div class="hint-text">
                                                <? $APPLICATION->IncludeComponent(
                                                    "bitrix:main.include",
                                                    "",
                                                    Array(
                                                        "COMPONENT_TEMPLATE" => ".default",
                                                        "AREA_FILE_SHOW" => "sect",
                                                        "AREA_FILE_SUFFIX" => "inc3003",
                                                        "EDIT_TEMPLATE" => "",
                                                        "AREA_FILE_RECURSIVE" => "Y"
                                                    )
                                                ); ?>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <h3>
                                    <div class="price-about">
                                        <div class="purple-checkbox">
                                            <input type="checkbox" id="cover" name="cover" value="600"/>
                                            <label for="cover"></label>
                                        </div>
                                        <div class="pa-name trigger" data-index="1">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc3004",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </div>
                                        <div class="pa-price"> +
                                    <span id="cover-price-label">

                                    <? $APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        "",
                                        Array(
                                            "COMPONENT_TEMPLATE" => ".default",
                                            "AREA_FILE_SHOW" => "sect",
                                            "AREA_FILE_SUFFIX" => "inc3005",
                                            "EDIT_TEMPLATE" => "",
                                            "AREA_FILE_RECURSIVE" => "Y"
                                        )
                                    ); ?>
                                    </span>
                                            <i class="fa fa-rub"></i></div>
                                        <div class="pa-about trigger" data-index="1">Подробнeе</div>
                                        <div class="clearfix"></div>
                                    </div>
                                </h3>
                                <div class="cover  ">
                                    <div class="item item1">
                                        <?php
                                        $file = file_get_contents('sect_inc9025.php');
                                        $spos = strpos($file, 'src="') + 5;
                                        $subs = substr($file, $spos);
                                        $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                                        ?>
                                        <a class="fancy2" href="<?= $furl ?>">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc9025",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </a>
                                    </div>
                                    <div class="item item2">
                                        <?php
                                        $file = file_get_contents('sect_inc9002.php');
                                        $spos = strpos($file, 'src="') + 5;
                                        $subs = substr($file, $spos);
                                        $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                                        ?>
                                        <a class="fancy2" href="<?= $furl ?>">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc9002",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </a>

                                        <div class="hint hint-above">
                                            <div class="hint-text">
                                                <? $APPLICATION->IncludeComponent(
                                                    "bitrix:main.include",
                                                    "",
                                                    Array(
                                                        "COMPONENT_TEMPLATE" => ".default",
                                                        "AREA_FILE_SHOW" => "sect",
                                                        "AREA_FILE_SUFFIX" => "inc3006",
                                                        "EDIT_TEMPLATE" => "",
                                                        "AREA_FILE_RECURSIVE" => "Y"
                                                    )
                                                ); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

                        <div class="frame form-block">

                            <div class="fblock-header">
                                <img class="fb-image" src="<?= SITE_TEMPLATE_PATH ?>/images/icons/frame-icon.png"
                                     alt="">

                                <div class="fb-text">
                                    <? $APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        "",
                                        Array(
                                            "COMPONENT_TEMPLATE" => ".default",
                                            "AREA_FILE_SHOW" => "sect",
                                            "AREA_FILE_SUFFIX" => "inc6",
                                            "EDIT_TEMPLATE" => "",
                                            "AREA_FILE_RECURSIVE" => "Y"
                                        )
                                    ); ?>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="accordion check-control">
                                <h3>
                                    <div class="price-about">
                                        <div class="purple-checkbox cb-to-control">
                                            <input type="checkbox" id="frame" name="frame" value="0"/>
                                            <label for="frame"></label>
                                        </div>
                                        <div class="pa-name trigger" data-index="0">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc3007",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </div>
                                        <div class="pa-price">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc3008",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?></div>
                                        <div class="pa-about trigger" data-index="0">Подробнeе</div>
                                        <div class="clearfix"></div>
                                    </div>
                                </h3>
                                <div class="full-frame">
                                    <div class="item item1">
                                        <?php
                                        $file = file_get_contents('sect_inc9003.php');
                                        $spos = strpos($file, 'src="') + 5;
                                        $subs = substr($file, $spos);
                                        $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                                        ?>
                                        <a class="fancy2" href="<?= $furl ?>">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc9003",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <?php
                                        $file = file_get_contents('sect_inc9004.php');
                                        $spos = strpos($file, 'src="') + 5;
                                        $subs = substr($file, $spos);
                                        $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                                        ?>
                                        <a class="fancy2" href="<?= $furl ?>">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc9004",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </a>

                                        <div class="hint">
                                            <div class="hint-text">
                                                <? $APPLICATION->IncludeComponent(
                                                    "bitrix:main.include",
                                                    "",
                                                    Array(
                                                        "COMPONENT_TEMPLATE" => ".default",
                                                        "AREA_FILE_SHOW" => "sect",
                                                        "AREA_FILE_SUFFIX" => "inc3009",
                                                        "EDIT_TEMPLATE" => "",
                                                        "AREA_FILE_RECURSIVE" => "Y"
                                                    )
                                                ); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <h3>
                                    <div class="price-about">
                                        <div class="purple-checkbox cb-to-control">
                                            <input type="checkbox" id="baget" name="baget" value="600"/>
                                            <label for="baget"></label>
                                        </div>
                                        <div class="pa-name trigger" data-index="1">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc3010",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </div>
                                        <div class="pa-price"> +
                                    <span id="baget-price-label">
                                    <? $APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        "",
                                        Array(
                                            "COMPONENT_TEMPLATE" => ".default",
                                            "AREA_FILE_SHOW" => "sect",
                                            "AREA_FILE_SUFFIX" => "inc3011",
                                            "EDIT_TEMPLATE" => "",
                                            "AREA_FILE_RECURSIVE" => "Y"
                                        )
                                    ); ?>
                                    </span>
                                            <i class="fa fa-rub"></i></div>
                                        <div class="pa-about trigger" data-index="1">Подробнeе</div>
                                        <div class="clearfix"></div>
                                    </div>
                                </h3>
                                <div class="corner-frame check-control">
                                    <div class="item">
                                        <div class="purple-checkbox cb-to-control parent-on">
                                            <input type="checkbox" id="baget1" name="baget1" value="600"/>
                                            <label for="baget1"></label>
                                        </div>
                                        <?php
                                        $file = file_get_contents('sect_inc9005.php');
                                        $spos = strpos($file, 'src="') + 5;
                                        $subs = substr($file, $spos);
                                        $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                                        ?>
                                        <a class="fancy2" href="<?= $furl ?>">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc9005",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <div class="purple-checkbox cb-to-control parent-on">
                                            <input type="checkbox" id="baget2" name="baget2" value="600"/>
                                            <label for="baget2"></label>
                                        </div>
                                        <?php
                                        $file = file_get_contents('sect_inc9006.php');
                                        $spos = strpos($file, 'src="') + 5;
                                        $subs = substr($file, $spos);
                                        $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                                        ?>
                                        <a class="fancy2" href="<?= $furl ?>">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc9006",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <div class="purple-checkbox cb-to-control parent-on">
                                            <input type="checkbox" id="baget3" name="baget3" value="600"/>
                                            <label for="baget3"></label>
                                        </div>
                                        <?php
                                        $file = file_get_contents('sect_inc9007.php');
                                        $spos = strpos($file, 'src="') + 5;
                                        $subs = substr($file, $spos);
                                        $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                                        ?>
                                        <a class="fancy2" href="<?= $furl ?>">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc9007",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <div class="purple-checkbox cb-to-control parent-on">
                                            <input type="checkbox" id="baget4" name="baget4" value="600"/>
                                            <label for="baget4"></label>
                                        </div>
                                        <?php
                                        $file = file_get_contents('sect_inc9008.php');
                                        $spos = strpos($file, 'src="') + 5;
                                        $subs = substr($file, $spos);
                                        $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                                        ?>
                                        <a class="fancy2" href="<?= $furl ?>">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc9008",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <div class="purple-checkbox cb-to-control parent-on">
                                            <input type="checkbox" id="baget5" name="baget5" value="600"/>
                                            <label for="baget5"></label>
                                        </div>
                                        <?php
                                        $file = file_get_contents('sect_inc90071.php');
                                        $spos = strpos($file, 'src="') + 5;
                                        $subs = substr($file, $spos);
                                        $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                                        ?>
                                        <a class="fancy2" href="<?= $furl ?>">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc90071",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <div class="purple-checkbox cb-to-control parent-on">
                                            <input type="checkbox" id="baget6" name="baget6" value="600"/>
                                            <label for="baget6"></label>
                                        </div>
                                        <?php
                                        $file = file_get_contents('sect_inc90072.php');
                                        $spos = strpos($file, 'src="') + 5;
                                        $subs = substr($file, $spos);
                                        $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                                        ?>
                                        <a class="fancy2" href="<?= $furl ?>">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc90072",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <div class="purple-checkbox cb-to-control parent-on">
                                            <input type="checkbox" id="baget7" name="baget7" value="600"/>
                                            <label for="baget7"></label>
                                        </div>
                                        <?php
                                        $file = file_get_contents('sect_inc90073.php');
                                        $spos = strpos($file, 'src="') + 5;
                                        $subs = substr($file, $spos);
                                        $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                                        ?>
                                        <a class="fancy2" href="<?= $furl ?>">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc90073",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <div class="purple-checkbox cb-to-control parent-on">
                                            <input type="checkbox" id="baget8" name="baget8" value="600"/>
                                            <label for="baget8"></label>
                                        </div>
                                        <?php
                                        $file = file_get_contents('sect_inc90074.php');
                                        $spos = strpos($file, 'src="') + 5;
                                        $subs = substr($file, $spos);
                                        $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                                        ?>
                                        <a class="fancy2" href="<?= $furl ?>">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc90074",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <div class="purple-checkbox cb-to-control parent-on">
                                            <input type="checkbox" id="baget9" name="baget9" value="600"/>
                                            <label for="baget9"></label>
                                        </div>
                                        <?php
                                        $file = file_get_contents('sect_inc90075.php');
                                        $spos = strpos($file, 'src="') + 5;
                                        $subs = substr($file, $spos);
                                        $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                                        ?>
                                        <a class="fancy2" href="<?= $furl ?>">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc90075",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <div class="purple-checkbox cb-to-control parent-on">
                                            <input type="checkbox" id="baget10" name="baget10" value="600"/>
                                            <label for="baget10"></label>
                                        </div>
                                        <?php
                                        $file = file_get_contents('sect_inc90076.php');
                                        $spos = strpos($file, 'src="') + 5;
                                        $subs = substr($file, $spos);
                                        $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                                        ?>
                                        <a class="fancy2" href="<?= $furl ?>">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc90076",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <div class="purple-checkbox cb-to-control parent-on">
                                            <input type="checkbox" id="baget11" name="baget11" value="600"/>
                                            <label for="baget11"></label>
                                        </div>
                                        <?php
                                        $file = file_get_contents('sect_inc90077.php');
                                        $spos = strpos($file, 'src="') + 5;
                                        $subs = substr($file, $spos);
                                        $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                                        ?>
                                        <a class="fancy2" href="<?= $furl ?>">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc90077",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <div class="purple-checkbox cb-to-control parent-on">
                                            <input type="checkbox" id="baget12" name="baget12" value="600"/>
                                            <label for="baget12"></label>
                                        </div>
                                        <?php
                                        $file = file_get_contents('sect_inc90078.php');
                                        $spos = strpos($file, 'src="') + 5;
                                        $subs = substr($file, $spos);
                                        $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                                        ?>
                                        <a class="fancy2" href="<?= $furl ?>">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc90078",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </a>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="hint hint-above">
                                        <div class="hint-text">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc3012",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="processing form-block">

                            <div class="fblock-header">
                                <img class="fb-image" src="<?= SITE_TEMPLATE_PATH ?>/images/icons/brush-icon.png"
                                     alt="">

                                <div class="fb-text">
                                    <? $APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        "",
                                        Array(
                                            "COMPONENT_TEMPLATE" => ".default",
                                            "AREA_FILE_SHOW" => "sect",
                                            "AREA_FILE_SUFFIX" => "inc7",
                                            "EDIT_TEMPLATE" => "",
                                            "AREA_FILE_RECURSIVE" => "Y"
                                        )
                                    ); ?>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="accordion check-control">
                                <h3>
                                    <div class="price-about">
                                        <div class="purple-checkbox cb-to-control">
                                            <input type="checkbox" id="correction" name="correction" value="0"/>
                                            <label for="correction"></label>
                                        </div>
                                        <div class="pa-name trigger" data-index="0">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc3013",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </div>
                                        <div class="pa-price">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc3014",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </div>
                                        <div class="pa-about trigger" data-index="0">Подробнeе</div>
                                        <div class="clearfix"></div>
                                    </div>
                                </h3>
                                <div class="correction">
                                    <div class="item">
                                        <?php
                                        $file = file_get_contents('sect_inc9010.php');
                                        $spos = strpos($file, 'src="') + 5;
                                        $subs = substr($file, $spos);
                                        $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                                        ?>
                                        <a class="fancy2" href="<?= $furl ?>">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc9010",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <?php
                                        $file = file_get_contents('sect_inc9011.php');
                                        $spos = strpos($file, 'src="') + 5;
                                        $subs = substr($file, $spos);
                                        $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                                        ?>
                                        <a class="fancy2" href="<?= $furl ?>">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc9011",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </a>

                                        <div class="hint">
                                            <div class="hint-text">
                                                <? $APPLICATION->IncludeComponent(
                                                    "bitrix:main.include",
                                                    "",
                                                    Array(
                                                        "COMPONENT_TEMPLATE" => ".default",
                                                        "AREA_FILE_SHOW" => "sect",
                                                        "AREA_FILE_SUFFIX" => "inc3015",
                                                        "EDIT_TEMPLATE" => "",
                                                        "AREA_FILE_RECURSIVE" => "Y"
                                                    )
                                                ); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <h3>
                                    <div class="price-about">
                                        <div class="purple-checkbox cb-to-control">
                                            <input type="checkbox" id="art-processing" name="art-processing"
                                                   value="600"/>
                                            <label for="art-processing"></label>
                                        </div>
                                        <div class="pa-name trigger" data-index="1">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc3016",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </div>
                                        <div class="pa-price">+
            <span id="art-processing-price-label">
            <? $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "AREA_FILE_SHOW" => "sect",
                    "AREA_FILE_SUFFIX" => "inc3017",
                    "EDIT_TEMPLATE" => "",
                    "AREA_FILE_RECURSIVE" => "Y"
                )
            ); ?>
            </span>
                                            <i class="fa fa-rub"></i></div>
                                        <div class="pa-about trigger" data-index="1">Подробнeе</div>
                                        <div class="clearfix"></div>
                                    </div>
                                </h3>
                                <div class="art-processing">
                                    <div class="item">
                                        <?php
                                        $file = file_get_contents('sect_inc9012.php');
                                        $spos = strpos($file, 'src="') + 5;
                                        $subs = substr($file, $spos);
                                        $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                                        ?>
                                        <a class="fancy2" href="<?= $furl ?>">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc9012",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <?php
                                        $file = file_get_contents('sect_inc9013.php');
                                        $spos = strpos($file, 'src="') + 5;
                                        $subs = substr($file, $spos);
                                        $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                                        ?>
                                        <a class="fancy2" href="<?= $furl ?>">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc9013",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <?php
                                        $file = file_get_contents('sect_inc9014.php');
                                        $spos = strpos($file, 'src="') + 5;
                                        $subs = substr($file, $spos);
                                        $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                                        ?>
                                        <a class="fancy2" href="<?= $furl ?>">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc9014",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <?php
                                        $file = file_get_contents('sect_inc9015.php');
                                        $spos = strpos($file, 'src="') + 5;
                                        $subs = substr($file, $spos);
                                        $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                                        ?>
                                        <a class="fancy2" href="<?= $furl ?>">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc9015",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <?php
                                        $file = file_get_contents('sect_inc90165.php');
                                        $spos = strpos($file, 'src="') + 5;
                                        $subs = substr($file, $spos);
                                        $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                                        ?>
                                        <a class="fancy2" href="<?= $furl ?>">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc90165",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <?php
                                        $file = file_get_contents('sect_inc901212.php');
                                        $spos = strpos($file, 'src="') + 5;
                                        $subs = substr($file, $spos);
                                        $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                                        ?>
                                        <a class="fancy2" href="<?= $furl ?>">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc901212",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </a>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="hint hint-above">
                                        <div class="hint-text">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc3018",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </div>
                                    </div>
                                </div>
                                <h3>
                                    <div class="price-about">
                                        <div class="purple-checkbox cb-to-control">
                                            <input type="checkbox" id="art-processing2" name="art-processing2"
                                                   value="<?php include "sect_inc3020.php" ?>"/>
                                            <label for="art-processing2"></label>
                                        </div>
                                        <div class="pa-name trigger" data-index="2">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc3019",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </div>
                                        <div class="pa-price">+
            <span id="art-processing2-price-label">
            <? $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "AREA_FILE_SHOW" => "sect",
                    "AREA_FILE_SUFFIX" => "inc3020",
                    "EDIT_TEMPLATE" => "",
                    "AREA_FILE_RECURSIVE" => "Y"
                )
            ); ?>
            </span>
                                            <i class="fa fa-rub"></i></div>
                                        <div class="pa-about trigger" data-index="2">Подробнeе</div>
                                        <div class="clearfix"></div>
                                    </div>
                                </h3>
                                <div class="caricature">
                                    <div class="item">
                                        <?php
                                        $file = file_get_contents('sect_inc9016.php');
                                        $spos = strpos($file, 'src="') + 5;
                                        $subs = substr($file, $spos);
                                        $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                                        ?>
                                        <a class="fancy2" href="<?= $furl ?>">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc9016",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <?php
                                        $file = file_get_contents('sect_inc9017.php');
                                        $spos = strpos($file, 'src="') + 5;
                                        $subs = substr($file, $spos);
                                        $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                                        ?>
                                        <a class="fancy2" href="<?= $furl ?>">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc9017",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <?php
                                        $file = file_get_contents('sect_inc901711.php');
                                        $spos = strpos($file, 'src="') + 5;
                                        $subs = substr($file, $spos);
                                        $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                                        ?>
                                        <a class="fancy2" href="<?= $furl ?>">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc901711",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <?php
                                        $file = file_get_contents('sect_inc901712.php');
                                        $spos = strpos($file, 'src="') + 5;
                                        $subs = substr($file, $spos);
                                        $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                                        ?>
                                        <a class="fancy2" href="<?= $furl ?>">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc901712",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <?php
                                        $file = file_get_contents('sect_inc901713.php');
                                        $spos = strpos($file, 'src="') + 5;
                                        $subs = substr($file, $spos);
                                        $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                                        ?>
                                        <a class="fancy2" href="<?= $furl ?>">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc901713",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <?php
                                        $file = file_get_contents('sect_inc901714.php');
                                        $spos = strpos($file, 'src="') + 5;
                                        $subs = substr($file, $spos);
                                        $furl = substr($subs, 0, strpos(substr($file, $spos), '"'));
                                        ?>
                                        <a class="fancy2" href="<?= $furl ?>">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc901714",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </a>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="hint hint-above">
                                        <div class="hint-text">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "AREA_FILE_SHOW" => "sect",
                                                    "AREA_FILE_SUFFIX" => "inc3021",
                                                    "EDIT_TEMPLATE" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y"
                                                )
                                            ); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="conditionbuy"><a href="#">Условия доставки и оплаты</a></div>
                        <div class="request">
                            <div class="form-block">
                                <div class="list">
                                    <div class="bottom">
                                        <div class="sum">
                                            <div class="sum-text">
                                                <? $APPLICATION->IncludeComponent(
                                                    "bitrix:main.include",
                                                    "",
                                                    Array(
                                                        "COMPONENT_TEMPLATE" => ".default",
                                                        "AREA_FILE_SHOW" => "sect",
                                                        "AREA_FILE_SUFFIX" => "inc2011",
                                                        "EDIT_TEMPLATE" => "",
                                                        "AREA_FILE_RECURSIVE" => "Y"
                                                    )
                                                ); ?>
                                            </div>
                                            <div class="sum-amount"><span class="value">0</span> <i
                                                    class="fa fa-rub"></i></div>
                                        </div>

                                        <input hidden id="all_price" type="text" name="all_price"/>



                                        <button type="submit" class="purple-button">
                                            <a class="pb-link form_des">ЗАКАЗАТЬ</a>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="two_big_h1">
                <h1><?=$SECT['NAME']?></h1>

                <h2>КАРТИНА <span> «<?=$arResult['NAME']?>»</span></h2>
            </div>
        </div>
        <div class="slider">
            <div class="container">
                <div class="color-header">А вот и <span class="text-bold">эмоции</span> наших клиентов</div>
                <div class="slider-control">
                    <div class="slider-prev"></div>
                    <div class="slider-next"></div>
                </div>
            </div>
            <div class="slider-opacity">
                <div class="left-side"></div>
                <div class="right-side"></div>
            </div>
            <div class="two-rows-carousel">
                <? $APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"slider_emotion", 
	array(
		"COMPONENT_TEMPLATE" => "slider_emotion",
		"IBLOCK_TYPE" => "slider",
		"IBLOCK_ID" => "4",
		"NEWS_COUNT" => "20",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "Y",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => ""
	),
	false
); ?>

            </div>
        </div>

        <div class="page">
            <h2 class="color-header"><?=$arResult['PREVIEW_TEXT']?></h2>
        </div>
        <div class="container">
            <div class="text_end">
                <p><?=$arResult['DETAIL_TEXT'];?></p>
            </div>
        </div>
        <div class="tobuy">
            <div class="form-to-close">
                <div class="form-wraper">
                    <form method="post" action="" id="ajaxform" class="send-form" enctype="multipart/form-data">
                        <div class="form-header">Абрахам ван Калрает - Abraham van Calraet</div>
                        <div class="form-small-text">A Horse with a Saddle Beside it</div>
                        <div class="left_in">
                            <p>Заполните форму</p>
                            <input type="text" class="form-input" name="name" placeholder="Ваше имя"/>

                            <div class="help-text">Для связи с Вами</div>
                            <div class="clearfix"></div>
                            <input type="text" class="form-input" name="phone" placeholder="Ваш телефон"/>

                            <div class="help-text">Для связи с Вами</div>
                            <div class="clearfix"></div>
                            <input type="email" class="form-input" name="email" placeholder="Ваш e-mail"/>

                            <div class="help-text">Чтобы отправить Вам готовый макет картины</div>
                            <div class="clearfix"></div>
                            <div class="price">Цена: <span>2500 р.</span></div>
                            <div class="manager">
                                Наш менеджер свяжется с вами по вопросам оплаты и доставки
                            </div>
                        </div>
                        <div class="right_in">
                            <textarea></textarea>
                            <button type="submit" class="send-button">ОТПРАВИТЬ ЗАЯВКУ</button>
                        </div>
                        <div class="clearfix"></div>
                        <img class="close-form" src="<?= SITE_TEMPLATE_PATH ?>/images/close-popup.png" alt="">
                    </form>
                </div>
            </div>
        </div>
        <div class="thanks_for">
            <a href="#"><img class="close-thanks" src="/bitrix/templates/pikcher/images/close-popup.png" alt=""></a>

            <div class="thank_you">
                Спасибо за Вашу заявку!
            </div>
            <p>Наш менеджер свяжется с вами <br/>
                <span>в течение 3-х часов.</span></p>
        </div>
    </div>