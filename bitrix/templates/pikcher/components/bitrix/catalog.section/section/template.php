<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
    <div class="rene">
    <div class="line5pic">
<? foreach($arResult['ITEMS'] as $item) { ?>
    <div class="item">
        <p><?=$item['NAME']?></p>
        <div class="for_img">
            <img src="<?=$item['PREVIEW_PICTURE']['SRC']?>" alt="" class="a-image">
        </div>
        <?

        $res = CIBlockElement::GetByID($item['ID']);
        if($ar_res = $res->GetNext())
        {
            $res2 = CIBlockSection::GetByID($ar_res['IBLOCK_SECTION_ID']);
            if($ar_res2 = $res2->GetNext()){
                $res3 = CIBlockSection::GetByID($ar_res2['IBLOCK_SECTION_ID']);
                if($ar_res3 = $res3->GetNext()){?>
                <a href="/catalog/<?=$ar_res3['CODE']?>/<?=$ar_res2['CODE']?>/<?=$item['CODE']?>/">Подробнее</a>
            <? }}}
        ?>
    </div>
    <? } ?>
    </div>
    </div>
    <div class="clearfix"></div>
	<? if ($arParams["DISPLAY_BOTTOM_PAGER"])
	{
		?><? echo $arResult["NAV_STRING"]; ?><?
	}
?>