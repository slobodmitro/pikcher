<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Репродукция");
?>
    <style>
        footer {
            margin-top: 0px !important;
        }
    </style>

    <div class="reproduct">
        <div class="picture-type-block">
            <div class="container">
                <div class="bread_crumb">
                    <ul>
                        <li><a href="#">Главная</a></li>
                        <li><a href="#">Виды картин</a></li>
                        <li><a href="#">Репродукции картин</a></li>
                    </ul>
                </div>
                <div class="page">
                    <h2 class="color-header">ПЕЧАТЬ КАРТИН НА ХОЛСТЕ</h2>
                </div>
                <div class="works">
                    <div class="item">
                        <div class="for_img">
                            <img src="<?= SITE_TEMPLATE_PATH ?>/images/reproduktsia/horse.jpg" alt="" class="a-image"/>
                            <div class="for_img_name">
                                Репродукции картин  художников
                            </div>
                        </div>
                        <a href="#">Подробнее</a>
                    </div>
                    <div class="item">
                        <div class="for_img">
                            <img src="<?= SITE_TEMPLATE_PATH ?>/images/reproduktsia/poster.jpg" alt="" class="a-image"/>
                            <div class="for_img_name">
                                постеры на холсте для интерьера
                            </div>
                        </div>
                        <a href="#">Подробнее</a>
                    </div>
                    <div class="item">
                        <div class="for_img">
                            <img src="<?= SITE_TEMPLATE_PATH ?>/images/reproduktsia/serjant.jpg" alt=""
                                 class="a-image"/>
                            <div class="for_img_name">
                                Фотопортрет с обработкой
                            </div>
                        </div>
                        <a href="#">Подробнее</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="left_bar">
                    <div class="search_in"><input type="text"/> <a href="#"></a></div>
                    <div class="clearfix"></div>
                    <div class="tag_cloud">
                        <span>Теги:</span> <a href="#">природа,</a>
                        <a href="#">пейзаж,</a>
                        <a href="#">люди,</a>
                        <a href="#">портрет,</a>
                        <a href="#">русские художники,</a>
                        <a href="#">море</a>
                    </div>
                    <p>Каталог авторов:</p>

                    <div class="leftmenu">
                        <ul>
                            <li><a href="#">Абрахам ван Калрает</a></li>
                            <li><a href="#">Адам Эльсхеймер</a></li>
                            <li><a href="#">Адольф Монтичелли</a></li>
                            <li><a href="#">Адриан ван де Велде</a></li>
                            <li><a href="#">Адриан ван Остаде</a></li>
                            <li><a href="#">Айвазовский И.К.</a></li>
                            <li><a href="#">Александр Джеймисон</a></li>
                            <li><a href="#">Альбер Глез</a></li>
                            <li><a href="#">Альберт Бирштадт</a></li>
                            <li><a href="#">Абрахам ван Калрает</a></li>
                            <li><a href="#">Адам Эльсхеймер</a></li>
                            <li><a href="#">Адольф Монтичелли</a></li>
                            <li><a href="#">Адриан ван де Велде</a></li>
                            <li><a href="#">Адриан ван Остаде</a></li>
                            <li><a href="#">Айвазовский И.К.</a></li>
                            <li><a href="#">Александр Джеймисон</a></li>
                            <li><a href="#">Альбер Глез</a></li>
                            <li><a href="#">Альберт Бирштадт</a></li>
                            <li><a href="#">Абрахам ван Калрает</a></li>
                            <li><a href="#">Адам Эльсхеймер</a></li>
                            <li><a href="#">Адольф Монтичелли</a></li>
                            <li><a href="#">Адриан ван де Велде</a></li>
                            <li><a href="#">Адриан ван Остаде</a></li>
                            <li><a href="#">Айвазовский И.К.</a></li>
                            <li><a href="#">Александр Джеймисон</a></li>
                            <li><a href="#">Альбер Глез</a></li>
                            <li><a href="#">Альберт Бирштадт</a></li>
                            <li><a href="#">Абрахам ван Калрает</a></li>
                            <li><a href="#">Адам Эльсхеймер</a></li>
                            <li><a href="#">Адольф Монтичелли</a></li>
                            <li><a href="#">Адриан ван де Велде</a></li>
                            <li><a href="#">Адриан ван Остаде</a></li>
                            <li><a href="#">Айвазовский И.К.</a></li>
                            <li><a href="#">Александр Джеймисон</a></li>
                            <li><a href="#">Альбер Глез</a></li>
                            <li><a href="#">Альберт Бирштадт</a></li>
                            <li><a href="#">Абрахам ван Калрает</a></li>
                            <li><a href="#">Адам Эльсхеймер</a></li>
                            <li><a href="#">Адольф Монтичелли</a></li>
                            <li><a href="#">Адриан ван де Велде</a></li>
                            <li><a href="#">Адриан ван Остаде</a></li>
                            <li><a href="#">Айвазовский И.К.</a></li>
                            <li><a href="#">Александр Джеймисон</a></li>
                            <li><a href="#">Альбер Глез</a></li>
                            <li><a href="#">Альберт Бирштадт</a></li>
                            <li><a href="#">Абрахам ван Калрает</a></li>
                            <li><a href="#">Адам Эльсхеймер</a></li>
                            <li><a href="#">Адольф Монтичелли</a></li>
                            <li><a href="#">Адриан ван де Велде</a></li>
                            <li><a href="#">Адриан ван Остаде</a></li>
                            <li><a href="#">Айвазовский И.К.</a></li>
                            <li><a href="#">Александр Джеймисон</a></li>
                            <li><a href="#">Альбер Глез</a></li>
                            <li><a href="#">Альберт Бирштадт</a></li>
                            <li><a href="#">Абрахам ван Калрает</a></li>
                            <li><a href="#">Адам Эльсхеймер</a></li>
                            <li><a href="#">Адольф Монтичелли</a></li>
                            <li><a href="#">Адриан ван де Велде</a></li>
                            <li><a href="#">Адриан ван Остаде</a></li>
                            <li><a href="#">Айвазовский И.К.</a></li>
                            <li><a href="#">Александр Джеймисон</a></li>
                            <li><a href="#">Альбер Глез</a></li>
                            <li><a href="#">Альберт Бирштадт</a></li>
                        </ul>
                    </div>
                </div>
                <div class="content">
                    <div class="page">
                        <h2 class="color-header">РЕПРОДУКЦИИ КАРТИН</h2>
                    </div>
                    <div class="rene">
                        <div class="line5pic">
                            <div class="item">
                                <p>Абрахам ван Калрает</p>

                                <div class="for_img">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/reproduktsia/1.jpg" alt=""
                                         class="a-image"/>
                                </div>
                                <a href="#">Подробнее</a>
                            </div>
                            <div class="item">
                                <p>Адам Эльсхеймер</p>

                                <div class="for_img">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/reproduktsia/2.jpg" alt=""
                                         class="a-image"/>
                                </div>
                                <a href="#">Подробнее</a>
                            </div>
                            <div class="item">
                                <p>Адольф Монтичелли</p>

                                <div class="for_img">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/reproduktsia/3.jpg" alt=""
                                         class="a-image"/>
                                </div>
                                <a href="#">Подробнее</a>
                            </div>
                            <div class="item">
                                <p>Адриан ван де Велде</p>

                                <div class="for_img">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/reproduktsia/4.jpg" alt=""
                                         class="a-image"/>
                                </div>
                                <a href="#">Подробнее</a>
                            </div>
                            <div class="item">
                                <p>Адриан ван Остаде</p>

                                <div class="for_img">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/reproduktsia/5.jpg" alt=""
                                         class="a-image"/>
                                </div>
                                <a href="#">Подробнее</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="line5pic">
                            <div class="item">
                                <p>Абрахам ван Калрает</p>

                                <div class="for_img">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/reproduktsia/1.jpg" alt=""
                                         class="a-image"/>
                                </div>
                                <a href="#">Подробнее</a>
                            </div>
                            <div class="item">
                                <p>Адам Эльсхеймер</p>

                                <div class="for_img">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/reproduktsia/2.jpg" alt=""
                                         class="a-image"/>
                                </div>
                                <a href="#">Подробнее</a>
                            </div>
                            <div class="item">
                                <p>Адольф Монтичелли</p>

                                <div class="for_img">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/reproduktsia/3.jpg" alt=""
                                         class="a-image"/>
                                </div>
                                <a href="#">Подробнее</a>
                            </div>
                            <div class="item">
                                <p>Адриан ван де Велде</p>

                                <div class="for_img">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/reproduktsia/4.jpg" alt=""
                                         class="a-image"/>
                                </div>
                                <a href="#">Подробнее</a>
                            </div>
                            <div class="item">
                                <p>Адриан ван Остаде</p>

                                <div class="for_img">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/reproduktsia/5.jpg" alt=""
                                         class="a-image"/>
                                </div>
                                <a href="#">Подробнее</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="line5pic">
                            <div class="item">
                                <p>Абрахам ван Калрает</p>

                                <div class="for_img">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/reproduktsia/1.jpg" alt=""
                                         class="a-image"/>
                                </div>
                                <a href="#">Подробнее</a>
                            </div>
                            <div class="item">
                                <p>Адам Эльсхеймер</p>

                                <div class="for_img">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/reproduktsia/2.jpg" alt=""
                                         class="a-image"/>
                                </div>
                                <a href="#">Подробнее</a>
                            </div>
                            <div class="item">
                                <p>Адольф Монтичелли</p>

                                <div class="for_img">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/reproduktsia/3.jpg" alt=""
                                         class="a-image"/>
                                </div>
                                <a href="#">Подробнее</a>
                            </div>
                            <div class="item">
                                <p>Адриан ван де Велде</p>

                                <div class="for_img">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/reproduktsia/4.jpg" alt=""
                                         class="a-image"/>
                                </div>
                                <a href="#">Подробнее</a>
                            </div>
                            <div class="item">
                                <p>Адриан ван Остаде</p>

                                <div class="for_img">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/reproduktsia/5.jpg" alt=""
                                         class="a-image"/>
                                </div>
                                <a href="#">Подробнее</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="line5pic">
                            <div class="item">
                                <p>Абрахам ван Калрает</p>

                                <div class="for_img">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/reproduktsia/1.jpg" alt=""
                                         class="a-image"/>
                                </div>
                                <a href="#">Подробнее</a>
                            </div>
                            <div class="item">
                                <p>Адам Эльсхеймер</p>

                                <div class="for_img">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/reproduktsia/2.jpg" alt=""
                                         class="a-image"/>
                                </div>
                                <a href="#">Подробнее</a>
                            </div>
                            <div class="item">
                                <p>Адольф Монтичелли</p>

                                <div class="for_img">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/reproduktsia/3.jpg" alt=""
                                         class="a-image"/>
                                </div>
                                <a href="#">Подробнее</a>
                            </div>
                            <div class="item">
                                <p>Адриан ван де Велде</p>

                                <div class="for_img">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/reproduktsia/4.jpg" alt=""
                                         class="a-image"/>
                                </div>
                                <a href="#">Подробнее</a>
                            </div>
                            <div class="item">
                                <p>Адриан ван Остаде</p>

                                <div class="for_img">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/reproduktsia/5.jpg" alt=""
                                         class="a-image"/>
                                </div>
                                <a href="#">Подробнее</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="line5pic">
                            <div class="item">
                                <p>Абрахам ван Калрает</p>

                                <div class="for_img">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/reproduktsia/1.jpg" alt=""
                                         class="a-image"/>
                                </div>
                                <a href="#">Подробнее</a>
                            </div>
                            <div class="item">
                                <p>Адам Эльсхеймер</p>

                                <div class="for_img">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/reproduktsia/2.jpg" alt=""
                                         class="a-image"/>
                                </div>
                                <a href="#">Подробнее</a>
                            </div>
                            <div class="item">
                                <p>Адольф Монтичелли</p>

                                <div class="for_img">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/reproduktsia/3.jpg" alt=""
                                         class="a-image"/>
                                </div>
                                <a href="#">Подробнее</a>
                            </div>
                            <div class="item">
                                <p>Адриан ван де Велде</p>

                                <div class="for_img">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/reproduktsia/4.jpg" alt=""
                                         class="a-image"/>
                                </div>
                                <a href="#">Подробнее</a>
                            </div>
                            <div class="item">
                                <p>Адриан ван Остаде</p>

                                <div class="for_img">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/reproduktsia/5.jpg" alt=""
                                         class="a-image"/>
                                </div>
                                <a href="#">Подробнее</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                    </div>

                    <h1> тут не зробив пагынацыю ы в шапыцы рамки на картинках</h1>
                    <div class="page">
                        <h2 class="color-header">КРАСИВЫЙ ЗАГОЛОВОК</h2>
                    </div>
                    <div class="text_end">
                        <p>Идейные соображения высшего порядка, а также начало повседневной работы по формированию
                            позиции требуют определения и уточнения форм развития.</p>

                        <p>С другой стороны рамки и место обучения кадров влечет за собой процесс внедрения и
                            модернизации дальнейших направлений развития. С другой стороны сложившаяся структура
                            организации позволяет оценить значение модели развития.</p>

                        <p> Задача организации, в особенности же постоянный количественный рост и сфера нашей активности
                            обеспечивает широкому кругу (специалистов) участие в формировании существенных финансовых и
                            административных условий. Таким образом новая модель организационной деятельности в
                            значительной степени обуславливает создание системы обучения кадров, соответствует насущным
                            потребностям.
                            Повседневная практика показывает, что постоянный количественный рост и сфера нашей
                            активности обеспечивает широкому кругу (специалистов) участие в формировании систем
                            массового участия. Равным образом постоянный количественный рост и сфера нашей активности в
                            значительной степени обуславливает создание дальнейших направлений развития.</p>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>