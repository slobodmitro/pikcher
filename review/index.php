<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Отзывы - PIKcher");
$APPLICATION->SetTitle("Виды картин");
?>
<div class="popup-response">
    <div class="response-wraper">
        <form class="response-form" method="post"  action="" id="ajaxform_review" enctype="multipart/form-data">
            <div class="form-header">заполните форму</div>
            <div class="form-small-text">чтобы оставить свой отзыв!</div>
            <input type="text" class="form-input" name="name" placeholder="Ваше имя"/>
            <input type="text" class="form-input" name="city" placeholder="Ваш город"/>
            <input type="email" class="form-input" name="email" placeholder="Ваш e-mail"/>
            <textarea class="response-text" name="response-text" id="response-text"
                      placeholder="Ваш комментарий"></textarea>

            <div class="upload-button">
                <input id="file_s" class="real-button" name="file" type="file">
                <input class="uploaded-name" type="text" placeholder="Ваше фото">
                <button class="fake-button">Загрузить файл</button>
            </div>
            <button type="submit" class="send-button">отправить</button>
            <div class="privacy">Политика конфиденциальности</div>
            <img class="close-form" src="<?=SITE_TEMPLATE_PATH?>/images/close-popup.png" alt="">
        </form>
        <script>
            $(document).ready(function() {

                    //LightBox
                    var fancy = jQuery('.fancy');
                    fancy.fancybox({
                        beforeShow: function () {
                            this.title = "<div class='fancy-title'>" +
                            "<div class='text'>Желаете такую же?</div>" +
                            "<div class='purple-button' style='margin-top: 0'><a class='pb-link' href='http://<?php echo $_SERVER['HTTP_HOST'] ?>/zakazat-kartinu-na-khoste'>заказать</a></div>" +
                            "</div>";
                        },
                        helpers: {
                            title: {
                                type: 'inside'
                            }
                        },
                        openEffect: 'elastic',
                        closeEffect: 'elastic'
                    });

                jQuery('form').on('focusout', 'input', function () {
                    debugger;
                    jQuery(this).css('border','none');
                });
                var body = jQuery('body');
                $("#ajaxform_review").submit(function(){
                    var form = $(this);
                    var error = false;
                    form.find('input, textarea').each( function(){
                        if ($(this).val() == '') {
                            $(this).css('border','1px solid red');
                            error = true; // oшибкa
                        }
                    });

                    if (!error) { // eсли oшибки нeт
                        var fd = new FormData();
                        fd.append('name', $(this).find('input[name=name]').val());
                        fd.append('email', $(this).find('input[name=email]').val());
                        fd.append('city', $(this).find('input[name=city]').val());
                        fd.append('comment', $(this).find('textarea[name=response-text]').val());
                        fd.append('img', $(this).find('#file_s')[0].files[0]);
                        $.ajax({
                            type: 'POST', // oтпрaвляeм в POST фoрмaтe, мoжнo GET
                            url: '<?=SITE_TEMPLATE_PATH?>/email-review.php', // путь дo oбрaбoтчикa, у нaс oн лeжит в тoй жe пaпкe
                            processData: false,
                            contentType: false,
                            dataType: 'json', // oтвeт ждeм в json фoрмaтe
                            data: fd, // дaнныe для oтпрaвки
                            beforeSend: function(data) { // сoбытиe дo oтпрaвки
                                form.find('input[type="submit"]').attr('disabled', 'disabled'); // нaпримeр, oтключим кнoпку, чтoбы нe жaли пo 100 рaз
                            },
                            success: function(data){ // сoбытиe пoслe удaчнoгo oбрaщeния к сeрвeру и пoлучeния oтвeтa
                                form.find('.close-form').click();
                                var sucForm = jQuery('.popup-success');
                                sucForm.toggle();
                                body.css({"overflow": "hidden"});
                            },
                            error: function (xhr, ajaxOptions, thrownError) { // в случae нeудaчнoгo зaвeршeния зaпрoсa к сeрвeру
                                alert(xhr.status); // пoкaжeм oтвeт сeрвeрa
                                alert(thrownError); // и тeкст oшибки
                            },
                            complete: function(data) { // сoбытиe пoслe любoгo исхoдa
                                form.find('input[type="submit"]').prop('disabled', false); // в любoм случae включим кнoпку oбрaтнo
                            }

                        });
                    }
                    return false; // вырубaeм стaндaртную oтпрaвку фoрмы
                });
            });
        </script>
    </div>
</div>
<script src="<?=SITE_TEMPLATE_PATH?>/js/response.js"></script>
    <div class="pagination">
        <div class="container">
            <div class="page">
                <div class="color-header">Отзывы <span class="text-bold">наших клиентов</span></div>
                <?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"reviews_our", 
	array(
		"COMPONENT_TEMPLATE" => "reviews_our",
		"IBLOCK_TYPE" => "review_our",
		"IBLOCK_ID" => "12",
		"NEWS_COUNT" => "3",
        "SORT_BY1" => "DATA",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "SORT",
        "SORT_ORDER2" => "DESC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "CITY",
			1 => "DATA",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "Y",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"PAGER_TEMPLATE" => "round",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => ""
	),
	false
);?>
            </div>
        </div>
        <div class="container">
        <div id="first_button" class="purple-button"><a class="pb-link call-pr" href="">Оставить свой отзыв</a></div>
        <div id="second_button" class="purple-button"><a class="pb-link" href="http://<?php echo $_SERVER['HTTP_HOST'] ?>/zakazat-kartinu-na-khoste/">Заказать фото на холсте</a></div>
    </div>
        </div>
    </div>
    <footer class="op-footer">
        <div class="grey-horizontal">
            <div class="container">
                <?$APPLICATION->IncludeComponent("bitrix:menu", "menu_bottom", Array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
                    "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                    "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                    "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                    "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                    "MAX_LEVEL" => "1",	// Уровень вложенности меню
                    "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                    "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                    "DELAY" => "N",	// Откладывать выполнение шаблона меню
                    "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                ),
                    false
                );?>
                <div class="phone-block">
                    <div class="phone phone-to-change">8 (495) 761-76-61</div>
                    <div class="work-time time-to-change">мы работаем: 9:00 - 20:00</div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="copyright">Copyright PikCher &copy; 2015.Все права защищены</div>
    </footer>
    </body>
    </html>
